# 🌴-carribean-regulation

[https://sourcecrypto.pub/bitcoin-history/blockchain-bahamas-crypto-caribbean/](https://sourcecrypto.pub/bitcoin-history/blockchain-bahamas-crypto-caribbean/)

[Bahamas: The ‘Silicon Valley’ of the Caribbean](https://sourcecrypto.pub/bitcoin-history/blockchain-bahamas-crypto-caribbean/)

Timeline of Blockchain and Cryptocurrency in the Caribbean.


[IMF Pressures Marshall Islands to Drop National Crypto - Bitcoin News](https://news.bitcoin.com/imf-pressures-marshall-islands-to-drop-national-crypto/)

The International Monetary Fund (IMF) has exerted pressure on the Marshall Islands to torpedo its proposed crypto. The move by the Washington-based global

[https://podcasts.apple.com/us/podcast/how-caribbean-got-on-road-to-central-bank-digital-currencies/id1123922160?i=1000444014015](https://podcasts.apple.com/us/podcast/how-caribbean-got-on-road-to-central-bank-digital-currencies/id1123922160?i=1000444014015)

[‎Unchained: Your No-Hype Resource for All Things Crypto: How the...](https://podcasts.apple.com/us/podcast/how-caribbean-got-on-road-to-central-bank-digital-currencies/id1123922160?i=1000444014015)

‎Show Unchained: Your No-Hype Resource for All Things Crypto, Ep How the Caribbean Got on the Road to Central Bank Digital Currencies - Ep.127 - Jul 9, 2019

- 🌴-bahamas-regulation

       

    **Discussion Paper: Proposed Approaches to Regulation of Crypto Assets in The Bahamas:** -[https://www.centralbankbahamas.com/download/065758600.pdf](https://www.centralbankbahamas.com/download/065758600.pdf) **Comments:** [https://www.centralbankbahamas.com/download/023817800.pdf](https://www.centralbankbahamas.com/download/023817800.pdf) [https://news.bitcoin.com/bahamas-releases-discussion-paper-on-crypto-asset-regulation/](https://news.bitcoin.com/bahamas-releases-discussion-paper-on-crypto-asset-regulation/) [https://www.centralbankbahamas.com/news.php?id=16453&cmd=view](https://www.centralbankbahamas.com/news.php?id=16453&cmd=view) [https://www.ccn.com/breaking-tether-confirms-its-banking-at-nassau-based-deltec-bank/](https://www.ccn.com/breaking-tether-confirms-its-banking-at-nassau-based-deltec-bank/) [https://web.archive.org/web/20181102165422/https://blokt.com/news/damage-control-tether-limited-announces-partnership-with-bahamas-based-deltec-bank-](https://web.archive.org/web/20181102165422/https://blokt.com/news/damage-control-tether-limited-announces-partnership-with-bahamas-based-deltec-bank-) [https://magneticmediatv.com/2018/11/turning-grand-bahama-into-a-tech-hub/](https://magneticmediatv.com/2018/11/turning-grand-bahama-into-a-tech-hub/)

    [Bahamas Releases Discussion Paper on Cryptoassets Regulation](https://news.bitcoin.com/bahamas-releases-discussion-paper-on-crypto-asset-regulation/)

    The Central Bank of The Bahamas (CBOB) has released a discussion paper proposing how it intends to regulate digital assets.

    
    [Breaking: Tether Opens $1.8 Billion Bank Account in the Bahamas](https://www.ccn.com/breaking-tether-confirms-its-banking-at-nassau-based-deltec-bank/)

    Tether Limited, the issuer of controversial USD-pegged cryptocurrency stablecoin tether (USDT), has confirmed that it is banking in the Bahamas.

    
    [https://allianceblockchain.org/](https://allianceblockchain.org/) [http://www.news9.com/story/39554251/idb-and-cba-collaborate-with-blockgeeks-to-launch-blockchain-technology-course-hackathon](http://www.news9.com/story/39554251/idb-and-cba-collaborate-with-blockgeeks-to-launch-blockchain-technology-course-hackathon) [https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000444014015](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000444014015)

    [Our Mission](https://allianceblockchain.org/)

    [‎Unchained: Your No-Hype Resource for All Things Crypto: How the...](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000444014015)

    ‎Show Unchained: Your No-Hype Resource for All Things Crypto, Ep How the Caribbean Got on the Road to Central Bank Digital Currencies - Ep.127 - Jul 9, 2019

    
- 🌴-cayman-islands

  
    [https://www.internationalinvestment.net/news/4002558/cayman-anti-money-laundering-rules-clamps-crypto](https://www.internationalinvestment.net/news/4002558/cayman-anti-money-laundering-rules-clamps-crypto)

    [https://forkast.news/2019/06/11/why-the-cayman-islands-is-not-the-crypto-haven-you-think/](https://forkast.news/2019/06/11/why-the-cayman-islands-is-not-the-crypto-haven-you-think/)

    In terms of funds raised companies from countries such as the UAE and Cayman Islands have leapfrogged ahead of the usual suspects like UK and Singapore. UAE and Cayman Island companies attracted $142 million and $100 million respectively and accounted for 21% and 15% of the total funds raised. The world over, companies were able to collectively raise a staggering $790 million in capital through ICOs.

    [https://hackernoon.com/three-months-into-2019-whats-the-news-in-crypto-d51962097a89](https://hackernoon.com/three-months-into-2019-whats-the-news-in-crypto-d51962097a89)

    [https://www.law.com/legaltechnews/2019/05/08/has-the-crypto-spring-arrived-in-the-cayman-islands/?slreturn=20190626000606](https://www.law.com/legaltechnews/2019/05/08/has-the-crypto-spring-arrived-in-the-cayman-islands/?slreturn=20190626000606)

    [https://www.ft.com/content/718a27a6-4a38-11e9-bde6-79eaea5acb64](https://www.ft.com/content/718a27a6-4a38-11e9-bde6-79eaea5acb64)

    Start-ups have gone to some lengths to avoid such infractions by setting up their businesses in offshore jurisdictions. One Hong Kong-based company last year first moved its official company domain to the Indian Ocean island of Mauritius. It then sold a token from a limited liability company in the Cayman Islands, which was in turn wholly owned by a trust in the island-nation of St Kitts and Nevis. However, such elaborate means of avoiding regulatory scrutiny have since gone out of style in the legal sector, Ms Ng says. Inquiries about STOs have surged this year, she adds, indicating that the future of cryptocurrencies is more likely to play out in regulatory daylight."

    [https://www.futurebooks.com/cayman-islands/](https://www.futurebooks.com/cayman-islands/)

    [https://www.conyers.com/publications/view/establishing-a-business-in-the-cayman-islands/](https://www.conyers.com/publications/view/establishing-a-business-in-the-cayman-islands/)

    [https://nomadcapitalist.com/2019/04/08/cayman-islands-residency/](https://nomadcapitalist.com/2019/04/08/cayman-islands-residency/)

    [https://www.caymanenterprisecity.com/cayman-internet-park](https://www.caymanenterprisecity.com/cayman-internet-park) [https://incorporations.io/cayman-islands](https://incorporations.io/cayman-islands) [https://cdn.discordapp.com/attachments/604163325814767618/604170265710297098/Walkers_Guide_To_Setting_Up_A_Cayman_Islands_Cryptocurrency_Or_Blockchain_Fund_-_Fin_Tech_-_Cayman_I.pdf](https://cdn.discordapp.com/attachments/604163325814767618/604170265710297098/Walkers_Guide_To_Setting_Up_A_Cayman_Islands_Cryptocurrency_Or_Blockchain_Fund_-_Fin_Tech_-_Cayman_I.pdf) [http://www.mondaq.com/caymanislands/x/793662/fin+tech/Walkers+Guide+To+Setting+Up+A+Cayman+Islands+Cryptocurrency+Or+Blockchain+Fund](http://www.mondaq.com/caymanislands/x/793662/fin+tech/Walkers+Guide+To+Setting+Up+A+Cayman+Islands+Cryptocurrency+Or+Blockchain+Fund)

    [Offshore IT Park - Cayman Tech City](https://www.caymanenterprisecity.com/cayman-internet-park)

    Relocate your IT company to the Cayman Islands and enjoy no taxes, no visa restrictions, offshore IP, and a lifestyle second to none
    [Set up a company and open a bank account in Cayman Islands - Incor...](https://incorporations.io/cayman-islands)

    Incorporate a company in Cayman Islands, open an offshore bank account, and learn what are the legal requirements and taxes in Cayman Islands to set up an international business.

    [Walkers Guide To Setting Up A Cayman Islands Cryptocurrency Or Blo...](http://www.mondaq.com/caymanislands/x/793662/fin+tech/Walkers+Guide+To+Setting+Up+A+Cayman+Islands+Cryptocurrency+Or+Blockchain+Fund)

    Once the investment strategy has been determined, and there is the necessary interest from investors, the decision needs to be made on the most optimal way to structure the fund. Cayman Islands Fin Tech Walkers 1 Apr 2019

    

    [http://premieroffshore.com/offshore-ico-scam-and-cayman-islands-corporations/](http://premieroffshore.com/offshore-ico-scam-and-cayman-islands-corporations/)

    [The Offshore ICO Scam and Cayman Islands Corporations - Premier Of...](http://premieroffshore.com/offshore-ico-scam-and-cayman-islands-corporations/)

    There’s an offshore ICO scam going on and it’s focused on Cayman Islands Corporations. If you’re planning an ICO, here’s what you need to know to avoid the offshore ICO scam and Cayman Islands Corporations. First, note that I use the term “scam” very carefully and...

    
    >The typical components of an offshore corporation in Caymans that will issue an ICO are as follows: - Corporate entity, - Financial Services Entity, - Anti Money Laundering Manual (AML), - Know Your Customers (KYC) procedures and systems, - Dedicated Money Laundering Reporting Officer or Chief Compliance Officer with several years experience, and - Sufficient capital to entice a correspondent bank to take you on as a client.

    

    [https://flagtheory.com/category/cayman-islands/](https://flagtheory.com/category/cayman-islands/)

    [Offshore company and bank account in Cayman Islands - Flag Theory](https://flagtheory.com/category/cayman-islands/)

    Learn why and how to set up an offshore company in Cayman Islands, open an offshore bank account, and get residency in these Caribbean Islands.

    ![https://flagtheory.com/wp-content/uploads/2018/07/logo_with_bg.png](https://flagtheory.com/wp-content/uploads/2018/07/logo_with_bg.png)