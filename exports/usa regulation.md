# 🇺🇸-usa-regulation

Crypto⧉Finance

🇺🇸-usa-regulation

26 messages

[https://www.forbes.com/sites/caitlinlong/2019/07/15/facebooks-cryptocurrency-libra-senate-banking-testimony/](https://www.forbes.com/sites/caitlinlong/2019/07/15/facebooks-cryptocurrency-libra-senate-banking-testimony/) (edited)

[Facebook's Cryptocurrency, Libra: Senate Banking Testimony](https://www.forbes.com/sites/caitlinlong/2019/07/15/facebooks-cryptocurrency-libra-senate-banking-testimony/)

The Senate Banking Committee staff reached out regarding Facebook's Libra project, pursuant to my recent Forbes.com article. During a conference call last week, they indicated interest in receiving formal written testimony from me. Here it is.

[Jake Chervinsky (@jchervinsky)](https://stanford-jblp.pubpub.org/) (edited)

Major US regulators for crypto: - SEC (securities, exchanges, funds...) - CFTC (derivatives, margin trading...) - DOJ (crimes...) - FINRA (brokers...) - FinCEN (money laundering...) - OFAC (sanctions...) - CFPB (consumers...) - OCC (banks...) And they're just getting sta...

Retweets

298

Likes

889

Twitter

[Marco Santori (@msantoriESQ)](https://twitter.com/msantoriESQ)

1/ Today, the US Department of the Treasury, through the Office of Foreign Assets Control (OFAC) took the historic step of adding two Bitcoin addresses to its list of sanctioned parties. In this thread, I'll break down what that means.

Retweets

147

Likes

265

Twitter

[Stanford Journal of Blockchain Law & Policy](https://stanford-jblp.pubpub.org/)

Stanford Journal of Blockchain Law & Policy

[https://blog.circle.com/2019/05/23/our-take-interpreting-recent-signals-from-us-regulatory-agencies/](https://blog.circle.com/2019/05/23/our-take-interpreting-recent-signals-from-us-regulatory-agencies/) Committee Democrats Call on Facebook to Halt Cryptocurrency Plans | Financial Services Committee [https://financialservices.house.gov/news/documentsingle.aspx?DocumentID=404009](https://financialservices.house.gov/news/documentsingle.aspx?DocumentID=404009) [https://www.youtube.com/channel/UCgo7FCCPuylVk4luP3JAgVw/videos](https://www.youtube.com/channel/UCgo7FCCPuylVk4luP3JAgVw/videos) [https://link.medium.com/5iXDZdkAzY](https://link.medium.com/5iXDZdkAzY) (edited)

[Our take: Interpreting recent signals from US regulatory agencies](https://blog.circle.com/2019/05/23/our-take-interpreting-recent-signals-from-us-regulatory-agencies/)

We want to highlight how recent signals from U.S. regulators are creating an uncertain environment for crypto assets, prompting us to take actions that we—and our customers and community—find deeply frustrating. The heart of our argument for a clear, forward-looking regul...

[Committee Democrats Call on Facebook to Halt Cryptocurrency Plans](https://financialservices.house.gov/news/documentsingle.aspx?DocumentID=404009)

[CRI](https://www.youtube.com/channel/UCgo7FCCPuylVk4luP3JAgVw/videos)

Congress can and should reflect the will of the people. Our research strives to achieve this goal by focusing on the institutional design of legislatures. Es...

[https://yt3.ggpht.com/a/AGF-l78NhHsYNMmOMy-Qhh1zteHReYJSC4yEQlCGGw=s900-c-k-c0xffffffff-no-rj-mo](https://yt3.ggpht.com/a/AGF-l78NhHsYNMmOMy-Qhh1zteHReYJSC4yEQlCGGw=s900-c-k-c0xffffffff-no-rj-mo)

[Operation Choke Point](https://link.medium.com/5iXDZdkAzY)

I was skimming the facebook page of a soon-to-be-podcast guest, Kingsley Edwards, and came across a disturbing article describing…

[Angela Walch (@angela_walch)](https://twitter.com/angela_walch/status/1154962452574085120?s=20)

If you've been following the miners as intermediaries discussion at all, this thread is oh so relevant. #crypto #blockchain [https://t.co/B78q1Y3xRt](https://t.co/B78q1Y3xRt)

Twitter

[https://www.bloomberg.com/news/articles/2018-05-24/bitcoin-manipulation-is-said-to-be-focus-of-u-s-criminal-probe](https://www.bloomberg.com/news/articles/2018-05-24/bitcoin-manipulation-is-said-to-be-focus-of-u-s-criminal-probe)

[Mike Dudas (@mdudas)](https://twitter.com/mdudas/status/1154877692883419138?s=12)

CoinDesk didn’t contact us for comment on a story

” “The Block did contact us for comment on a story. We are going to front run it, they are bad guys with evil motives.” ~ @bitcoinlawyer

Twitter

[https://unchainedpodcast.com/jeremy-allaire-on-why-the-us-government-needs-a-new-category-for-digital-assets/](https://unchainedpodcast.com/jeremy-allaire-on-why-the-us-government-needs-a-new-category-for-digital-assets/)

[Jeremy Allaire on Why the US Government Needs a New Category for D...](https://unchainedpodcast.com/jeremy-allaire-on-why-the-us-government-needs-a-new-category-for-digital-assets/)

Jeremy Allaire, cofounder and CEO of Circle, explains why the company has opened a new subsidiary in Bermuda, obtained a full Digital Assets Business Act license there and how it[...]Keep reading...

[https://www.banking.senate.gov/hearings/examining-regulatory-frameworks-for-digital-currencies-and-blockchain](https://www.banking.senate.gov/hearings/examining-regulatory-frameworks-for-digital-currencies-and-blockchain)

[Examining Regulatory Frameworks for Digital Currencies and Blockchain](https://www.banking.senate.gov/hearings/examining-regulatory-frameworks-for-digital-currencies-and-blockchain)

The Official website of The United States Committee on Banking, Housing, and Urban Affairs

[https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437366894](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437366894)

[‎Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distr...](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437366894)

‎Show Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distributed Technologies, Ep Peter Van Valkenburgh: Where US Cryptocurrency Regulation is Heading - Mar 21, 2018

[https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000409055013](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000409055013)

[‎Unchained: Your No-Hype Resource for All Things Crypto: The Cha...](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000409055013)

‎Show Unchained: Your No-Hype Resource for All Things Crypto, Ep The Chamber of Digital Commerce's Perianne Boring and Amy Kim on Why U.S. Crypto Regulation Is Complicated and Confusing - Apr 17, 2018

[https://www.banking.senate.gov/hearings/exploring-the-cryptocurrency-and-blockchain-ecosystem](https://www.banking.senate.gov/hearings/exploring-the-cryptocurrency-and-blockchain-ecosystem)

[Exploring the Cryptocurrency and Blockchain Ecosystem](https://www.banking.senate.gov/hearings/exploring-the-cryptocurrency-and-blockchain-ecosystem)

The Official website of The United States Committee on Banking, Housing, and Urban Affairs

[https://www.lw.com/thoughtLeadership/yellow-brick-road-for-consumer-tokens-path-to-sec-cftc-compliance](https://www.lw.com/thoughtLeadership/yellow-brick-road-for-consumer-tokens-path-to-sec-cftc-compliance)

The yellow brick road for consumer tokens:The path to SEC andCFTC complianceDavid L. Concannon, Yvette D. Valdez & Stephen P. WinkLatham & Watkins LLPDeveloping a framework for consumer tokens

[https://medium.com/@wmougayar/a-whos-who-in-the-fight-for-better-blockchain-regulation-in-the-us-bf04ebd2541c](https://medium.com/@wmougayar/a-whos-who-in-the-fight-for-better-blockchain-regulation-in-the-us-bf04ebd2541c)

[A Who’s Who in The Fight for Better Blockchain Regulation in the US](https://medium.com/@wmougayar/a-whos-who-in-the-fight-for-better-blockchain-regulation-in-the-us-bf04ebd2541c)

Who is challenging US regulators and primarily the SEC for friendlier blockchain regulation?

[https://www.forbes.com/sites/ktorpey/2019/07/30/us-lawmakers-are-realizing-they-cant-ban-bitcoin/](https://www.forbes.com/sites/ktorpey/2019/07/30/us-lawmakers-are-realizing-they-cant-ban-bitcoin/)

[U.S. Lawmakers Are Realizing They Can’t Ban Bitcoin](https://www.forbes.com/sites/ktorpey/2019/07/30/us-lawmakers-are-realizing-they-cant-ban-bitcoin/)

Implementing a Bitcoin ban may not be as easy as some cryptocurrency skeptics think.

[https://corpgov.law.harvard.edu/2019/04/22/cryptocurrency-and-other-digital-assets-for-asset-managers/](https://corpgov.law.harvard.edu/2019/04/22/cryptocurrency-and-other-digital-assets-for-asset-managers/)

[Cryptocurrency and Other Digital Assets for Asset Managers](https://corpgov.law.harvard.edu/2019/04/22/cryptocurrency-and-other-digital-assets-for-asset-managers/)

Read our latest post from Davis Polk partner Gregory S. Rowland and associate Trevor I. Kiviat.

[https://www.americancryptoassociation.com/2018/12/05/gsx-group-tzero-and-sharespost-among-founders-of-new-interoperable-security-token-framework/](https://www.americancryptoassociation.com/2018/12/05/gsx-group-tzero-and-sharespost-among-founders-of-new-interoperable-security-token-framework/)

[GSX Group, tZERO, and SharesPost among founders of new interoperab...](https://www.americancryptoassociation.com/2018/12/05/gsx-group-tzero-and-sharespost-among-founders-of-new-interoperable-security-token-framework/)

A consortium of influential blockchain industry companies have announced the launch of a common framework to facilitate market interoperability of security tokens. The Millbrook Accord, … GSX Group, tZERO, and SharesPost among founders of new interoperable security token f...

[https://verifiedtokenframework.com/](https://verifiedtokenframework.com/)

[Verified Token Framework](https://verifiedtokenframework.com/)

Enabling the brightline controls required for Security Tokens

[https://identitymindglobal.com/blog/bitcoin-address-sanctions/](https://identitymindglobal.com/blog/bitcoin-address-sanctions/) [https://github.com/ethhub-io/ethhub/blob/master/other/ethhub-cftc-response.md](https://github.com/ethhub-io/ethhub/blob/master/other/ethhub-cftc-response.md)

[Bitcoin on the OFAC List: How to Protect Yourself As a Bank, Crypt...](https://identitymindglobal.com/blog/bitcoin-address-sanctions/)

This blog post was originally posted on December 5, 2018, and updated extensively on 3/27/2019. US Exchanges Banned from Interacting with Specific Bitcoin Addresses The US Government has been monitoring cryptocurrencies for several years. In March 2013, FinCEN’s guidance ex...

[https://theblockcrypto.com/tiny/congress-member-to-introduce-three-pro-cryptocurrency-bills/](https://theblockcrypto.com/tiny/congress-member-to-introduce-three-pro-cryptocurrency-bills/)

[U.S. Congressman to introduce three pro-cryptocurrency bills - The...](https://theblockcrypto.com/tiny/congress-member-to-introduce-three-pro-cryptocurrency-bills/)

Representative Tom Emmer announced three bills to support blockchain technology and cryptocurrency. The bills are as follows: The Resolution Supporting Digital Currencies and Blockchain Technology supports a “light touch, consistent, and simple legal environment” for cr...

[https://media.consensys.net/blockchain-law-congress-house-senate-2019-133e30fd5dd5](https://media.consensys.net/blockchain-law-congress-house-senate-2019-133e30fd5dd5) [https://www.congress.gov/bill/115th-congress/house-bill/7356/](https://www.congress.gov/bill/115th-congress/house-bill/7356/) [https://twitter.com/stephendpalley/status/1154541606042853376?s=17](https://twitter.com/stephendpalley/status/1154541606042853376?s=17)

[Meet the American Legislators Bullish on Blockchain](https://media.consensys.net/blockchain-law-congress-house-senate-2019-133e30fd5dd5)

The Congressional Blockchain Congress sees 2019 as a breakout year, while States around the country are moving forward fast.

[https://miro.medium.com/max/1200/0*E5j6e8NVskC0e58W](https://miro.medium.com/max/1200/0*E5j6e8NVskC0e58W)

[H.R.7356 - 115th Congress (2017-2018): Token Taxonomy Act](https://www.congress.gov/bill/115th-congress/house-bill/7356/)

Summary of H.R.7356 - 115th Congress (2017-2018): Token Taxonomy Act

[Palley (@stephendpalley)](https://twitter.com/stephendpalley)

Breaking news that isn't a surprise: if you sell a token that can be used on a blockchain platform already fully developed & functional & the token can be used for its intended purpose (here, gaming) when you buy it's not a security which is no surprise. [https://t.co/ykjA](https://t.co/ykjA)...

Twitter

[https://coincenter.org/entry/three-pro-cryptocurrency-bills-are-being-introduced-in-congress](https://coincenter.org/entry/three-pro-cryptocurrency-bills-are-being-introduced-in-congress)

[Three pro-cryptocurrency bills are being introduced in Congress | ...](https://coincenter.org/entry/three-pro-cryptocurrency-bills-are-being-introduced-in-congress)

The Congressional Blockchain Caucus is growing and its new co-chair is taking steps to make life easier for cryptocurrency developers and users

[https://stanford-jblp.pubpub.org/](https://stanford-jblp.pubpub.org/) [https://jblp.scholasticahq.com/](https://jblp.scholasticahq.com/) [https://www.banking.senate.gov/imo/media/doc/Roubini%20Testimony%2010-11-18.pdf](https://www.banking.senate.gov/imo/media/doc/Roubini%2520Testimony%252010-11-18.pdf)

[Stanford Journal of Blockchain Law & Policy](https://stanford-jblp.pubpub.org/)

Stanford Journal of Blockchain Law & Policy

[Stanford Journal of Blockchain Law & Policy](https://jblp.scholasticahq.com/)

The first law journal to publish on the greater blockchain technology space, featuring Articles (peer-reviewed), Essays, and Comments.

[https://medium.com/crypto-caselaw-minute/crypto-caselaw-minute-19-1-16-2019-2b737c726f17](https://medium.com/crypto-caselaw-minute/crypto-caselaw-minute-19-1-16-2019-2b737c726f17)

[Crypto Caselaw Minute #19 — 1/16/2019](https://medium.com/crypto-caselaw-minute/crypto-caselaw-minute-19-1-16-2019-2b737c726f17)

This week we look at two new complaints, and one new opinion. The new complaints deal with the actions of agents, what happens when you…

[https://cointelegraph.com/news/us-crypto-review-top-5-states-with-welcoming-regulations](https://cointelegraph.com/news/us-crypto-review-top-5-states-with-welcoming-regulations)

[US Crypto Review: Top-5 States With Welcoming Regulations](https://cointelegraph.com/news/us-crypto-review-top-5-states-with-welcoming-regulations)

Two U.S. states introduce regulation over crypto space. States regulate crypto differently, a question arises: What are the most friendly ones?

[Drew Hinkes (@propelforward)](https://twitter.com/propelforward/status/1166362937537290248?s=12)

#Illinois adopts SB 1464; includes revisions to state unclaimed property act which incorporates #virtualcurrency in state definition of property.

Twitter

- 🇺🇸-nys-new-york

        Crypto⧉Finance

    🇺🇸-nys-new-york

    NEVER
    10 messages

    

    [https://twitter.com/kylesgibson/status/1154968988214054913?s=12](https://twitter.com/kylesgibson/status/1154968988214054913?s=12)

    
    [Kyle S. Gibson (@KyleSGibson)](https://twitter.com/KyleSGibson)

    what @TheBlock__ reported today about Bitfinex not having simple stop-gaps to block NY users corroborates what the OAG said in their Virtual Currency Report. [https://t.co/agA2vPIwcJ](https://t.co/agA2vPIwcJ)

    
    Twitter

    

    [https://www.bloomberg.com/news/articles/2018-09-18/cme-s-crypto-partners-get-called-out-by-new-york-regulators](https://www.bloomberg.com/news/articles/2018-09-18/cme-s-crypto-partners-get-called-out-by-new-york-regulators)

    [https://twitter.com/jespow/status/1042450533120786432](https://twitter.com/jespow/status/1042450533120786432)

    
    [Jesse Powell (@jespow)](https://twitter.com/jespow)

    NY is that abusive, controlling ex you broke up with 3 years ago but they keep stalking you, throwing shade on your new relationships, unable to accept that you have happily moved on and are better off without them. #getoverit [https://t.co/DC5S1WyRnp](https://t.co/DC5S1WyRnp)

    Retweets

    135

    Likes

    643

    
    Twitter

    

    [https://twitter.com/mdudas/status/1154877692883419138?s=12](https://twitter.com/mdudas/status/1154877692883419138?s=12)

    
    [Mike Dudas (@mdudas)](https://twitter.com/mdudas)

    CoinDesk didn’t contact us for comment on a story

    
    
    
    ” “The Block did contact us for comment on a story. We are going to front run it, they are bad guys with evil motives.” ~ @bitcoinlawyer

    
    Twitter

    

    [https://twitter.com/mdudas/status/1156910235619090433?s=12](https://twitter.com/mdudas/status/1156910235619090433?s=12)

    
    [Mike Dudas (@mdudas)](https://twitter.com/mdudas)

    The NY BitLicense is a disaster. It is insane how many great new financial products are available to people in 49 states but not New York. And to add insult to injury, the creator of the disastrous regulation, @BenLawsky, now sits on the @Ripple Board, reaping an $XRP win...

    Likes

    321

    
    Twitter

    

    [https://www.blockchainandthelaw.com/2018/02/new-york-state-department-of-financial-services-issues-guidance-to-virtual-currency-entities/](https://www.blockchainandthelaw.com/2018/02/new-york-state-department-of-financial-services-issues-guidance-to-virtual-currency-entities/)

    [New York State Department of Financial Services Issues Guidance to...](https://www.blockchainandthelaw.com/2018/02/new-york-state-department-of-financial-services-issues-guidance-to-virtual-currency-entities/)

    On February 7, 2018, the New York State Department of Financial Services (“DFS”) issued guidance for all virtual currency entities licensed by New York

    
    [https://www.blockchainandthelaw.com/2018/05/cryptoasset-exchanges-respond-to-new-york-attorney-generals-virtual-markets-integrity-initiative/](https://www.blockchainandthelaw.com/2018/05/cryptoasset-exchanges-respond-to-new-york-attorney-generals-virtual-markets-integrity-initiative/)

    [Cryptoasset Exchanges Respond to New York Attorney General’s Vir...](https://www.blockchainandthelaw.com/2018/05/cryptoasset-exchanges-respond-to-new-york-attorney-generals-virtual-markets-integrity-initiative/)

    On April 17, 2018, the New York Attorney General’s Office (“OAG”) launched a Virtual Markets Integrity Initiative and sent letters to thirteen cryptoasset

    
    

    [https://virtualmarkets.ag.ny.gov/](https://virtualmarkets.ag.ny.gov/)

    [Virtual Markets Integrity Initiative Report](https://virtualmarkets.ag.ny.gov/)

    New York AG Barbara D. Underwood released a report on virtual markets, finding many platforms vulnerable to abusive trading, conflicts of interest, and other consumer risks.

    
    

    [https://messari.substack.com/p/about-the-nyags-crypto-report-messaris-unqualified-opinions](https://messari.substack.com/p/about-the-nyags-crypto-report-messaris-unqualified-opinions)

    [About the NYAG's crypto report - Messari's Unqualified Opinions](https://messari.substack.com/p/about-the-nyags-crypto-report-messaris-unqualified-opinions)

    Messari’s Unqualified Opinions Issue #2 TLDR: The NYAG report on crypto trading platforms is the most comprehensive investigation released to date by a regulator. It contains a wealth of information. But to understand it, you need some context. How did the NYAG’s office o...

    

    [https://www.blockchainandthelaw.com/2019/07/new-york-state-department-of-financial-services-establishes-new-research-and-innovation-division/](https://www.blockchainandthelaw.com/2019/07/new-york-state-department-of-financial-services-establishes-new-research-and-innovation-division/)

    [New York State Department of Financial Services Establishes New Re...](https://www.blockchainandthelaw.com/2019/07/new-york-state-department-of-financial-services-establishes-new-research-and-innovation-division/)

    On July 23, the New York State Department of Financial Services (the “DFS”) issued a press release announcing the establishment of a new Research and

    
- 🇺🇸-montana

        Crypto⧉Finance

    🇺🇸-montana

    2 messages

    

    [https://cointelegraph.com/news/montana-passes-bill-to-recognize-utility-tokens-and-exempt-them-from-state-securities](https://cointelegraph.com/news/montana-passes-bill-to-recognize-utility-tokens-and-exempt-them-from-state-securities)

    [Montana Passes Bill to Recognize Utility Tokens and Exempt Them Fr...](https://cointelegraph.com/news/montana-passes-bill-to-recognize-utility-tokens-and-exempt-them-from-state-securities)

    Montana Governor Steve Bullock signed a bill exempting utility tokens from state securities earlier in May.

    
    [https://www.coindesk.com/montana-passes-bill-to-exempt-utility-tokens-from-securities-laws](https://www.coindesk.com/montana-passes-bill-to-exempt-utility-tokens-from-securities-laws)

    [Montana Passes Bill to Exempt Utility Tokens From Securities Laws ...](https://www.coindesk.com/montana-passes-bill-to-exempt-utility-tokens-from-securities-laws)

    "Big Sky Country" has gone crypto friendly, with new legislation exempting utility tokens from securities laws.

    
- 🇺🇸-irs-taxes

        Crypto⧉Finance

    🇺🇸-irs-taxes

    10 messages

    

    
    [Crypto Tax Girl (@CryptoTaxGirl)](https://twitter.com/CryptoTaxGirl)

    If crypto is your only source of income, your first $12,200 of short-term gains and your first $51,575 of long-term gains are completely tax free. (If you’re married, replace those numbers with $24,400 and $103,150 respectively)

    Retweets

    241

    Likes

    957

    
    Twitter

    
    [Crypto Tax Girl (@CryptoTaxGirl)](https://twitter.com/CryptoTaxGirl)

    I've had quite a few clients receive letters from the IRS these past two weeks regarding cryptocurrency compliance. It's a standard letter that is three pages long and basically says that the IRS has record of you owning crypto, so you better be compliant.

    Likes

    166

    
    Twitter

    [IRS Confirms It Trained Staff to Find Crypto Wallets - CoinDesk](https://www.coindesk.com/irs-confirms-it-trained-staff-on-finding-crypto-wallets)

    The IRS may subpoena tech firms like Apple, Google and Microsoft in search of taxpayers’ unreported crypto holdings.

    
    
    [James Foust (@jtfoust)](https://twitter.com/jtfoust)

    On May 16, the IRS acknowledged that taxpayers don't have "clarity on basic issues related to the taxation of virtual currency transactions." Today, they sent letters to more than 10,000 cryptocurrency users they suspect "did not report their [crypto] transactions properl...

    
    Twitter

    [https://twitter.com/cryptotaxgirl/status/1148370517415456768?s=12](https://twitter.com/cryptotaxgirl/status/1148370517415456768?s=12)

    
    [Crypto Tax Girl (@CryptoTaxGirl)](https://twitter.com/CryptoTaxGirl)

    I recently got hold of a presentation given to special agents in the IRS Criminal Investigation division that discussed investigating taxpayers who hold crypto. I went through all of the 181 horribly formatted slides (attached for reference haha) and here's what I learned...

    Retweets

    619

    Likes

    1442

    
    Twitter

    

    [https://www.irs.gov/individuals/international-taxpayers/us-citizens-and-resident-aliens-abroad](https://www.irs.gov/individuals/international-taxpayers/us-citizens-and-resident-aliens-abroad)

    [U.S. Citizens and Resident Aliens Abroad | Internal Revenue Service](https://www.irs.gov/individuals/international-taxpayers/us-citizens-and-resident-aliens-abroad)

    If you are a U.S. citizen or resident alien, the rules for filing income, estate, and gift tax returns and paying estimated tax are generally the same whether you are in the United States or abroad. Your worldwide income is subject to U.S. income tax, regardless of where you ...

    
    

    [https://twitter.com/burgercryptoam/status/1154991474305581057?s=12](https://twitter.com/burgercryptoam/status/1154991474305581057?s=12)

    
    [₿urger (@BurgerCryptoAM)](https://twitter.com/BurgerCryptoAM)

    I hope you all filed your taxes. You don't want to run the risk of getting penalised by the IRS. Happy that in the Netherlands crypto is just included in the wealth tax. Makes it not only easier, but also less expensive. [https://t.co/tIMqePJ12F](https://t.co/tIMqePJ12F)

    
    Twitter

    

    [https://coincenter.org/entry/reps-polis-schweikert-introduce-cryptocurrency-tax-fairness-act-in-congress](https://coincenter.org/entry/reps-polis-schweikert-introduce-cryptocurrency-tax-fairness-act-in-congress)

    [Reps. Polis & Schweikert introduce Cryptocurrency Tax Fairness Act...](https://coincenter.org/entry/reps-polis-schweikert-introduce-cryptocurrency-tax-fairness-act-in-congress)

    Bipartisan bill will make it much easier to use Bitcoin to pay for every day goods and services.

    
    

    [https://cryptotaxgirl.com/](https://cryptotaxgirl.com/)

    [Crypto Tax Girl](https://cryptotaxgirl.com/)

    Making Crypto Taxation Easy

    
    [https://www.youtube.com/channel/UCw4rlAuUNwGqTUTbi3U6EpA/videos](https://www.youtube.com/channel/UCw4rlAuUNwGqTUTbi3U6EpA/videos)

    [Laura Walter](https://www.youtube.com/channel/UCw4rlAuUNwGqTUTbi3U6EpA/videos)

    [https://yt3.ggpht.com/a/AGF-l78hsYiItKfk-diYpZUwUwPVHddacgSTuTkVRw=s900-c-k-c0xffffffff-no-rj-mo](https://yt3.ggpht.com/a/AGF-l78hsYiItKfk-diYpZUwUwPVHddacgSTuTkVRw=s900-c-k-c0xffffffff-no-rj-mo)

    

    [https://twitter.com/bennd77/status/1159660241786757120?s=12](https://twitter.com/bennd77/status/1159660241786757120?s=12)

    
    [Nunya Bidness Giga🌮 and ⚡️🔑 (@bennd77)](https://twitter.com/bennd77)

    Yeah BUDdy
    
    Twitter

    

    [https://tokentax.us/](https://tokentax.us/)

    [TokenTax | Bitcoin Tax | Tax Filing Software for Crypto Like Coinbase](https://tokentax.us/)

    Import your crypto trading from any exchange and export the required bitcoin tax forms. Easy Tax
    
    

    [https://twitter.com/jchervinsky/status/1181997214194749441?s=12](https://twitter.com/jchervinsky/status/1181997214194749441?s=12)

    
    [Jake Chervinsky (@jchervinsky)](https://twitter.com/jchervinsky)

    The IRS has finally published guidance on how digital assets are taxed under US law. It's a mixed bag. Some parts are helpful (calculating FMV); some are bad but expected (payments = capital gains/losses); some are nonsense (forks/airdrops). See here: ([https://t.co/MNO0J](https://t.co/MNO0J)...

    
    Twitter

- 🇺🇸-fincen

        Crypto⧉Finance

    🇺🇸-fincen

    9 messages

    

    [https://cointelegraph.com/news/fincen-takes-first-enforcement-action-against-p2p-cryptocurrency-exchanger](https://cointelegraph.com/news/fincen-takes-first-enforcement-action-against-p2p-cryptocurrency-exchanger)

    [FinCEN Takes First Enforcement Action Against P2P Cryptocurrency E...](https://cointelegraph.com/news/fincen-takes-first-enforcement-action-against-p2p-cryptocurrency-exchanger)

    FinCEN has assessed a civil money penalty for a California resident accused of violating money transmission laws.

    
    [https://www.coindesk.com/fincen-says-some-dapps-are-subject-to-u-s-money-transmitter-rules](https://www.coindesk.com/fincen-says-some-dapps-are-subject-to-u-s-money-transmitter-rules)

    [FinCEN Says Some Dapps Are Subject to US Money Transmitter Rules -...](https://www.coindesk.com/fincen-says-some-dapps-are-subject-to-u-s-money-transmitter-rules)

    Decentralized applications (dapps) may sometimes qualify as money transmitters under U.S. law, FinCEN said.

    
    [https://www.natlawreview.com/article/new-fincen-cryptocurrency-guidance-clarifies-applicability-anti-money-laundering](https://www.natlawreview.com/article/new-fincen-cryptocurrency-guidance-clarifies-applicability-anti-money-laundering)

    [New FinCEN Cryptocurrency Guidance Clarifies Applicability of Anti...](https://www.natlawreview.com/article/new-fincen-cryptocurrency-guidance-clarifies-applicability-anti-money-laundering)

    The Financial Crimes Enforcement Network (FinCEN) is the U.S. Treasury Department bureau charged with monitoring financial transactions in order to combat domestic and international money laundering,

    
    

    [https://www.fincen.gov/news/news-releases/fincen-penalizes-peer-peer-virtual-currency-exchanger-violations-anti-money](https://www.fincen.gov/news/news-releases/fincen-penalizes-peer-peer-virtual-currency-exchanger-violations-anti-money)

    

    [https://bitcoinmagazine.com/articles/op-ed-understanding-latest-fincen-guidance-cryptocurrencies](https://bitcoinmagazine.com/articles/op-ed-understanding-latest-fincen-guidance-cryptocurrencies)

    Peer-to-Peer: Services like LocalBitcoins or OTC trading, are still MSBs if the buyer or seller is advertising the services and/or making a profit from either crypto-to-crypto or fiat-to-crypto exchanges. CVC Wallets: A new four-factor test is created to determine if a wallet provider needs to register as an MSB: (a) who owns the value; (b) where the value is stored; (c) whether the owner interacts directly with the payment system where the CVC runs; and (d) whether the person acting as the intermediary has total independent control over the value.

    (edited)

    [Op Ed: Understanding the Latest FinCEN Guidance for Cryptocurrencies](https://bitcoinmagazine.com/articles/op-ed-understanding-latest-fincen-guidance-cryptocurrencies)

    The Financial Crimes Enforcement Network has issued new “interpretive guidance” about how its regulations apply to businesses that conduct money transmissions in virtual currencies. What do you need to know?

    

    [https://www.systems.cs.cornell.edu/docs/fincen-cvc-guidance-final.pdf](https://www.systems.cs.cornell.edu/docs/fincen-cvc-guidance-final.pdf)

    The Financial Crimes Enforcement Network (FinCEN) is issuing this interpretive guidance to remind persons subject to the Bank Secrecy Act (BSA) how FinCEN regulations relating to money services businesses (MSBs) apply to certain business models1 involving money transmission denominated in value that substitutes for currency, specifically, convertible virtual currencies (CVCs).2 This guidance does not establish any new regulatory expectations or requirements. Rather, it consolidates current FinCEN regulations, and related administrative rulings and guidance issued since 2011, and then applies these rules and interpretations to other common business models involving CVC engaging in the same underlying patterns of activity.

    [https://www.fincen.gov/sites/default/files/2019-05/FinCEN%20CVC%20Guidance%20FINAL.pdf](https://www.fincen.gov/sites/default/files/2019-05/FinCEN%2520CVC%2520Guidance%2520FINAL.pdf)

    Lastly, a person who is engaged in more than one type of business model at the same time may be subject to more than one type of regulatory obligation or exemption. For example, a developer or seller of either a software application or a new CVC platform may be exempt from BSA obligations associated with creating or selling the application or CVC platform, but may still have BSA obligations as a money transmitter if the seller or developer also uses the new application to engage as a business in accepting and transmitting currency, funds, or value that substitutes for currency, or uses the new platform to engage as a business in accepting and transmitting the new CVC. Likewise, an exemption may apply to a person performing a certain role in the development or sale of a software application, while a different person using the same application to accept and transmit currency, funds, or value that substitutes for currency would be still subject to BSA obligations.

    (edited)

    FinCEN’s regulations define the term “money transmitter” to include a “person that provides money transmission services,” or “any other person engaged in the transfer of funds.”7 A “transmittor,” on the other hand, is “[t]he sender of the first transmittal order in a transmittal of funds. The term transmittor includes an originator, except where the transmittor’s financial institution is a financial institution or foreign financial agency other than a bank or foreign bank.”8

    In other words, a

    **transmittor**

    *initiates*

    a transaction that the

    **money transmitter**

    actually

    *executes*

    .

- 🇺🇸-federal-reserve

        Crypto⧉Finance

    🇺🇸-federal-reserve

    4 messages

    

    [https://www.trustnodes.com/2018/07/19/fed-not-jurisdiction-regulatory-authority-cryptocurrencies-says-chairman-powell](https://www.trustnodes.com/2018/07/19/fed-not-jurisdiction-regulatory-authority-cryptocurrencies-says-chairman-powell)

    [Fed Does Not Have Jurisdiction or Regulatory Authority Over Crypto...](https://www.trustnodes.com/2018/07/19/fed-not-jurisdiction-regulatory-authority-cryptocurrencies-says-chairman-powell)

    Jerome Powell, the current Chairman of the Federal Reserve, has stated cryptocurrencies come up a lot at G7 and other international forums, but the Fed doesn’t have any authority over it....

    
    [https://twitter.com/BobMurphyEcon/status/1035314165932548097](https://twitter.com/BobMurphyEcon/status/1035314165932548097)

    
    [Robert P. Murphy (@BobMurphyEcon)](https://twitter.com/BobMurphyEcon)

    Sorry to burst your bubble, folks, but the Fed has never been independent. The president and Senate pick the Board of Governors. C'mon. [https://t.co/gBhQuhLiLF](https://t.co/gBhQuhLiLF)

    Likes

    139

    
    Twitter

    

    [https://twitter.com/moorehn/status/1174167592736579584?s=12](https://twitter.com/moorehn/status/1174167592736579584?s=12)

    
    [Heidi N. Moore (@moorehn)](https://twitter.com/moorehn)

    Okay, for everyone who is confused: Here is a quick-and-dirty explainer thread on the overnight lending market that the Federal Reserve just bailed out.

    Retweets

    1334

    Likes

    3333

    
    Twitter

    

    [https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160#episodeGuid=6f4a33b72f7b4d5da513035a26f9c6b8](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160#episodeGuid=6f4a33b72f7b4d5da513035a26f9c6b8)

- 🇺🇸-cftc

        Crypto⧉Finance

    🇺🇸-cftc

    8 messages

    

    [https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000442687393](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000442687393)

    [‎The What Bitcoin Did Podcast: Brian Quintenz on How the CFTC Re...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000442687393)

    ‎Show The What Bitcoin Did Podcast, Ep Brian Quintenz on How the CFTC Regulates Cryptocurrencies - Jun 25, 2019

    
    

    [https://www.cftc.gov/PressRoom/PressReleases/7820-18](https://www.cftc.gov/PressRoom/PressReleases/7820-18)

    

    [https://www.cftc.gov/PressRoom/SpeechesTestimony/opaquintenz11](https://www.cftc.gov/PressRoom/SpeechesTestimony/opaquintenz11)

    [https://www.cftc.gov/PressRoom/PressReleases/pr7614-17](https://www.cftc.gov/PressRoom/PressReleases/pr7614-17)

    [https://www.cftc.gov/PressRoom/PressReleases/7831-18](https://www.cftc.gov/PressRoom/PressReleases/7831-18)

    

    [https://www.rollcall.com/news/congress/this-government-agency-wants-to-partner-with-fintech-firms-but-a-gift-rule-is-blocking-it](https://www.rollcall.com/news/congress/this-government-agency-wants-to-partner-with-fintech-firms-but-a-gift-rule-is-blocking-it)

    [This government agency wants to partner with fintech firms. But a ...](https://www.rollcall.com/news/congress/this-government-agency-wants-to-partner-with-fintech-firms-but-a-gift-rule-is-blocking-it)

    Rules are slowing down U.S. innovation in fintech, leaving the country to fall behind others, the head of the Commodity Futures Trading Commission warns.

    

    [https://toshitimes.com/cftc-chairman-talks-blockchain-cooperation-needed-as-cftc-is-four-years-behind/](https://toshitimes.com/cftc-chairman-talks-blockchain-cooperation-needed-as-cftc-is-four-years-behind/)

    [CFTC Chairman Talks Blockchain, Cooperation Needed As CFTC is ”F...](https://toshitimes.com/cftc-chairman-talks-blockchain-cooperation-needed-as-cftc-is-four-years-behind/)

    The US Commodity Futures Trading Commission’s chairman, Cristopher Giancarlo, has recently made some waves following blockchain-related comments made at a Congressional public hearing this week. The hearing was dubbed ”Examining the Upcoming Agenda for the CFTC”, and wa...

    
    [https://twitter.com/jchervinsky/status/1045093084374212608](https://twitter.com/jchervinsky/status/1045093084374212608)

    
    [Jake Chervinsky (@jchervinsky)](https://twitter.com/jchervinsky)

    0/ A quick summary of today's ruling in the My Big Coin case. In short, a federal court says *all* digital currencies are commodities & the CFTC has jurisdiction to prosecute fraud & manipulation in crypto. In my view, the ruling is deeply flawed. Thread.[https://t.co/Wsy](https://t.co/Wsy)...

    Likes

    149

    
    Twitter

- 🇺🇸-california

        Crypto⧉Finance

    🇺🇸-california

    3 messages

    

    [https://cointelegraph.com/news/fincen-takes-first-enforcement-action-against-p2p-cryptocurrency-exchanger](https://cointelegraph.com/news/fincen-takes-first-enforcement-action-against-p2p-cryptocurrency-exchanger)

    [FinCEN Takes First Enforcement Action Against P2P Cryptocurrency E...](https://cointelegraph.com/news/fincen-takes-first-enforcement-action-against-p2p-cryptocurrency-exchanger)

    FinCEN has assessed a civil money penalty for a California resident accused of violating money transmission laws.

    
    [https://news.bitcoin.com/california-passes-bill-defining-blockchain-and-crypto-terms/](https://news.bitcoin.com/california-passes-bill-defining-blockchain-and-crypto-terms/)

    [California Passes Bill Defining Blockchain and Crypto Terms - Bitc...](https://news.bitcoin.com/california-passes-bill-defining-blockchain-and-crypto-terms/)

    A draft law designed to amend California’s legislation to create legal grounds for the implementation of crypto-related technologies has been passed by the state’s legislature. Assembly Bill 2658 contains important definitions of some key blockchain and crypto terms.

    
    

    [https://www.sos.ca.gov/administration/regulations/current-regulations/technology/digital-signatures/](https://www.sos.ca.gov/administration/regulations/current-regulations/technology/digital-signatures/) [https://www.sos.ca.gov/administration/regulations/current-regulations/technology/digital-signatures/approved-certification-authorities/](https://www.sos.ca.gov/administration/regulations/current-regulations/technology/digital-signatures/approved-certification-authorities/)

- 🇺🇸-blue-sky-laws

        Crypto⧉Finance

    🇺🇸-blue-sky-laws

    state laws

    7 messages

    

    [https://www.wealthforge.com/insights/why-you-should-be-worrying-about-blue-sky-filings](https://www.wealthforge.com/insights/why-you-should-be-worrying-about-blue-sky-filings)

    [Why You Should Worry About Blue Sky Filings](https://www.wealthforge.com/insights/why-you-should-be-worrying-about-blue-sky-filings)

    Issuers of securities in the United States need to not only worry about registration on a federal level, but also registration and/or filing requirements for each individual state in which securities are to be sold.

    
    

    [https://libraryguides.law.pace.edu/c.php?g=319368&p=2133675](https://libraryguides.law.pace.edu/c.php?g=319368&p=2133675)

    [Research Guides: Securities Law Research Guide: STATE](https://libraryguides.law.pace.edu/c.php?g=319368&p=2133675)

    Statutes, regulations, treatises, and other sources of securities law

    

    [https://medium.com/@bx3capital/state-vs-federal-laws-in-cryptocurrency-blue-sky-or-running-in-the-red-e088cda8c54](https://medium.com/@bx3capital/state-vs-federal-laws-in-cryptocurrency-blue-sky-or-running-in-the-red-e088cda8c54)

    Since the passage of the NSMIA, 92 percent of all private securities offerings greater than $1 million have been conducted under the more daunting Rule 506, which is exempt from blue-sky registration requirements, and not Rules 504 or 505, which remain subject to blue sky requirements. State anti-fraud provisions still apply to all offerings. This demonstrates the clear preference of securities issuers and investors for certainty and consistency in securities regulation, as they flock to SEC safe harbors that require compliance with only one uniform set of rules, and away from those that require compliance with more than 50 separate sets of rules.

    [State vs. Federal Laws in Cryptocurrency: Blue Sky, or Running in ...](https://medium.com/@bx3capital/state-vs-federal-laws-in-cryptocurrency-blue-sky-or-running-in-the-red-e088cda8c54)

    by Philip C. Berg, corporate department chairman and privacy and cybersecurity practice, Otterbourg

    
    [https://www.dobs.pa.gov/Documents/Securities%20Resources/MTA%20Guidance%20for%20Virtual%20Currency%20Businesses.pdf](https://www.dobs.pa.gov/Documents/Securities%2520Resources/MTA%2520Guidance%2520for%2520Virtual%2520Currency%2520Businesses.pdf)

    Money Transmitter Act Guidance for Virtual Currency Businesses The Pennsylvania Department of Banking and Securities (“DoBS”) has received multiple inquiries from entities engaged in various forms of virtual currency exchanges. As the DoBS will not be responding to these requests for guidance on a case-by-case basis, the DoBS is providing the following guidance on the applicability of the Money Transmission Business Licensing Law, otherwise known as the Money Transmitter Act (“MTA”), to virtual currency exchanges.

    [https://www.longhash.com/news/is-cryptocurrency-money-depends-on-your-state](https://www.longhash.com/news/is-cryptocurrency-money-depends-on-your-state)

    [Is Cryptocurrency Money? Depends on Your State](https://www.longhash.com/news/is-cryptocurrency-money-depends-on-your-state)

    Is cryptocurrency money? In America, the question is largely decided by regulators charged with overseeing their state’s money transmitter rules.

    
    

    [https://www.carltonfields.com/insights/publications/2018/state-regulations-on-virtual-currency-and-blockchain-technologies](https://www.carltonfields.com/insights/publications/2018/state-regulations-on-virtual-currency-and-blockchain-technologies)

    
    

    [https://assure.co/blue-sky-notice-filings-do-i-need-to-file/](https://assure.co/blue-sky-notice-filings-do-i-need-to-file/)

    ["Blue Sky Notice Filings: Do I Need to File?" is locked Blue Sky N...](https://assure.co/blue-sky-notice-filings-do-i-need-to-file/)

    Every fund organizer should ask whether their investment fund must comply with state Blue Sky notice filings because most investment fund offerings meet the definition of a "security" for purposes of federal and state disclosure and notice requirements.

- 🇺🇸-bank-secrecy-act

        Crypto⧉Finance

    🇺🇸-bank-secrecy-act

    3 messages

    

    [https://bitaml.com/5th-bsa-compliance-pillar-crypto/](https://bitaml.com/5th-bsa-compliance-pillar-crypto/)

    [Does The BSA's '5th Pillar' Apply To Crypto Businesses? | BitAML](https://bitaml.com/5th-bsa-compliance-pillar-crypto/)

    The BSA's so-called "5th pillar" has created a lot of confusion over how it applies to crypto, if at all. This post explains what it means for crypto businesses.

    
    [https://twitter.com/CaitlinLong_/status/1061700897464561664?s=20](https://twitter.com/CaitlinLong_/status/1061700897464561664?s=20)

    
    [Caitlin Long 🔑 (@CaitlinLong_)](https://twitter.com/CaitlinLong_)

    The #crypto industry was hit hard by after-effects of #operationchokepoint, which was #FDIC's strategy to choke off bank services to legal businesses. This is why #Wyoming's proposed special-purpose bank bill is critical--option to avoid FDIC
    Likes

    155

    
    Twitter

    -[https://coincenter.org/files/e-cash-dex-constitution.pdf](https://coincenter.org/files/e-cash-dex-constitution.pdf) AbstractRegulators, law enforcement, and the general public have come to expect that cryptocurrencytransactions will leave a public record on a blockchain, and that most cryptocurrency exchangeswill take place using centralized businesses that are regulated and surveilled through the BankSecrecy Act. The emergence of electronic cash and decentralized exchange software challengesthese expectations. Transactions need not leave any public record and exchanges can beaccomplished peer to peer without using a regulated third party in between. Faced withdiminished visibility into cryptocurrency transactions, policymakers may propose newapproaches to financial surveillance. Regulating cryptocurrency software developers andindividual users of that software under the Bank Secrecy Act would be unconstitutional underthe Fourth Amendment because it would be a warrantless search and seizure of informationprivate to cryptocurrency users. Furthermore, any law or regulation attempting to ban, requirelicensing for, or compel the altered publication (​e.g.​ backdoors) of cryptocurrency softwarewould be unconstitutional under First Amendment protections for speech.