# 🇨🇭-switzerland

Crypto⧉Finance

🇨🇭-switzerland

8 messages

[FINMA publishes ICO guidelines](https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/)

In guidelines published today, the Swiss Financial Market Supervisory Authority FINMA sets out how it intends to apply financial market legislation in handling enquiries from ICO organisers. The guidelines also define the information FINMA requires to deal with such enquiries...


[https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/](https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/)

[FINMA publishes ICO guidelines](https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/)

In guidelines published today, the Swiss Financial Market Supervisory Authority FINMA sets out how it intends to apply financial market legislation in handling enquiries from ICO organisers. The guidelines also define the information FINMA requires to deal with such enquiries...

[https://news.bitcoin.com/switzerland-considers-granting-crypto-businesses-access-to-banking-services/](https://news.bitcoin.com/switzerland-considers-granting-crypto-businesses-access-to-banking-services/)

[Switzerland Considers Granting Crypto Businesses Access to Banking...](https://news.bitcoin.com/switzerland-considers-granting-crypto-businesses-access-to-banking-services/)

Crypto companies based in Switzerland may receive access to regular banking services as early as this year. Political will and economic wisdom seem to be in place as some government officials and bankers are already working to resolve the issue with Swiss bank accounts for cr...

[https://cointelegraph.com/news/swiss-fintech-license-allows-blockchain-crypto-firms-to-accept-100-mln-in-public-funds](https://cointelegraph.com/news/swiss-fintech-license-allows-blockchain-crypto-firms-to-accept-100-mln-in-public-funds)

[Swiss Fintech License Allows Blockchain, Crypto Firms to Accept $1...](https://cointelegraph.com/news/swiss-fintech-license-allows-blockchain-crypto-firms-to-accept-100-mln-in-public-funds)

Fintech firms including blockchain startups will be able to apply for the new fintech license with Switzerland’s FINMA starting from Jan. 1, 2019.

[https://www.loc.gov/law/help/cryptocurrency/switzerland.php](https://www.loc.gov/law/help/cryptocurrency/switzerland.php)

[Regulation of Cryptocurrency](https://www.loc.gov/law/help/cryptocurrency/switzerland.php)

This report by the Law Library of Congress provides information on the regulation of cryptocurrency in selected jurisdictions.

[https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/switzerland](https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/switzerland)

[International legal business solutions - Global Legal Insights](https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/switzerland)

Provides essential insights into the current legal issues, readers with expert analysis of legal, economic and policy developments with the world's leading lawyers.

[Urs Bolt | bolt.now 🇨🇭 (@UrsBolt)](https://twitter.com/UrsBolt/status/1106490284236595200?s=20)

#Switzerland: FinTech licence and sandbox – update to #FINMA circulars Swiss Financial Market Supervisory Authority (FINMA) allows less extensive auditing for companies with a #FinTech licence. @FINMA_media: [https://t.co/0ahlXhAdSB](https://t.co/0ahlXhAdSB) #banking #regulations #RegTech

Twitter

[https://www.crowdfundinsider.com/2019/06/148940-switzerland-finma-introduces-new-fintech-license-and-sandbox-update/](https://www.crowdfundinsider.com/2019/06/148940-switzerland-finma-introduces-new-fintech-license-and-sandbox-update/)

[Switzerland: FINMA Introduces New Fintech License and Sandbox Update](https://www.crowdfundinsider.com/2019/06/148940-switzerland-finma-introduces-new-fintech-license-and-sandbox-update/)

The Swiss Financial Market Supervisory Authority (FINMA) has introduced a new Fintech license and a 'revision' of the provisions related to their Fintech sandbox ecosystem. FINMA stated that the Swiss parliament has introduced a new licensing category for Fintech companies. S...

- 🇨🇭-finma

        Crypto⧉Finance

    🇨🇭-finma

    4 messages

    

    [https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/](https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/)

    [FINMA publishes ICO guidelines](https://www.finma.ch/en/news/2018/02/20180216-mm-ico-wegleitung/)

    In guidelines published today, the Swiss Financial Market Supervisory Authority FINMA sets out how it intends to apply financial market legislation in handling enquiries from ICO organisers. The guidelines also define the information FINMA requires to deal with such enquiries...

    
    

    [https://webcache.googleusercontent.com/search?q=cache:ZIuTW0tDy7kJ:https://www.finma.ch/en/~/media/finma/dokumente/dokumentencenter/myfinma/faktenblaetter/faktenblatt-virtuelle-waehrungen.pdf%3Fla%3Den+&cd=3&hl=en&ct=clnk&gl=us&client=opera](https://webcache.googleusercontent.com/search?q=cache:ZIuTW0tDy7kJ:https://www.finma.ch/en/~/media/finma/dokumente/dokumentencenter/myfinma/faktenblaetter/faktenblatt-virtuelle-waehrungen.pdf%253Fla%253Den+&cd=3&hl=en&ct=clnk&gl=us&client=opera)

    

    [https://www.finma.ch/en/~/media/finma/dokumente/dokumentencenter/myfinma/1bewilligung/fintech/20180321_ppt-ico-veranstaltung_en.pdf?la=en](https://www.finma.ch/en/~/media/finma/dokumente/dokumentencenter/myfinma/1bewilligung/fintech/20180321_ppt-ico-veranstaltung_en.pdf?la=en)

    [https://webcache.googleusercontent.com/search?q=cache:NadBsEibq-YJ:https://www.finma.ch/en/~/media/finma/dokumente/dokumentencenter/myfinma/1bewilligung/fintech/wegleitung-ico.pdf%3Fla%3Den+&cd=2&hl=en&ct=clnk&gl=us&client=ubuntu](https://webcache.googleusercontent.com/search?q=cache:NadBsEibq-YJ:https://www.finma.ch/en/~/media/finma/dokumente/dokumentencenter/myfinma/1bewilligung/fintech/wegleitung-ico.pdf%253Fla%253Den+&cd=2&hl=en&ct=clnk&gl=us&client=ubuntu)

- 🇨🇭-bis

        Crypto⧉Finance

    🇨🇭-bis

    4 messages

    

    [https://www.bis.org/stability.htm](https://www.bis.org/stability.htm)

    [About committees and associations](https://www.bis.org/stability.htm)

    Overview of the BIS's committees & associations

    
    [https://www.bis.org/publ/bcbs_nl21.htm](https://www.bis.org/publ/bcbs_nl21.htm)

    [Statement on crypto-assets](https://www.bis.org/publ/bcbs_nl21.htm)

    The Basel Committee on Banking Supervision is releasing the newsletter no 21 on "Statement on crypto-assets ", 13 March 2019.

    
    [https://www.bis.org/publ/work765.pdf](https://www.bis.org/publ/work765.pdf) Much of the allure surrounding cryptocurrencies stems from the fact that no government is needed to issue them. And they can be held and traded without a bank account. Instead, they are exchanged via simple technical protocols for communication between participants, as well as a publicly shared ledger of transactions (the “blockchain”) that is updated by a decentralised network of “miners” via costly computations, ie “proof-of-work”.

    

    [https://www.bis.org/review/r190329d.htm](https://www.bis.org/review/r190329d.htm)

    [Yandraduth Googoolye: Cross-border regulation in relation to digit...](https://www.bis.org/review/r190329d.htm)

    Speech by Mr Yandraduth Googoolye, Governor of the Bank of Mauritius, at the ADC Global Blockchain Summit, Adelaide, 27 March 2019.

    ![https://www.bis.org/img/bislogo_og.jpg](https://www.bis.org/img/bislogo_og.jpg)