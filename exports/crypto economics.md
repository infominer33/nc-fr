# 💹-crypto-economics

[A Note on Metcalfe's Law, Externalities and Ecosystem Splits](https://vitalik.ca/general/2017/07/27/metcalfe.html)
Looks like it’s blockchain split season again. For background of various people discussing the topic, and whether such splits are good or bad:

[Asian investors have plenty of cash, a hearty appetite for investm...](https://techcrunch.com/2018/08/15/asian-investors-have-plenty-of-cash-a-hearty-appetite-for-investments-and-a-different-approach-to-doing-deals/)
If you’re being courted by Asian investors, you’ll need to adjust the VCs’ expectations. That can be a challenging task when the parties have different perspectives on appropriate management styles and levels of control.

[Cryptocurrencies – towards a circular economy](https://www.linkedin.com/pulse/cryptocurrencies-towards-circular-economy-titus-capilnean/)

“A local currency is used within a defined area and promotes demand for local goods and services. A multiplier effect occurs as the service or goods provider in turn spends the funds locally, with each reuse strengthening the system, promoting local value, self-sufficiency ...

[Cryptocurrencies are money, not equity](https://tokeneconomy.co/cryptocurrencies-are-money-not-equity-30ff8d0491bb)
Developer incentivization and the power of holders

[Incentive Compatibility on the Blockchain](https://www.bankofcanada.ca/2018/07/staff-working-paper-2018-34/)
A blockchain is a digital ledger that keeps track of a record of ownership without the need for a designated party to update and enforce changes to the record. The updating of the ledger is done directly by the users of the blockchain and is traditionally governed by a proof-...

[https://medium.com/coinshares/half-of-the-remaining-non-minted-bitcoin-supply-is-spoken-for-10df2a45e45d](https://medium.com/coinshares/half-of-the-remaining-non-minted-bitcoin-supply-is-spoken-for-10df2a45e45d) 

[http://www.nber.org/papers/w24717](http://www.nber.org/papers/w24717)

[Asian investors have plenty of cash, a hearty appetite for investm...](https://techcrunch.com/2018/08/15/asian-investors-have-plenty-of-cash-a-hearty-appetite-for-investments-and-a-different-approach-to-doing-deals/)
If you’re being courted by Asian investors, you’ll need to adjust the VCs’ expectations. That can be a challenging task when the parties have different perspectives on appropriate management styles and levels of control.

[The Bear Case for Crypto](https://prestonbyrne.com/2017/09/01/the-bear-case-for-crypto/)
I’m not your lawyer, this is not legal advice. It’s not investment advice, either. Part of my series on ICO Mania. See also The Bear Case for Crypto, Part II: The Great Bank Run; Part I…

[Ryan Selkis (@twobitidiot)](https://twitter.com/twobitidiot/status/1029190840365789186) 
Top 5 crypto theses this year: "Fat Monies" - @arjunblj @MustStopMurad "Shitcoin Waterfall" - @Melt_Dem "Long BTC/Short ETH" - @BMBernstein @TetrasCapital "Fat App, Thin Protocol" - @KyleSamani "CryptoAsset Barbell" - shamelessly, me (I'll fight detractors) Who's mis...

[Bitcoin Economics – Credit expansion and the characteristics of ...](https://blog.bitmex.com/bitcoin-economics-credit-expansion-part-1/)

[Austrian school](https://rationalwiki.org/wiki/Austrian_school)

[Capturing Crypto Value (Part 3/3): Fattening the protocols & dApps](https://hackernoon.com/capturing-crypto-value-part-3-3-fattening-the-protocols-dapps-9a7307b8ca35?gi=85b92b10ba33)

[Vitalik Buterin on Cryptoeconomics and Markets in Everything (Ep. 45)](https://medium.com/conversations-with-tyler/vitalik-buterin-tyler-cowen-cryptocurrency-blockchain-tech-3a2b20c12c97)
Is he secretly the best young economist today?

[Nick Szabo 🔑 (@NickSzabo4)]
George is a fellow OG but this is wrong. Bitcoin is seamlessly global, it does not and need not conform to the ideological speculations ("macroeconomic theories") of particular polities. [https://t.co/XbrNPeTqmN](https://t.co/XbrNPeTqmN)

(https://blog.coinbase.com/the-emergence-of-cryptoeconomic-primitives-14ef3300cc10)

[Rhys Lindmark (@RhysLindmark)](https://twitter.com/RhysLindmark)

1/ #Fomo3d is a variant on a well-studied game theory problem called "entrapment". This is just the start of "Game Theory As A Dark Art." Here's what we can learn from it

[Lamar (@LamarDealMaker)](https://twitter.com/LamarDealMaker)

List of @saifedean's Austrian Economics reading recommendations from his excellent blog post: A Beginner’s Guide to Austrian Economics...

[Hart Lambur (@hal2001)](https://twitter.com/hal2001)

1/ Important paper published by my friend Prof Tonetti (@ChrisTonetti) and his colleague Prof Jones @StanfordGSB. They have rigorously studied the "economics of data" to show that there are large social gains from letting users own and sell their own data. Some key takeawa...

[Bitcoin Economics – Credit expansion and the characteristics of ...](https://blog.bitmex.com/bitcoin-economics-credit-expansion-part-1/)

[Nathaniel Whittemore (@nlw)]

NARRATIVE WATCH: "OPEN FINANCE." If you’ve opened CryptoTwitter in the last few days, you likely seen the phrase “Open Finance,” and based on who you follow, it might be extremely positive or extraordinarily negative. So, what’s going on? Thread

[A Brief Study of Cryptonetwork Forks](https://www.placeholder.vc/blog/2018/9/17/a-brief-study-of-cryptonetwork-forks)

Summary The vast majority of child networks resulting from chain forks are in disuse and have lost significant value relative to their parent networks. Despite lower use metrics, child networks trade at higher user and transaction value multiples (e.g., NVT ratio...

[Charlan Nemeth on In Defense of Troublemakers - Econlib](http://www.econtalk.org/charlan-nemeth-on-in-defense-of-troublemakers/)

Psychologist Charlan Nemeth of the University of California, Berkeley and author of In Defense of Troublemakers talks with EconTalk host Russ Roberts about the ideas in the book–the power of groupthink, the power of conviction, and the opportunity for an authentic, persiste...

[Larry Cermak (@lawmaster)](https://twitter.com/lawmaster)

1/ There is a big misconception that ICO companies have liquidated most of their ETH holdings. In today’s issue of Diar, we looked at all the publicly available ICO treasuries and analyzed the numbers. [https://t.co/EryLsW9Ouj](https://t.co/EryLsW9Ouj)

[A Brief Study of Cryptonetwork Forks](https://www.placeholder.vc/blog/2018/9/17/a-brief-study-of-cryptonetwork-forks)

Summary The vast majority of child networks resulting from chain forks are in disuse and have lost significant value relative to their parent networks. Despite lower use metrics, child networks trade at higher user and transaction value multiples (e.g., NVT ratio...

[Economics back into Cryptoeconomics](https://medium.com/econaut/economics-back-into-cryptoeconomics-20471f5ceeea)

Dick Bryan, Benjamin Lee, Robert Wosnitzer, Akseli Virtanen*

[jpantunes/awesome-cryptoeconomics](https://github.com/jpantunes/awesome-cryptoeconomics/)

An awesome curated list of Cryptoeconomic research and learning materials - jpantunes/awesome-cryptoeconomics

[http://cryptoeconomics.com.au/](http://cryptoeconomics.com.au/) [https://www.tokendaily.co/blog/cryptonetworks-and-the-theory-of-the-firm](https://www.tokendaily.co/blog/cryptonetworks-and-the-theory-of-the-firm) [https://twitter.com/HappyWithCrypto/status/1048128546881908737](https://twitter.com/HappyWithCrypto/status/1048128546881908737) (edited)

[Bitcoin Market-Value-to-Realized-Value (MVRV) Ratio](https://medium.com/@kenoshaking/bitcoin-market-value-to-realized-value-mvrv-ratio-3ebc914dbaee)

Introducing realized cap to BTC market cycle analysis

[Satoshi as the base unit](https://medium.com/@melik_87377/satoshi-as-the-base-unit-561e8934b2ee)

We’ve seen them all Bitcoin, Bitcent, mBTC, Millibit, Milli, µBTC, Microbitcoin, Mike, Bit, Finney, Satoshi, etc. But I fear that these…

[Cryptonetworks and the Theory of the Firm - Token Daily](https://www.tokendaily.co/blog/cryptonetworks-and-the-theory-of-the-firm)

If Austrian economics is the theoretical foundation for Bitcoin, I think Ronald Coase is the inspiration for general cryptonetworks.

[https://www.youtube.com/watch?v=2F3RjzhBY3Q](https://www.youtube.com/watch?v=2F3RjzhBY3Q) [https://cdn.discordapp.com/attachments/477580669686382593/508991211986747402/unknown.png](https://cdn.discordapp.com/attachments/477580669686382593/508991211986747402/unknown.png) [https://www.gwern.net/docs/economics/1688-delavega-confusionofconfusions.pdf](https://www.gwern.net/docs/economics/1688-delavega-confusionofconfusions.pdf)

[Crypto Cast Network](https://www.youtube.com/channel/UCHFL9uTsDbOuBtkhfabU38w)

[Deciphered # 8 - Interview with economist Stephan Livera](https://www.youtube.com/watch?v=2F3RjzhBY3Q)

In this episode we'll be talking with economist Stephan Livera (@stephanlivera) and discussing why #blockchain developers should care about Austrian economic...

[https://stories.platformdesigntoolkit.com/platform-value-chain-z-shape-385f759faffa](https://stories.platformdesigntoolkit.com/platform-value-chain-z-shape-385f759faffa) [https://medium.com/coinmonks/insights-into-the-ethereum-ecosystem-6ffff98e1f0e](https://medium.com/coinmonks/insights-into-the-ethereum-ecosystem-6ffff98e1f0e) [https://www.dashforcenews.com/study-shorting-difficulties-and-heterogeneous-beliefs-drive-speculative-cryptocurrency-prices/](https://www.dashforcenews.com/study-shorting-difficulties-and-heterogeneous-beliefs-drive-speculative-cryptocurrency-prices/)

[Understanding Platforms through Value Chain Maps](https://stories.platformdesigntoolkit.com/platform-value-chain-z-shape-385f759faffa)

Why a Platforms’ Wardley (Value Chain) Map is Z-Shaped

[Insights into the Ethereum ecosystem](https://medium.com/coinmonks/insights-into-the-ethereum-ecosystem-6ffff98e1f0e)

Analysis of the top 250 ERC-20 tokens by market cap

[https://medium.com/@cburniske/productive-capital-in-cryptonetworks-3bad74ad9bfb](https://medium.com/@cburniske/productive-capital-in-cryptonetworks-3bad74ad9bfb) [https://medium.com/@nic__carter/transaction-count-is-an-inferior-measure-fba2d5ac97f1](https://medium.com/@nic__carter/transaction-count-is-an-inferior-measure-fba2d5ac97f1)

[Productive Capital in Cryptonetworks](https://medium.com/@cburniske/productive-capital-in-cryptonetworks-3bad74ad9bfb)

Thus far, cryptonetworks have used their native asset to entice early investment in their economies via two primary pathways:

[Transaction count is an inferior measure](https://medium.com/@nic__carter/transaction-count-is-an-inferior-measure-fba2d5ac97f1)

It is popular to measure Bitcoin by looking at its daily transaction count. This is just one variable in a broader analysis.

[Tuur Demeester (@TuurDemeester)]
Does anyone have a chart or data showing the market caps of aboveground, physical monetary gold vs. silver? Goal is to calculate gold's market dominance over time.

[Listen to the The World Crypto Network Podcast Episode - Gresham's...](https://www.iheart.com/podcast/269-the-world-crypto-network-p-29453174/episode/greshams-law-in-bitcoin-to-the-30329965/)

Stream the The World Crypto Network Podcast episode, Gresham's Law in Bitcoin to the Max, free & on demand on iHeartRadio.

[Hasu (@hasufl)](https://twitter.com/hasufl/status/1142493524279083009?s=12)
What are the best movies (not documentaries) about finance and economics?

[Behavioral Cryptoeconomics: The Secret of Digital Currencies](https://link.medium.com/H2tcviCq9Y)

(or The Additive-only Future: Release the Banana Hostages
[Crypto Words // Bitcoin journal (@_cryptowords)](https://twitter.com/_cryptowords/status/1160964528676495360?s=12)

For the new followers, be sure to check out the CY19 Financial Journal which includes Bitcoin financial models, charts, and methods. Onward

[https://t.co/ZtQdGXyqWc?ssr=true](https://t.co/ZtQdGXyqWc?ssr=true)

[Home · Cryptoeconomics.Study](http://cryptoeconomics.study)
A free, open-source course on the fundamentals of Blockchain protocols

- 💹-walstreet

        Crypto⧉Finance

    💹-walstreet

    4 messages

    

    The Commonwealth Bank of Australia has announced it is creating what it claims is the first bond entirely transferred and managed on the blockchain.

    [The Bitcoin ETF: breaking down the CBOE ETF proposal](https://hackernoon.com/cboebitcoinetf-53992ec38a70)

    
    [Caspian Crypto Trading Platform adds to advisory board](https://www.institutionalassetmanager.co.uk/2018/08/13/267375/caspian-crypto-trading-platform-adds-advisory-board)

    BlockTower Capital co-founder Ari Paul (pictured), has been appointed to the advisory board of Caspian, an institutional-grade crypto trading platform.

    
    [Jamaican Stock Exchange To Allow Crypto Trading](https://www.ethnews.com/jamaican-stock-exchange-to-allow-crypto-trading)

    The move could signal a desire not to be left behind as other Caribbean countries embrace blockchain tech.

    
    
    [Andreas M. Antonopoulos (@aantonop)](https://twitter.com/aantonop)

    Bitcoin Q&A: Why I'm against ETFs [https://t.co/8mQtKJgy0L](https://t.co/8mQtKJgy0L)

    Retweets

    249

    Likes

    650

    
    Twitter

    [Interview: Wall Street Vet Warns of ICE Bringing Bad Banking Pract...](https://www.ccn.com/interview-wall-street-vet-warns-of-ice-bringing-bad-banking-practices-to-crypto/)

    Wall Street veteran Caitlin Long warns that ICE's new bitcoin market could bring bad banking practices to the crypto industry.

    
    
    [Crypto Quantamental (@CryptoQF)](https://twitter.com/CryptoQF)

    Here is the Van Eck CBOE SolidX Bitcoin ETF proposal made to the SEC a week ago. It’s ROCK SOLID. It addresses the SEC concerns head on. Major leagues vs the Winks amateur hour. I’m even more confident in approval. Don’t take my word for it, check it out [https://t.co](https://t.co/)...

    Likes

    153

    
    Twitter

    
    [Caitlin Long 🔑 (@CaitlinLong_)](https://twitter.com/CaitlinLong_)

    Hard forks & rehypothecated #bitcoins don’t mix well-could cause the financial system losses. via @ForbesCrypto [https://t.co/z10fZS5UBE](https://t.co/z10fZS5UBE)

    Likes

    165

    
    Twitter

    [Can Andy Warhol Beat the S&P 500?](https://medium.com/@masterworksio/can-andy-warhol-beat-the-s-p-500-d524f21eec9)

    by Masha Golovina, Director of Market Analysis

    
    
    [Caitlin Long 🔑 (@CaitlinLong_)](https://twitter.com/CaitlinLong_)

    1/ Great morning for a tweetstorm updating the latest on the interaction of #WallSt & #bitcoin, drinking coffee from my new BitMug (thanks AC Fenton
    Likes

    304

    
    Twitter

    [https://podcasts.apple.com/us/podcast/caitlin-long-wall-street-isnt-bitcoins-friend/id1434060078?i=1000418587786](https://podcasts.apple.com/us/podcast/caitlin-long-wall-street-isnt-bitcoins-friend/id1434060078?i=1000418587786)

    
    [https://twitter.com/blockchainmikey/status/1153387829176819712/photo/1](https://twitter.com/blockchainmikey/status/1153387829176819712/photo/1)

    [‎Off the Chain: Caitlin Long: Wall Street Isn't Bitcoin's Friend...](https://podcasts.apple.com/us/podcast/caitlin-long-wall-street-isnt-bitcoins-friend/id1434060078?i=1000418587786)

    ‎Show Off the Chain, Ep Caitlin Long: Wall Street Isn't Bitcoin's Friend - Aug 27, 2018

    
    
    [Crypto Mickey
    @Bakkt @ICE_Markets Awesome

    
    
    Twitter

    

    [http://people.stern.nyu.edu/adamodar/New_Home_Page/valuationtools.html#cf](http://people.stern.nyu.edu/adamodar/New_Home_Page/valuationtools.html#cf)

- 💹-venture-captial

        Crypto⧉Finance

    💹-venture-captial

    4 messages

    

    [https://twitter.com/arcalinea/status/1154939320534331393?s=12](https://twitter.com/arcalinea/status/1154939320534331393?s=12)

    
    [Jay Graber (@arcalinea)](https://twitter.com/arcalinea)

    Never heard the word "operator" as used by VCs until recently. Was amazed there's a name for basically "people who do things." Didn't know there was an alternative?

    
    Twitter

    

    [https://news.ycombinator.com/item?id=20543077](https://news.ycombinator.com/item?id=20543077)

    hhs

    [A history sheds light on venture capital’s ties to military-industrial complex](https://news.ycombinator.com/item?id=20543077)

    
    Hacker News • 27-Jul-19 01:34 PM

    

    [https://twitter.com/jacobkostecki/status/1172312807158665216?s=12](https://twitter.com/jacobkostecki/status/1172312807158665216?s=12)

    
    [Jacob Kostecki (@jacobkostecki)](https://twitter.com/jacobkostecki)

    future token pre-sale token pre-sale token private sale ICO IEO Token sale w/ no pre-sale Token swap from ERC-20 to own testnet 2nd ICO Sale only to investors in Asian countries, not in US Token swap from testnet to mainnet Pre-A (equity) in US with token sale what am I m...

    
    Twitter

    

    [https://twitter.com/mantlecurve/status/1187889966140903424?s=12](https://twitter.com/mantlecurve/status/1187889966140903424?s=12)

    
    [Mithun Madhusoodanan (@mantlecurve)](https://twitter.com/mantlecurve)

    A Visual Introduction to VC Financing [https://t.co/LjzTlquA49](https://t.co/LjzTlquA49)

    
    Twitter

- 💹-token-economics

        Crypto⧉Finance

    💹-token-economics

    2 messages

    

    [https://multicoin.capital/2018/11/09/new-models-for-token-distribution/](https://multicoin.capital/2018/11/09/new-models-for-token-distribution/) [https://medium.com/swlh/tokenomics-is-the-study-of-token-prospects-984a9babda15](https://medium.com/swlh/tokenomics-is-the-study-of-token-prospects-984a9babda15)

    [A new Model for Token Distribution](https://multicoin.capital/2018/11/09/new-models-for-token-distribution/)

    Crypto networks are supposed to be decentralized. As Balaji Srinivasan, CTO of Coinbase, has written, decentralization can be measured in a number of dimensions.

    [Tokenomics Is the Study of Token Prospects](https://medium.com/swlh/tokenomics-is-the-study-of-token-prospects-984a9babda15)

    Tokenomics is the utopian study of a successful project economic model that originated before one-pagers and terms and conditions. It…

    
    [https://ethereumworldnews.com/the-fallacy-high-supply-low-price/](https://ethereumworldnews.com/the-fallacy-high-supply-low-price/)

    [The Fallacy of High Supply, Low Price](https://ethereumworldnews.com/the-fallacy-high-supply-low-price/)

    If Bitcoin is normalized and prices calculated depending on the circulating supply, then it would still be 6.5 Times more valuable than XRP

    
- 💹-investing

        Crypto⧉Finance

    💹-investing

    4 messages

    

    [https://twitter.com/adampaulmoore/status/1154820319191928832?s=12](https://twitter.com/adampaulmoore/status/1154820319191928832?s=12)

    
    [I’m your hodlberry (@AdamPaulMoore)](https://twitter.com/AdamPaulMoore)

    Right now, $100 will buy you around .01 Bitcoin. If we gave out .01 Bitcoin to every person on earth... Psych
    Likes

    149

    
    Twitter

    

    [https://podcasts.apple.com/us/podcast/bitcoin-crypto-trading-ledger-cast/id1322087447?i=1000445255288](https://podcasts.apple.com/us/podcast/bitcoin-crypto-trading-ledger-cast/id1322087447?i=1000445255288)

    [‎Bitcoin & Crypto Trading: Ledger Cast: The appeal to institutio...](https://podcasts.apple.com/us/podcast/bitcoin-crypto-trading-ledger-cast/id1322087447?i=1000445255288)

    ‎Show Bitcoin & Crypto Trading: Ledger Cast, Ep The appeal to institutional crypto investors, with Seed CX's Edward Woodford - Jul 24, 2019

    
    

    [https://twitter.com/BrianNorgard/status/1171792111911309312?s=20](https://twitter.com/BrianNorgard/status/1171792111911309312?s=20)

    
    [Norgard (@BrianNorgard)](https://twitter.com/BrianNorgard)

    Do you want to learn from @naval and @nivi about how to angel invest? Follow this new podcast which open-sources what we teach at @hellospearhead, a fund we created that trains the next generation of angels. [https://t.co/ZS1yzKLiAM](https://t.co/ZS1yzKLiAM)

    Likes

    138

    
    Twitter

    

    [https://www.youtube.com/watch?v=45TVlYsXgCU](https://www.youtube.com/watch?v=45TVlYsXgCU)

    [Epicenter Podcast](https://www.youtube.com/user/epicenterbtc)

    [152 – Tuur Demeester: Investing In Bitcoin](https://www.youtube.com/watch?v=45TVlYsXgCU)

    Support the show, consider donating: 17JDvCdr45fkEppAtJpx8fuCcezvY6WFgg ([http://bit.ly/2dZUdqN)](http://bit.ly/2dZUdqN)) In 2012, a year after discovering Bitcoin in Argentina, Dutch...

    
- 💹-institutional-derivitives

        Crypto⧉Finance

    💹-institutional-derivitives

    1 messages

    

    [https://www.bloomberg.com/news/articles/2018-08-15/lovelorn-u-s-bitcoin-etf-fans-may-find-satisfaction-in-sweden](https://www.bloomberg.com/news/articles/2018-08-15/lovelorn-u-s-bitcoin-etf-fans-may-find-satisfaction-in-sweden) [https://twitter.com/bitcoinist/status/1040499178445721602](https://twitter.com/bitcoinist/status/1040499178445721602) [https://letstalkbitcoin.com/blog/post/ltb-373-bitcoin-etfs-and-the-mainstream-moment](https://letstalkbitcoin.com/blog/post/ltb-373-bitcoin-etfs-and-the-mainstream-moment) [https://globalcoinreport.com/ethereum-futures-wall-street/](https://globalcoinreport.com/ethereum-futures-wall-street/) [https://www.theblockcrypto.com/2018/11/26/traders-are-going-crazy-over-switzerlands-new-exchange-traded-product/](https://www.theblockcrypto.com/2018/11/26/traders-are-going-crazy-over-switzerlands-new-exchange-traded-product/) [https://twitter.com/jchervinsky/status/1072216015801585664](https://twitter.com/jchervinsky/status/1072216015801585664)

    
    [Bitcoinist.com (@bitcoinist)](https://twitter.com/bitcoinist)

    Canadian regulators have approved a regulated Bitcoin trust fund which allows investors to invest in Bitcoin through retirement and savings accounts. [https://t.co/VXbUjIfn83](https://t.co/VXbUjIfn83)

    
    Twitter

    [LTB #373 Bitcoin ETFs and the Mainstream Moment?](https://letstalkbitcoin.com/blog/post/ltb-373-bitcoin-etfs-and-the-mainstream-moment)

    On Todays Episode of Let's Talk Bitcoin... Andreas Antonopoulos, Stephanie Murphy, Jonathan Mohan and Adam B. Levine are joined by returning guest Caitlin Long for an in-depth discussion of the fascination with, and implications of, bitcoin or cryptocurrency ETFs.

    
    [Ethereum Futures Coming To Wall Street](https://globalcoinreport.com/ethereum-futures-wall-street/)

    READ LATER - DOWNLOAD THIS POST AS PDFCrypto backed futures contracts have been the subject of debate ever since the CBOE and CME group started offering Bitcoin (BTC) futures contracts back in mid-December 2017. What happened before the investment instrument went live on Wall...

    

    [Traders are going crazy over Switzerland's new exchange traded pro...](https://www.theblockcrypto.com/2018/11/26/traders-are-going-crazy-over-switzerlands-new-exchange-traded-product/)

    An exchange traded product tracking five of the largest cryptos has become a darling among traders even as the market for digital assets continues to dump. The so-called Amun Crypto Basket ETP began trading on Switzerland’s Six Exchange last week and it now has the highes...

    
- 💹-ico-evaluation

        Crypto⧉Finance

    💹-ico-evaluation

    11 messages

    

    This paper reviews three approaches to valuing Crypto Assets. 1) cost of production, 2) equation of exchange and 3) network value. We then propose a new model, modifying the cost of production. Given the growing popularity and value of the Crypto Asset market, we add to the growing academic and professional research [https://cdn.discordapp.com/attachments/477580669686382593/496216134912311296/TheTokenizationEconomyValuingDigita_preview_1.pdf](https://cdn.discordapp.com/attachments/477580669686382593/496216134912311296/TheTokenizationEconomyValuingDigita_preview_1.pdf)

    

    [https://hackernoon.com/a-framework-for-evaluating-cryptocurrencies-e1b504179848](https://hackernoon.com/a-framework-for-evaluating-cryptocurrencies-e1b504179848) [https://cryptoear.com/](https://cryptoear.com/) -keeps track of twitter mentions [https://jordancooper.blog/2017/05/23/what-i-look-for-in-cryptocoins/](https://jordancooper.blog/2017/05/23/what-i-look-for-in-cryptocoins/) [https://thecontrol.co/our-process-for-evaluating-new-tokens-4627ed97f500](https://thecontrol.co/our-process-for-evaluating-new-tokens-4627ed97f500) [https://twitter.com/paulsbohm/status/1015027146157273088](https://twitter.com/paulsbohm/status/1015027146157273088)

    [https://www.tokendaily.co/blog/the-best-fundamental-indicator-new-inflows](https://www.tokendaily.co/blog/the-best-fundamental-indicator-new-inflows) [https://medium.com/@jbackus/invest-in-the-ugly-duckling-decentralization-product-market-fit-and-the-law-bea856a6bbad](https://medium.com/@jbackus/invest-in-the-ugly-duckling-decentralization-product-market-fit-and-the-law-bea856a6bbad) [https://concourseq.io](https://concourseq.io/)

    [Evaluating Crypto Networks - Token Daily](https://www.tokendaily.co/blog/the-best-fundamental-indicator-new-inflows)

    New inflows: The best fundamental indicator when evaluating crypto networks.

    
    [Invest in the ugly duckling — decentralization, product/market f...](https://medium.com/@jbackus/invest-in-the-ugly-duckling-decentralization-product-market-fit-and-the-law-bea856a6bbad)

    Investing in decentralized technology is different. The legally savvy are unlikely to start by leading the market. Invest in the ugly ducks.

    
    [ConcourseQ | ICO Due Diligence](https://concourseq.io/)

    ConcourseQ is a collaborative due diligence community. Together we research and review icos and blockchain projects.

    
    [https://steemit.com/cryptocurrency/@neoxian/neoxian-s-guide-to-evaluating-a-crypto-currency](https://steemit.com/cryptocurrency/@neoxian/neoxian-s-guide-to-evaluating-a-crypto-currency) [https://www.reddit.com/r/CryptoCurrency/comments/7psz41/whats_in_a_whitepaper_a_detailed_analysis_of_what/](https://www.reddit.com/r/CryptoCurrency/comments/7psz41/whats_in_a_whitepaper_a_detailed_analysis_of_what/) [https://cryptopotato.com/10-keys-evaluating-initial-coin-offering-ico-investments/](https://cryptopotato.com/10-keys-evaluating-initial-coin-offering-ico-investments/) [https://hackernoon.com/evaluating-tokens-and-icos-e6c22c1885bb](https://hackernoon.com/evaluating-tokens-and-icos-e6c22c1885bb)

    [Neoxian's guide to evaluating a crypto-currency — Steemit](https://steemit.com/cryptocurrency/@neoxian/neoxian-s-guide-to-evaluating-a-crypto-currency)

    Hey my Steem friends, looking at some strange new crypto-coin and wondering if you should buy in? Well Old Neoxian is… by neoxian

    
    [r/CryptoCurrency - What's in a whitepaper? A detailed analysis of ...](https://www.reddit.com/r/CryptoCurrency/comments/7psz41/whats_in_a_whitepaper_a_detailed_analysis_of_what/)

    44 votes and 34 comments so far on Reddit

    [Evaluating Tokens and ICOs](https://hackernoon.com/evaluating-tokens-and-icos-e6c22c1885bb)

    
    [https://nulltx.com/why-do-so-many-icos-fail-heres-what-the-experts-say/](https://nulltx.com/why-do-so-many-icos-fail-heres-what-the-experts-say/) [https://captainaltcoin.com/why-most-icos-fail/](https://captainaltcoin.com/why-most-icos-fail/) [https://thechain.media/reasons-why-most-icos-fail](https://thechain.media/reasons-why-most-icos-fail)

    [Why most ICOs fail? | CaptainAltcoin](https://captainaltcoin.com/why-most-icos-fail/)

    According to CoinSchedule.com, there were 235 ICOs in 2017 alone, raising more than $3,7 billion. The peak was in September when it was raised more than $800 million. Top Ten ICOs of 2017 Position Project Total Raised 1 Filecoin $257,000,000 2 Tezos $232,319,985 3 EOS Stage 1...

    
    [Reasons Why Most ICOs Fail](https://thechain.media/reasons-why-most-icos-fail)

    Most ICOs do not actually become profitable—ever. Here's why most ICOs fail, and how to tell if your investment will be the next to die out.

    [https://res.cloudinary.com/jerrick/image/upload/f_auto,fl_progressive,q_auto,c_fill,h_400,w_600/kgzs6qoqvuznz7ajswid](https://res.cloudinary.com/jerrick/image/upload/f_auto,fl_progressive,q_auto,c_fill,h_400,w_600/kgzs6qoqvuznz7ajswid)

    [https://bravenewcoin.com/news/is-tracking-github-activity-a-good-way-to-evaluate-crypto-projects/](https://bravenewcoin.com/news/is-tracking-github-activity-a-good-way-to-evaluate-crypto-projects/) [https://blockgeeks.com/guides/why-most-icos-will-fail/](https://blockgeeks.com/guides/why-most-icos-will-fail/) [https://hackernoon.com/4-primary-reasons-why-icos-fail-43274fd34e2e](https://hackernoon.com/4-primary-reasons-why-icos-fail-43274fd34e2e) [https://coincentral.com/why-are-so-many-icos-failing/](https://coincentral.com/why-are-so-many-icos-failing/) [https://modernconsensus.com/people/innovators/why-most-icos-will-fail-monetago/](https://modernconsensus.com/people/innovators/why-most-icos-will-fail-monetago/)

    [Is tracking GitHub activity a good way to evaluate crypto projects...](https://bravenewcoin.com/news/is-tracking-github-activity-a-good-way-to-evaluate-crypto-projects/)

    In this article, you will be introduced to the idea of tracking GitHub activity as a way to evaluate crypto asset projects and whether this approach merits inclusion in a crypto investor’s arsenal.

    
    [Why Most ICO's Will Fail: A Cold Hard Truth](https://blockgeeks.com/guides/why-most-icos-will-fail/)

    -AMAZONPOLLY-ONLYWORDS-START- In this guide you will learn why most ICO’s Will Fail.. On June 12, 2017, an Ethereum based called Bancor held its ICO. It raised $153 million in 3 hours. No, you are not reading it wrong, 153 million…..in 3 hours
    
    [4 Primary Reasons Why ICOs Fail](https://hackernoon.com/4-primary-reasons-why-icos-fail-43274fd34e2e)

    
    [Why are So Many ICOs Failing? | CoinCentral](https://coincentral.com/why-are-so-many-icos-failing/)

    Why ICOs fail, and some of the key factors to watch out for when investing in them.

    
    [Why most ICOs will fail | Modern Consensus.](https://modernconsensus.com/people/innovators/why-most-icos-will-fail-monetago/)

    Tech companies that do an initial coin offering before they even build their businesses face many problems. Unfortunately for them, issuing tokens adds to those challenges. We chat with the team from Monetago, a blockchain company that never did—and will never do—an initi...

    
    [https://steemit.com/ico/@coinhoarder/most-icos-will-fail](https://steemit.com/ico/@coinhoarder/most-icos-will-fail) [https://medium.com/iconominet/why-icos-fail-1f9530a6d135](https://medium.com/iconominet/why-icos-fail-1f9530a6d135) [https://cryptovest.com/education/5-reasons-your-ico-will-fail-and-how-to-save-it/](https://cryptovest.com/education/5-reasons-your-ico-will-fail-and-how-to-save-it/) [https://hackernoon.com/most-icos-fail-tale-of-two-worlds-d1ab7625ff66](https://hackernoon.com/most-icos-fail-tale-of-two-worlds-d1ab7625ff66) (edited)

    [Most ICOs Will Fail — Steemit](https://steemit.com/ico/@coinhoarder/most-icos-will-fail)

    They say that 90 percent of startups fail. That is an exaggerated statistic, but nonetheless creating a successful… by coinhoarder

    
    [Why ICOs fail](https://medium.com/iconominet/why-icos-fail-1f9530a6d135)

    ICONOMI ICO fundamentals — 2 of 4

    
    [5 Reasons Your ICO Will Fail (and How to Save It) - Cryptovest](https://cryptovest.com/education/5-reasons-your-ico-will-fail-and-how-to-save-it/)

    An ICO is the easiest way to raise money today, but success depends on several factors. Here are 5 reasons your ICO (or any ICO) will fail, and how you can save it.

    
    [Most ICOs Fail: Tale of Two Worlds](https://hackernoon.com/most-icos-fail-tale-of-two-worlds-d1ab7625ff66)

    
    [https://medium.com/@cincinnati/the-ponzico-white-paper-8baa98b19b97](https://medium.com/@cincinnati/the-ponzico-white-paper-8baa98b19b97) [https://hackernoon.com/stay-away-from-icos-which-34ac544e1d49](https://hackernoon.com/stay-away-from-icos-which-34ac544e1d49) [https://twitter.com/NTmoney/status/958381086718902277](https://twitter.com/NTmoney/status/958381086718902277) [https://twitter.com/SarahJamieLewis/status/1010731016456003584](https://twitter.com/SarahJamieLewis/status/1010731016456003584) [https://twitter.com/ReformedBroker/status/1017748548207685632](https://twitter.com/ReformedBroker/status/1017748548207685632)

    [The PonzICO White Paper](https://medium.com/@cincinnati/the-ponzico-white-paper-8baa98b19b97)

    Let’s Just Cut to the Chase

    
    [Stay away from ICOs which…](https://hackernoon.com/stay-away-from-icos-which-34ac544e1d49)

    
    
    [Nick Tomaino (@NTmoney)](https://twitter.com/NTmoney)

    If cryptocurrency prices declined by 90%, would this person still be doing what they are doing? This is a fundamental question to ask yourself when doing business with people in the industry. Unfortunately, the majority of the time the clear answer is no.

    Retweets

    211

    Likes

    967

    
    Twitter

    
    [Sarah Jamie Lewis (@SarahJamieLewis)](https://twitter.com/SarahJamieLewis)

    OK since I got asked this twice today, and multiple times the last few weeks, here is Sarah's Saturday Night guide to telling a bad cryptocurrency whitepaper from a maybe-OK one, with the help of real, awful papers. Ready? Let's begin.

    Retweets

    139

    Likes

    302

    
    Twitter

    
    [Downtown Josh Brown (@ReformedBroker)](https://twitter.com/ReformedBroker)

    I don’t think I would ever invest money with someone who isn’t on Twitter. [https://t.co/pLNmExthbJ](https://t.co/pLNmExthbJ)

    Likes

    371

    
    Twitter

    [https://hackernoon.com/valuing-productive-cryptoassets-89cedad444e6](https://hackernoon.com/valuing-productive-cryptoassets-89cedad444e6) [https://t.co/sgMOn40ZOt](https://t.co/sgMOn40ZOt) [https://twitter.com/DoveyWan/status/1035729838261264384](https://twitter.com/DoveyWan/status/1035729838261264384) [https://twitter.com/adamludwin/status/888168591043969024](https://twitter.com/adamludwin/status/888168591043969024) [https://twitter.com/Melt_Dem/status/1038173464291680256](https://twitter.com/Melt_Dem/status/1038173464291680256)

    [Valuing Productive Cryptoassets](https://hackernoon.com/valuing-productive-cryptoassets-89cedad444e6)

    
    [Here's How You Can Validate An ICO Using Your Chrome Browser](https://t.co/sgMOn40ZOt)

    A new way to protect your ICO investments has launched. Sentinel Protect includes a Chrome browser extention to detect anything suspicious and a community to report back to.

    
    [Dovey Wan 🗝 🦖 (@DoveyWan)](https://twitter.com/DoveyWan)

    You need 5 types of talents to build a sustainable crypto project 1. Phd level cryptographers 2. Solid system engineers 3. Creative economists 4. Interdisciplinary designers 5. Charismatic community leaders There is lots of emphasis on #1 but too LITTLE on the rest...

    Likes

    216

    
    Twitter

    
    [Adam Ludwin (@adamludwin)](https://twitter.com/adamludwin)

    1/THREAD: A few thoughts on “fundamentals analysis” of cryptocurrencies

    Retweets

    505

    Likes

    1131

    
    Twitter

    
    [Meltem Demirors (@Melt_Dem)](https://twitter.com/Melt_Dem)

    1/ in markets, investor psychology is everything. i shared my thoughts on greed, investor psychology, shitcoin, and market cannibalization at @dezentral_io in berlin and wanted to share some of these ideas here... option b: skip directly to slides here: [https://t.co/eMZ8y](https://t.co/eMZ8y)...

    Retweets

    190

    Likes

    566

    
    Twitter

    [https://medium.com/@adilabdulhalim/a-beginners-guide-to-self-evaluating-initial-coin-offerings-icos-8e31cd7e2ac1](https://medium.com/@adilabdulhalim/a-beginners-guide-to-self-evaluating-initial-coin-offerings-icos-8e31cd7e2ac1) [https://www.coindesk.com/beyond-price-why-we-need-a-better-way-to-value-crypto-assets](https://www.coindesk.com/beyond-price-why-we-need-a-better-way-to-value-crypto-assets) [https://coinmetrics.io/realized-capitalization/](https://coinmetrics.io/realized-capitalization/) [https://twitter.com/imbagsy/status/1153468112618041345?s=12](https://twitter.com/imbagsy/status/1153468112618041345?s=12)

    [A Beginner’s Guide to Self Evaluating Initial Coin Offerings (ICOs)](https://medium.com/@adilabdulhalim/a-beginners-guide-to-self-evaluating-initial-coin-offerings-icos-8e31cd7e2ac1)

    Let my past mistakes, useless tokens and lost value be your guide

    

    [Beyond Price: Why We Need a Better Way to Value Crypto Assets - Co...](https://www.coindesk.com/beyond-price-why-we-need-a-better-way-to-value-crypto-assets)

    Institutional investors must set aside dollar-centric valuation models and recognize that value is a different concept in the crypto world.

    

    [Introducing Realized Capitalization - Coin Metrics](https://coinmetrics.io/realized-capitalization/)

    The motivation for the creation of Realized Cap was the realization that “Market Capitalization” is often an empty metric when …

    
    
    [Bagsy (@imBagsy)](https://twitter.com/imBagsy)

    Are there any more gem hunters still in existence? Shill me your favorite FA digger, I want to do some fundamental research tonight.

    
    Twitter

    [https://medium.com/@nlw/complexity-theater-3375a4bdafba](https://medium.com/@nlw/complexity-theater-3375a4bdafba) [https://medium.com/@visionhill_/introducing-a-crypto-benchmark-framework-a873a54a7cfd](https://medium.com/@visionhill_/introducing-a-crypto-benchmark-framework-a873a54a7cfd) [https://twitter.com/danzuller/status/1042854743880724481](https://twitter.com/danzuller/status/1042854743880724481) [https://twitter.com/Mounia_NL/status/1004867878515101696](https://twitter.com/Mounia_NL/status/1004867878515101696) [https://twitter.com/ljin18/status/1049700450495123456](https://twitter.com/ljin18/status/1049700450495123456)

    [Complexity Theater](https://medium.com/@nlw/complexity-theater-3375a4bdafba)

    Complexity theater is when the presentation of ideas is complicated in order to make those ideas seem more valid. It is not limited to the…

    
    [Introducing a Crypto Benchmark Framework](https://medium.com/@visionhill_/introducing-a-crypto-benchmark-framework-a873a54a7cfd)

    TL;DR. The purpose of this piece is to introduce a rough draft framework for benchmarking in the cryptoasset industry. According to Vision…

    
    
    [Dan Zuller (@danzuller)](https://twitter.com/danzuller)

    1/ A thread on benchmarking in crypto. [https://t.co/M4k9NmvNq3](https://t.co/M4k9NmvNq3)

    
    Twitter

    
    [Mounia Rabhi, MSc. 🇳🇱 🇲🇦 (@Mounia_NL)](https://twitter.com/Mounia_NL)

    @crypgfgf Fundamental Analysis

    
    Youtube: @crypto_bobby

    [https://t.co/ILMhHNgtVK](https://t.co/ILMhHNgtVK)

    
    Article: @cryptonator1337

    [https://t.co/1mYN4nvgsZ](https://t.co/1mYN4nvgsZ)

    
    FA-master on Twitter: @Brad2pointO

    
    Twitter

    
    [Li Jin (@ljin18)](https://twitter.com/ljin18)

    Thread: There are over 400 startups trying to be the next Warby Parker, but history shows that 90%+ of e-commerce companies will fail. What separates the successes from the failures? Here are 5 things I look for to figure out if an e-commerce startup is a good opportunity...

    Retweets

    741

    Likes

    3017

    
    Twitter

- 💹-austrian-economics

        Crypto⧉Finance

    💹-austrian-economics

    4 messages

    

    [https://btcmanager.com/bitcoin-foundation-austrian-economics](https://btcmanager.com/bitcoin-foundation-austrian-economics)

    [Bitcoin and its Foundation in Austrian Economics | BTCMANAGER](https://btcmanager.com/bitcoin-foundation-austrian-economics)

    The recently published book “The Bitcoin Standard” by economist Saifedean Ammous makes the case that bitcoin is rooted in the principles of Austrian economics. This view is shared by many bitcoin maximalists who prescribe to this economic school of thought. How Does Bitco...

    
    [https://thesaifhouse.wordpress.com/2018/07/22/a-beginners-guide-to-austrian-economics/](https://thesaifhouse.wordpress.com/2018/07/22/a-beginners-guide-to-austrian-economics/)

    [A Beginner’s Guide to Austrian Economics](https://thesaifhouse.wordpress.com/2018/07/22/a-beginners-guide-to-austrian-economics/)

    For years I had noticed how some of the smartest people I know all tell me that they find economics very difficult to understand. Doctors, engineers, businessmen, and many other highly intelligent …

    
    [https://rationalwiki.org/wiki/Austrian_school](https://rationalwiki.org/wiki/Austrian_school)

    [Austrian school](https://rationalwiki.org/wiki/Austrian_school)

    

    [https://twitter.com/LamarDealMaker/status/1024495681967452161](https://twitter.com/LamarDealMaker/status/1024495681967452161) ^^^ Solid Thread

    
    [Lamar (@LamarDealMaker)](https://twitter.com/LamarDealMaker)

    List of @saifedean's Austrian Economics reading recommendations from his excellent blog post: A Beginner’s Guide to Austrian Economics...

    Likes

    182

    
    Twitter

- 💹-halving

        Crypto⧉Finance

    💹-halving

    1 messages

    

    [https://messari.io/c/research/messari-research-how-observed-halving-dates-differ-from-their-targets](https://messari.io/c/research/messari-research-how-observed-halving-dates-differ-from-their-targets)

- 💹-enterprise

        Crypto⧉Finance

    💹-enterprise

    1 messages

    

    [https://www.idc.com/getdoc.jsp?containerId=prUS44150518](https://www.idc.com/getdoc.jsp?containerId=prUS44150518)

    [Worldwide Spending on Blockchain Forecast to Reach $11.7 Billion i...](https://www.idc.com/getdoc.jsp?containerId=prUS44150518)

    IDC examines consumer markets by devices, applications, networks, and services to provide complete solutions for succeeding in these expanding markets.

    
- 💹-economics

        Crypto⧉Finance

    💹-economics

    2 messages

    

    [https://podcasts.apple.com/us/podcast/the-cryptoconomy-podcast/id1359544516?i=1000442208650](https://podcasts.apple.com/us/podcast/the-cryptoconomy-podcast/id1359544516?i=1000442208650)

    [‎The Cryptoconomy Podcast: CryptoQuikRead_262 - The Island of St...](https://podcasts.apple.com/us/podcast/the-cryptoconomy-podcast/id1359544516?i=1000442208650)

    ‎Show The Cryptoconomy Podcast, Ep CryptoQuikRead_262 - The Island of Stone Money - Jun 20, 2019

    
    

    [https://www.networkforafreesociety.org/wp-content/uploads/2015/04/08_Kasper_EcFreed_Dev.pdf](https://www.networkforafreesociety.org/wp-content/uploads/2015/04/08_Kasper_EcFreed_Dev.pdf)