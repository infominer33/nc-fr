# 💴-stable-coins

Crypto⧉Finance

💴-stable-coins

5 messages

[Berkeley Economics Professor Critiques Stablecoins, Calls Them A ...](https://www.ethnews.com/berkeley-economics-professor-critiques-stablecoins-calls-them-a-myth)

One academic does not buy into the stablecoin trend.

[Avery Carter (@imlevelhead)](https://twitter.com/imlevelhead)

Some stability models briefly explained.

Twitter

[Why 'stable coins' are no answer to bitcoin's instability](https://www.theguardian.com/technology/2018/sep/11/stable-coins-bitcoin-cryptocurrencies-tether)

New cryptocurrencies such as Tether may be pegged to the dollar, but they have big flaws

[Part 3: The Strengths & Weaknesses of Stablecoins](https://medium.com/makerdao/part-3-the-strengths-weaknesses-of-stablecoins-62f13b592e3f)

As the crypto universe expands, the need for stable assets increases. Simple transactions can become incredibly challenging if the…

[Huobi Unveils New ‘All-in-One’ Stablecoin for Stablecoins (Exc...](https://thebitcoinnews.com/huobi-unveils-new-all-in-one-stablecoin-for-stablecoins-except-tether/)

Cryptocurrency exchange Huobi announced today that it has launched its very own interchangeable stablecoin dubbed HUSD. “All-In-One Stablecoin” Huobi, which is currently the third largest cryptocurrency exchange by means of traded volume according to data from CoinMarketC...

[Fiatcoins want to be aggregators, aggregators centralize power](https://www.tonysheng.com/fiatcoins-aggregators)

Fiatcoins (regulated stablecoins) have dominated conversations these last few weeks with Gemini, Paxos, Coinbase and Circle releasing USD-backed stablecoins. As I’ve written previously, while fiatcoins offer substantial benefits over fiat (e.g. programmability, transferabil...

[a16z Podcast: All About Stablecoins - a16z](https://pca.st/YRA6)

with Andy Milenius (@realzandy), Jesse Walden (@j…

[On The (in)Stability of Stablecoins](https://medium.com/@bob.mcelrath/on-the-in-stability-of-stablecoins-517b7d17c3ee)

A “stablecoin” is an attempt to create a synthetic asset that is “more stable” than its underlying constituents. In this article we prove…

[Pomp 🌪 (@APompliano)](https://twitter.com/APompliano)

Winklevoss twins just announced a stablecoin backed by USD. The Ethereum-based token is: - Approved by NY Department of Financial Services - Dollars held in FDIC-insured State Street account - Audited by multiple third-parties at various times The market continues to mat...

Retweets

1580

Likes

5205

Twitter

[Hailey Lennon (@HaileyLennonBTC)](https://twitter.com/HaileyLennonBTC)

Today @GeminiDotCom and @itBit @PaxosGlobal independently announced their own launch of a NYDFS approved stable coin. Interesting development and timing

Twitter

[Berkeley Economics Professor Critiques Stablecoins, Calls Them A ...](https://www.ethnews.com/berkeley-economics-professor-critiques-stablecoins-calls-them-a-myth)

One academic does not buy into the stablecoin trend.

[Avery Carter (@imlevelhead)](https://twitter.com/imlevelhead)

Some stability models briefly explained.

Twitter

[https://www.coinspeaker.com/the-worlds-first-canadian-stable-coin-cadt-in-compliance-with-regulations-for-individual-clients-is-about-to-launch/](https://www.coinspeaker.com/the-worlds-first-canadian-stable-coin-cadt-in-compliance-with-regulations-for-individual-clients-is-about-to-launch/)

[The World’s First Canadian Stable Coin (CADT) in Compliance with...](https://www.coinspeaker.com/the-worlds-first-canadian-stable-coin-cadt-in-compliance-with-regulations-for-individual-clients-is-about-to-launch/)

CADT is the first ever blockchain stable coin in compliance with regulations in the world that is for individual clients.

- 💴-tether

        Crypto⧉Finance

    💴-tether

    5 messages

    

    [Tether's bank said to be 'desperate' for cash | Modern Consensus.](https://modernconsensus.com/cryptocurrencies/tether/noble-funding-cash-tether/)

    Noble may be looking for funding while Tether traders unsuccessfully try to unload large amounts of the coin A Puerto Rico-based startup bank is said to be struggling to survive and that could have repercussions in the cryptocurrency markets. Noble Bank is trying

    
    [Study Finds “No Evidence” of USDT Price Manipulation](https://news.bitcoin.com/study-finds-no-evidence-usdt-price-manipulation/)

    A study examining the popularly suspected correlation between Tether issuance and BTC price movement, undertaken by Wang Chun Wei and published by the University of Queensland, has found that USDT grants do not have a “statistically significant” effect on price fluctuatio...

    

    [Why 'stable coins' are no answer to bitcoin's instability](https://www.theguardian.com/technology/2018/sep/11/stable-coins-bitcoin-cryptocurrencies-tether)

    New cryptocurrencies such as Tether may be pegged to the dollar, but they have big flaws

    
    

    [https://medium.com/coinmonks/did-tether-just-create-a-2-27-billion-monero-like-dark-pool-using-liquid-522178e4e508](https://medium.com/coinmonks/did-tether-just-create-a-2-27-billion-monero-like-dark-pool-using-liquid-522178e4e508)

    [Did Tether Just Create a $2.27 Billion Monero Like Dark Pool using...](https://medium.com/coinmonks/did-tether-just-create-a-2-27-billion-monero-like-dark-pool-using-liquid-522178e4e508)

    I love the crypto space for days like today (July 29th, 2019). I’m just going to go through the day as a simple point by point timeline to…

    
    [https://twitter.com/blockstream/status/1155873715340529665?s=21](https://twitter.com/blockstream/status/1155873715340529665?s=21)

    
    [Blockstream (@Blockstream)](https://twitter.com/Blockstream)

    We're excited to announce the launch of the industry's largest stablecoin, Tether (USDt), on the #LiquidNetwork. As an upgrade to $USDt's original platform, Liquid provides fast settlement times, #ConfidentialTransactions, & robust multisig security.

    
    
    ️

    [https://t.c](https://t.c/)

    ...

    Retweets

    131

    Likes

    354

    
    Twitter

    [https://www.coindesk.com/tether-usdt-russia-china-importers](https://www.coindesk.com/tether-usdt-russia-china-importers)

    [Millions in Crypto Is Crossing the Russia-China Border Daily. Ther...](https://www.coindesk.com/tether-usdt-russia-china-importers)

    Tether has a real-world use case: Chinese importers of cheap goods in Russia use it to send millions home daily.

    
- 💴-dai

        Crypto⧉Finance

    💴-dai

    1 messages

    

    [https://medium.com/bzxnetwork/a-tour-of-the-varieties-of-dai-9ff155f7666c](https://medium.com/bzxnetwork/a-tour-of-the-varieties-of-dai-9ff155f7666c)

    [A Tour Of the Varieties of DAI](https://medium.com/bzxnetwork/a-tour-of-the-varieties-of-dai-9ff155f7666c)

    It can feel impossible to keep up with all the new types of DAI being released. This article is here to help.

    ![https://miro.medium.com/max/1200/1*u_fPkE2LdoXJ8R2aXzDjCA.png](https://miro.medium.com/max/1200/1*u_fPkE2LdoXJ8R2aXzDjCA.png)