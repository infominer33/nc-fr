# 💸-central-bank-digital-currency-cbdc

Crypto⧉Finance

💸-central-bank-digital-currency-cbdc

8 messages

[https://www.imf.org/en/Publications/Staff-Discussion-Notes/Issues/2018/11/13/Casting-Light-on-Central-Bank-Digital-Currencies-46233](https://www.imf.org/en/Publications/Staff-Discussion-Notes/Issues/2018/11/13/Casting-Light-on-Central-Bank-Digital-Currencies-46233) [https://www.imf.org/en/News/Articles/2018/11/13/sp111418-winds-of-change-the-case-for-new-digital-currency](https://www.imf.org/en/News/Articles/2018/11/13/sp111418-winds-of-change-the-case-for-new-digital-currency) [https://twitter.com/jp_koning/status/1071756027396321281](https://twitter.com/jp_koning/status/1071756027396321281) [https://www.coindesk.com/this-brazilian-bank-is-using-ethereum-to-issue-a-stablecoin](https://www.coindesk.com/this-brazilian-bank-is-using-ethereum-to-issue-a-stablecoin) [https://news.hodlhodl.com/news/swiss-startup-to-produce-banknotes-for-marshall-islands-official-cryptocurrency-2809](https://news.hodlhodl.com/news/swiss-startup-to-produce-banknotes-for-marshall-islands-official-cryptocurrency-2809)

[Casting Light on Central Bank Digital Currencies](https://www.imf.org/en/Publications/Staff-Discussion-Notes/Issues/2018/11/13/Casting-Light-on-Central-Bank-Digital-Currencies-46233)

Digitalization is reshaping economic activity, shrinking the role of cash, and spurring new digital forms of money. Central banks have been pondering wheter and how to adapt. One possibility is central bank digital currency (CBDC)-- a widely accessible digital form of fiat mo...

[Winds of Change: The Case for New Digital Currency](https://www.imf.org/en/News/Articles/2018/11/13/sp111418-winds-of-change-the-case-for-new-digital-currency)

By Christine Lagarde, IMF Managing Director Singapore Fintech Festival In Singapore, it is often windy. Winds here bring change, and opportunity. Historically, they blew ships to its port. These resupplied while waiting for the Monsoon to pass, for the seasons to change. Cha...

[JP Koning (@jp_koning)](https://twitter.com/jp_koning)

My paper for @inside_r3 exploring the idea of a Brazilian central bank digital currency is up: [https://t.co/2oHwE5AYNt](https://t.co/2oHwE5AYNt)

Twitter

[This Brazilian Bank Is Using Ethereum to Issue a Stablecoin - CoinDesk](https://www.coindesk.com/this-brazilian-bank-is-using-ethereum-to-issue-a-stablecoin)

The Brazilian National Social Development Bank is to pilot a stablecoin based on ethereum to combat corruption.

[https://research.stlouisfed.org/publications/review/2018/02/13/the-case-for-central-bank-electronic-money-and-the-non-case-for-central-bank-cryptocurrencies/](https://research.stlouisfed.org/publications/review/2018/02/13/the-case-for-central-bank-electronic-money-and-the-non-case-for-central-bank-cryptocurrencies/)

[The Case for Central Bank Electronic Money and the Non-case for Ce...](https://research.stlouisfed.org/publications/review/2018/02/13/the-case-for-central-bank-electronic-money-and-the-non-case-for-central-bank-cryptocurrencies/)

Central banks facilitate transactions but will they take on cryptocurrency?

[https://news.bitcoin.com/imf-pressures-marshall-islands-to-drop-national-crypto/](https://news.bitcoin.com/imf-pressures-marshall-islands-to-drop-national-crypto/)

[IMF Pressures Marshall Islands to Drop National Crypto - Bitcoin News](https://news.bitcoin.com/imf-pressures-marshall-islands-to-drop-national-crypto/)

The International Monetary Fund (IMF) has exerted pressure on the Marshall Islands to torpedo its proposed crypto. The move by the Washington-based global

[https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437367034](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437367034)

[‎Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distr...](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437367034)

‎Show Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distributed Technologies, Ep David Andolfatto: The Impact of Central Bank Digital Currencies on the Banking Sector - Dec 18, 2018

[https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000446782616](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000446782616)

[‎Unchained: Your No-Hype Resource for All Things Crypto: The IMF...](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000446782616)

‎Show Unchained: Your No-Hype Resource for All Things Crypto, Ep The IMF on How to Design Central Bank Digital Currencies - Ep.132 - Aug 13, 2019

-Countries that Have or Are Issuing National or RegionalCryptocurrencies [https://www.loc.gov/law/help/cryptocurrency/map3.pdf](https://www.loc.gov/law/help/cryptocurrency/map3.pdf) [https://i.imgur.com/2ZSZqxI.png](https://i.imgur.com/2ZSZqxI.png)

[https://www.theblockcrypto.com/tiny/rwandan-central-bank-researching-official-digital-currency/?utm_source=newsletter&utm_medium=email&utm_campaign=2019-08-22](https://www.theblockcrypto.com/tiny/rwandan-central-bank-researching-official-digital-currency/?utm_source=newsletter&utm_medium=email&utm_campaign=2019-08-22)

[Rwanda's central bank researching official digital currency - The ...](https://www.theblockcrypto.com/tiny/rwandan-central-bank-researching-official-digital-currency/?utm_source=newsletter&utm_medium=email&utm_campaign=2019-08-22)

The National Bank of Rwanda, Rwanda’s central bank, is researching ways to issue its own digital currency, according to a report by Bloomberg. The central bank plans to use its digital currency to improve transaction processing and boost economic growth. According to Peace ...

[Bitcoin (@Charts4bitcoin)](https://twitter.com/charts4bitcoin/status/1188726401358163968?s=12)

G. Central Bank Digital Currency 70. Most central banks are considering non-anonymous CBDC......Several focusing research on two-pronged approach with anonymous tokens for small holdings/transactions, & traceable currency for large ones. #IMF #DigitalCurrency #FinTech #Blo...

Twitter

- 💸-china-cbdc

        Crypto⧉Finance

    💸-china-cbdc

    3 messages

    

    [https://www.theblockcrypto.com/tiny/chinas-central-bank-digital-currency-is-ready-after-5-years-of-development/](https://www.theblockcrypto.com/tiny/chinas-central-bank-digital-currency-is-ready-after-5-years-of-development/)

    [China's central bank digital currency is "ready" after 5 years of ...](https://www.theblockcrypto.com/tiny/chinas-central-bank-digital-currency-is-ready-after-5-years-of-development/)

    A senior official at China’s central bank announced at the China Finance 40 Group meeting today that the country will soon roll out its central bank digital currency (CBDC.) Mu Changchun, Deputy Chief in the Payment and Settlement Division of the People’s Bank of China (P...

    
    [http://www.chinadaily.com.cn/a/201907/09/WS5d239217a3105895c2e7c56f.html](http://www.chinadaily.com.cn/a/201907/09/WS5d239217a3105895c2e7c56f.html)

    [Central bank unveils plan on digital currency - Chinadaily.com.cn](http://www.chinadaily.com.cn/a/201907/09/WS5d239217a3105895c2e7c56f.html)

    The central bank is accelerating its efforts to introduce a government-backed digital currency, aiming at securing a cutting-edge position in the global cryptocurrency race.

    
    [https://medium.com/swlh/facebook-spur-china-on-to-look-at-their-own-digital-currency-d53b82d3f5c5](https://medium.com/swlh/facebook-spur-china-on-to-look-at-their-own-digital-currency-d53b82d3f5c5)

    [Facebook spur China on to look at their own Digital Currency](https://medium.com/swlh/facebook-spur-china-on-to-look-at-their-own-digital-currency-d53b82d3f5c5)

    With the announcement of Libra, Facebook’s new Digital Currency, it was reported in The South China Morning Post that Wang Xin — a…

    ![https://miro.medium.com/max/1200/1*1akxstnwsCzjTUR83UNzhQ.png](https://miro.medium.com/max/1200/1*1akxstnwsCzjTUR83UNzhQ.png)