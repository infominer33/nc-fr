# 🗺-regulation

Crypto⧉Finance

🗺-regulation

19 messages

[https://medium.com/@craig_10243/defining-smart-contracts-eb31fd825de6](https://medium.com/@craig_10243/defining-smart-contracts-eb31fd825de6) (edited)

[Defining smart contracts](https://medium.com/@craig_10243/defining-smart-contracts-eb31fd825de6)

When contrasting contractual principles, it is clear that where a contract is not required to be in writing (Columbia Law Review, Apr…

[https://twitter.com/Conor0Hanlon/status/1045226934391599104](https://twitter.com/Conor0Hanlon/status/1045226934391599104) [https://docs.google.com/document/d/1jm9t61JnqLqsV50cD9Mzwyu8cXWRmVFaI2XK-UaFggk/edit](https://docs.google.com/document/d/1jm9t61JnqLqsV50cD9Mzwyu8cXWRmVFaI2XK-UaFggk/edit) [https://medium.com/@blockarray/smart-contracts-dumb-signatures-why-blockchain-based-signatures-may-not-be-legal-in-your-efb64bc030c0](https://medium.com/@blockarray/smart-contracts-dumb-signatures-why-blockchain-based-signatures-may-not-be-legal-in-your-efb64bc030c0) (edited)

[http://www.fsb.org/wp-content/uploads/P101018.pdf](http://www.fsb.org/wp-content/uploads/P101018.pdf)

Crypto-assets,1 such as bitcoin2 and ether, reached an estimated total market capitalisation of $830 billion on 8 January 2018, before falling sharply in subsequent months. These markets remain small compared to the global financial system, and crypto-assets are not yet widely used for financial transactions, but markets are changing rapidly. The growth of crypto-asset trading platforms (often misleadingly called ‘exchanges’);3 the introduction of new financial products (such as crypto-asset funds and trusts and exchange-traded products); and the growing interest by retail investors, raise questions about the implications of crypto-assets for financial stability.

(edited)

[http://www.sisudoc.org/samples_by_filetype/html/autonomy_markup0.en.html#26](http://www.sisudoc.org/samples_by_filetype/html/autonomy_markup0.en.html#26) [https://medium.com/@blockchainlaw_PC/cryptocurrency-derivatives-funds-and-advisers-key-considerations-under-u-s-6ab6718115c1](https://medium.com/@blockchainlaw_PC/cryptocurrency-derivatives-funds-and-advisers-key-considerations-under-u-s-6ab6718115c1) (edited)

[Cryptocurrency Derivatives, Funds and Advisers: Key Considerations...](https://medium.com/@blockchainlaw_PC/cryptocurrency-derivatives-funds-and-advisers-key-considerations-under-u-s-6ab6718115c1)

By Andrew P. Cross on September 14, 2018 Posted in Bitcoin, Blockchain and Digital Currencies

[https://twitter.com/jchervinsky/status/1045093691537395712](https://twitter.com/jchervinsky/status/1045093691537395712) [https://twitter.com/Conor0Hanlon/status/1045226939441508352](https://twitter.com/Conor0Hanlon/status/1045226939441508352) [https://medium.com/@cburniske/cryptoassets-flow-amplification-reflexivity-7e306815dd8c](https://medium.com/@cburniske/cryptoassets-flow-amplification-reflexivity-7e306815dd8c) (edited)

[https://twitter.com/dweinberger/status/1057025410276450304](https://twitter.com/dweinberger/status/1057025410276450304) (edited)

[https://toshitimes.com/ukraine-cryptocurrency-regulations/](https://toshitimes.com/ukraine-cryptocurrency-regulations/) [https://www.tonysheng.com/katherine-wu-regulation-crypto](https://www.tonysheng.com/katherine-wu-regulation-crypto) (edited)

[Ukraine to Legalise and Regulate Cryptocurrencies](https://toshitimes.com/ukraine-cryptocurrency-regulations/)

The Ukrainian government confirmed their plans to establish a regulation to legalize cryptos in the region, according to an official statement. The Ministry of Economic Development and trade have initiated a state policy in the field of digital assets.

[Katherine Wu on regulation in crypto, and the cost of grey areas](https://www.tonysheng.com/katherine-wu-regulation-crypto)

Tonysheng.com is supported by the generosity of its members. In addition funding the free weekly essay (published on Mondays), members receive an additional members-only analysis that is timely and substantial and transcripts to interviews (like this one

[r/Bitcoin - What a landmark legal case from mid-1700s Scotland tel...](https://www.reddit.com/r/Bitcoin/comments/1qomqt/what_a_landmark_legal_case_from_mid1700s_scotland/?st=JP2M5KCP&sh=c065a482)

451 votes and 101 comments so far on Reddit

[Ben Hunt (@EpsilonTheory)](https://twitter.com/EpsilonTheory)

Proposal to set up “exchange rate” between cash and e-cash, so that negative interest rates on e-cash hit your cash when spent. They’re. Not. Even. Pretending. Anymore. [https://t.co/7WominabmP](https://t.co/7WominabmP)

Retweets

244

Likes

585

Twitter

[John McAfee (@officialmcafee)](https://twitter.com/officialmcafee)

Bitcoin mixers are now being targeted. Anonymity itself is slowly being considered a crime. The word "Privacy" will soon mean "Criminal Intent". [https://t.co/BN9eM3yozb](https://t.co/BN9eM3yozb)

Retweets

181

Likes

570

Twitter

[https://twitter.com/coinyeezy/status/1150831522339012610?s=12](https://twitter.com/coinyeezy/status/1150831522339012610?s=12) [https://twitter.com/zndtoshi/status/1152153130655715328?s=20](https://twitter.com/zndtoshi/status/1152153130655715328?s=20) [https://twitter.com/angela_walch/status/1152054148071866368?s=20](https://twitter.com/angela_walch/status/1152054148071866368?s=20) (edited)

[Jason Somensatto (@jasonsomensatto)](https://twitter.com/jasonsomensatto/status/1154750361078566912?s=12)

I’m starting to think dedicated crypto legislation may be necessary. Observing lawyers all come up with divergent (and sometimes crazy) legal theories suggests to me there is little consensus on how the law applies to this industry.

Twitter

[https://www.coindesk.com/7-legal-questions-that-will-define-blockchain-in-2019](https://www.coindesk.com/7-legal-questions-that-will-define-blockchain-in-2019)

[7 Legal Questions That Will Define Blockchain in 2019 - CoinDesk](https://www.coindesk.com/7-legal-questions-that-will-define-blockchain-in-2019)

In this exclusive op-ed, Jenny Leung, an Australian attorney who formerly worked for the country's financial regulator, explained 7 legal questions that will define blockchain this year.

[https://www.acc.com/sites/default/files/resources/vl/membersonly/Article/1489775_1.pdf](https://www.acc.com/sites/default/files/resources/vl/membersonly/Article/1489775_1.pdf)

[Simon Taylor (@sytaylor)](https://twitter.com/sytaylor/status/1026155964481851392)

I wrote a thing

Why I think policy makers don't have to fear decentralised exchanges (the devil is in the detail) cc @HectorRosekrans @DistribLedgers @GlobalDigitalFi @lwintermeyer @Melt_Dem @mayazi @NoelleInMadrid

[https://t.co/1Pi6Mn3Yb4](https://t.co/1Pi6Mn3Yb4)

Twitter

[https://research.circle.com/wp-content/uploads/2019/04/Circle-Research-1Q19-Retrospective-1.pdf](https://research.circle.com/wp-content/uploads/2019/04/Circle-Research-1Q19-Retrospective-1.pdf)

[https://openlaw.io/](https://openlaw.io/)

[OpenLaw — A free legal repository](https://openlaw.io/)

We are home to a passionate group of people, technologists, and dreamers committed to rebuilding the legal industry.

What Blockchain products require identity verification? – Blockchain Support Center [https://support.blockchain.com/hc/en-us/articles/360018359871-What-Blockchain-products-require-identity-verification-](https://support.blockchain.com/hc/en-us/articles/360018359871-What-Blockchain-products-require-identity-verification-)

[What Blockchain products require identity verification?](https://support.blockchain.com/hc/en-us/articles/360018359871-What-Blockchain-products-require-identity-verification-)

Identity verification is required in order to use Swap, our non-custodial crypto-to-crypto exchange within the Blockchain Wallet, and for The PIT (learn more about this here). Once verified, you wi...

[https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000448169635](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000448169635)

[‎The What Bitcoin Did Podcast: Bill Barhydt on Libra: The Dawn o...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000448169635)

‎Show The What Bitcoin Did Podcast, Ep Bill Barhydt on Libra: The Dawn of Corporate Money - Aug 30, 2019

[https://commons.wikimedia.org/wiki/File:Legal_status_of_bitcoin_(new).png](https://commons.wikimedia.org/wiki/File:Legal_status_of_bitcoin_(new).png)

[File:Legal status of bitcoin (new).png](https://commons.wikimedia.org/wiki/File:Legal_status_of_bitcoin_(new).png)

- 🗺-global-regulation

        Crypto⧉Finance

    🗺-global-regulation

    7 messages

    

    ?? Legal Aspects of Blockchain UNOPS Attachment file type: acrobat Attachment-1012901.pdf 4.14 MB -Regulation of Cryptocurrency Around the World [https://www.loc.gov/law/help/cryptocurrency/world-survey.php](https://www.loc.gov/law/help/cryptocurrency/world-survey.php) -Full Report [https://www.loc.gov/law/help/cryptocurrency/cryptocurrency-world-survey.pdf](https://www.loc.gov/law/help/cryptocurrency/cryptocurrency-world-survey.pdf) -Legal Status PDF [https://www.loc.gov/law/help/cryptocurrency/map1.pdf](https://www.loc.gov/law/help/cryptocurrency/map1.pdf) [https://imgur.com/8dbkEoH.png](https://imgur.com/8dbkEoH.png) Regulatory Framework -[https://www.loc.gov/law/help/cryptocurrency/map2.pdf](https://www.loc.gov/law/help/cryptocurrency/map2.pdf) [https://i.imgur.com/Egh6Ms9.png](https://i.imgur.com/Egh6Ms9.png) -Regulation of Cryptocurrencies in Selected Jurisdictions [https://www.loc.gov/law/help/cryptocurrency/regulation-of-cryptocurrency.pdf](https://www.loc.gov/law/help/cryptocurrency/regulation-of-cryptocurrency.pdf) * Argentina * Australia * Belarus * Brazil * Canada * China * France * Gibraltar * Iran * Israel * Japan * Jersey * Mexico * Switzerland (edited)

    [Regulation of Cryptocurrency Around the World](https://www.loc.gov/law/help/cryptocurrency/world-survey.php)

    This report by the Law Library of Congress provides information on the regulation of cryptocurrency in selected jurisdictions and around the world.

    [Screenshot](https://imgur.com/8dbkEoH)

    
    [https://coindoo.com/top-countries-where-cryptocurrency-is-illegal/](https://coindoo.com/top-countries-where-cryptocurrency-is-illegal/) [https://twitter.com/raewkl/status/1059871124882739201](https://twitter.com/raewkl/status/1059871124882739201) [https://twitter.com/digime/status/1075685750493143040](https://twitter.com/digime/status/1075685750493143040) (edited)

    [Top Countries Where Cryptocurrency is Illegal - Coindoo](https://coindoo.com/top-countries-where-cryptocurrency-is-illegal/)

    The use of cryptos is often associated with illegal or criminal transactions, and many governments cite this as being the reason for their ban.

    
    
    [Rachel (@raewkl)](https://twitter.com/raewkl)

    1/ Such a great panel on the creation, evolution, and application of regulation with @MoadFahmi @BermudaMonetary, Kristin Boggiano @AlphaPointLive, Commissioner Robert Jackson Jr @SEC_News and @kerstpe of Digital Finance Europe, moderated by Aditya Narain @IMFNews #dcfinte...

    
    Twitter

    
    [digi.me (@digime)](https://twitter.com/digime)

    US, China and Europe have different data laws — that could be a big headache for companies in 2019 [https://t.co/QTyiIyT2Oz](https://t.co/QTyiIyT2Oz)

    
    Twitter

    

    [https://www.cnbc.com/2018/03/27/a-complete-guide-to-cyprocurrency-regulations-around-the-world.html](https://www.cnbc.com/2018/03/27/a-complete-guide-to-cyprocurrency-regulations-around-the-world.html) [https://thenextweb.com/hardfork/2018/04/27/cryptocurrency-regulation-2018-world-stands-right-now/](https://thenextweb.com/hardfork/2018/04/27/cryptocurrency-regulation-2018-world-stands-right-now/)

    [Your guide to cryptocurrency regulations around the world and wher...](https://www.cnbc.com/2018/03/27/a-complete-guide-to-cyprocurrency-regulations-around-the-world.html)

    As the demand for cryptocurrency grows, global regulators are divided on the best way to police them.

    
    [Cryptocurrency regulation in 2018: Where the world stands right now](https://thenextweb.com/hardfork/2018/04/27/cryptocurrency-regulation-2018-world-stands-right-now/)

    [https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2018%2F04%2F234567654321.jpg&signature=4302be7b238397e886ec33480525de13](https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2018%2F04%2F234567654321.jpg&signature=4302be7b238397e886ec33480525de13)

    [https://bitfalls.com/2018/08/31/cryptocurrency-regulation-across-the-world/](https://bitfalls.com/2018/08/31/cryptocurrency-regulation-across-the-world/)

    [Cryptocurrency Regulation across the World: September 2018 - Bitfalls](https://bitfalls.com/2018/08/31/cryptocurrency-regulation-across-the-world/)

    This post is a brief list of cryptocurrency regulation status in various countries around the world - who likes them, who wants the criminalize them, etc.

    
    

    [https://www.bis.org/publ/qtrpdf/r_qt1809f.htm](https://www.bis.org/publ/qtrpdf/r_qt1809f.htm)

    [Regulating cryptocurrencies: assessing market reactions](https://www.bis.org/publ/qtrpdf/r_qt1809f.htm)

    Part 4 of "International banking and financial market developments" (BIS Quarterly Review), September 2018, by Raphael Auer and Stijn Claessens. Cryptocurrencies are often thought to operate out of the reach of national regulation, but in fact their valuations, transaction vo...

    
    

    [https://coincentral.com/bitcoin-legal-issues/](https://coincentral.com/bitcoin-legal-issues/)

    [Bitcoin Legal Issues | Important Cases You Should Watch](https://coincentral.com/bitcoin-legal-issues/)

    Check out the important Bitcoin legal issues that may reshape the market and shed light on some long sought after questions soon.

    
    

    [https://twitter.com/itifdc/status/1188423849500692482?s=12](https://twitter.com/itifdc/status/1188423849500692482?s=12)

    
    [ITIF (@ITIFdc)](https://twitter.com/ITIFdc)

    The United States and China have fundamentally different approaches to governance of the digital economy. [https://t.co/tBZfOWUU0q](https://t.co/tBZfOWUU0q)

    
    Twitter

- 🗺-where-to-locate

        Crypto⧉Finance

    🗺-where-to-locate

    4 messages

    

    [https://flagtheory.com/where-to-start-your-cryptocurrency-business/](https://flagtheory.com/where-to-start-your-cryptocurrency-business/)

    [Where to start your cryptocurrency business - Flag Theory](https://flagtheory.com/where-to-start-your-cryptocurrency-business/)

    The blockchain space will see increased regulation and government oversight as tokenizing assets is a trend that will not go away. For countries that get it right, this is an opportunity to create value and boost their economies. Learn what are the best jurisdictions to start...

    
    [https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html](https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html)

    SAN FRANCISCO — Hedge funds go to the Cayman Islands to incorporate. Big companies are generally domiciled in Delaware. And online poker companies often set up their bases in Gibraltar and Malta. Now the race is on to become the go-to destination for cryptocurrency companies that are looking for shelter from regulatory uncertainty in the United States and Asia. In small countries and territories including Bermuda, Malta, Gibraltar and Liechtenstein, officials have recently passed laws, or have legislation in the works, to make themselves more welcoming to cryptocurrency companies and projects. In Malta, the government passed three laws on July 4 so companies can easily issue new cryptocurrencies and trade existing ones. In Bermuda this year, the legislature passed a law that lets start-ups doing initial coin offerings apply to the minister of finance for speedy approval.

    [https://hackernoon.com/pros-cons-how-do-you-choose-which-crypto-friendly-jurisdiction-to-start-your-company-in-df4e0b239aa8](https://hackernoon.com/pros-cons-how-do-you-choose-which-crypto-friendly-jurisdiction-to-start-your-company-in-df4e0b239aa8)

    By NATHANIEL POPPER

    [Have a Cryptocurrency Company? Bermuda, Malta or Gibraltar Wants You](https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html)

    With their eyes on blockchain jobs and revenue, small countries and territories are competing to become the go-to destinations for entrepreneurs and projects.

    
    [Pros & Cons: How do you choose which crypto-friendly jurisdiction ...](https://hackernoon.com/pros-cons-how-do-you-choose-which-crypto-friendly-jurisdiction-to-start-your-company-in-df4e0b239aa8)

    
    

    [http://premieroffshore.com/where-to-start-a-cryptocurrency-exchange-crypto-friendly-countries/](http://premieroffshore.com/where-to-start-a-cryptocurrency-exchange-crypto-friendly-countries/)

    [Where to Start a Cryptocurrency Exchange - Crypto Friendly Countri...](http://premieroffshore.com/where-to-start-a-cryptocurrency-exchange-crypto-friendly-countries/)

    In this article, I will focus on where to start a cryptocurrency exchange in 2018. That is, where to incorporate a new cryptocurrency exchange. Which countries are friendly to startup cryptocurrency exchanges and why you should consider each based on your business model. Coun...

    
    

    [https://flagtheory.com/where-to-start-your-cryptocurrency-business/](https://flagtheory.com/where-to-start-your-cryptocurrency-business/)

    [Where to start your cryptocurrency business - Flag Theory](https://flagtheory.com/where-to-start-your-cryptocurrency-business/)

    The blockchain space will see increased regulation and government oversight as tokenizing assets is a trend that will not go away. For countries that get it right, this is an opportunity to create value and boost their economies. Learn what are the best jurisdictions to start...

    
- 🗺-g20

        Crypto⧉Finance

    🗺-g20

    4 messages

    

    ### G20 * [When Is a Bitcoin Not a Bitcoin? When It's an Asset, Says G-20](https://www.bloomberg.com/news/articles/2018-03-20/when-is-a-bitcoin-not-a-bitcoin-when-it-s-an-asset-says-g-20) * [FATF Report to the G20 Finance Ministers and Central Bank Governors](http://www.fatf-gafi.org/media/fatf/documents/reports/FATF-Report-G20-FM-CBG-July-2018.pdf) * [To G20 Finance Ministers and Central Bank Governors](https://www.fsb.org/wp-content/uploads/P180318.pdf)

    [https://www.coindesk.com/g20-reaffirms-it-will-apply-expected-tough-new-fatf-rules-on-crypto](https://www.coindesk.com/g20-reaffirms-it-will-apply-expected-tough-new-fatf-rules-on-crypto)

    [G20 Reaffirms It Will Apply Expected Tough New FATF Rules on Crypt...](https://www.coindesk.com/g20-reaffirms-it-will-apply-expected-tough-new-fatf-rules-on-crypto)

    The G20 has reaffirmed it will apply standards to counter money laundering and terrorism funding, soon to be finalized by the Financial Action Task Force.

    
    [https://www.fsb.org/2019/05/crypto-assets-work-underway-regulatory-approaches-and-potential-gaps/](https://www.fsb.org/2019/05/crypto-assets-work-underway-regulatory-approaches-and-potential-gaps/)

    [Crypto-assets: Work underway, regulatory approaches and potential gaps](https://www.fsb.org/2019/05/crypto-assets-work-underway-regulatory-approaches-and-potential-gaps/)

    Report to the G20 on work to address risks from crypto-assets.

    
    

    [https://www.g20.org/sites/default/files/documentos_producidos/crypto-assets_report_on_work_by_the_fsb_and_standard-setting_bodies_fsb_1.pdf](https://www.g20.org/sites/default/files/documentos_producidos/crypto-assets_report_on_work_by_the_fsb_and_standard-setting_bodies_fsb_1.pdf) (edited)

- 🗺-fatf-financial-action-task-force

        Crypto⧉Finance

    🗺-fatf-financial-action-task-force

    11 messages

    

    [https://www.coindesk.com/g20-reaffirms-it-will-apply-expected-tough-new-fatf-rules-on-crypto](https://www.coindesk.com/g20-reaffirms-it-will-apply-expected-tough-new-fatf-rules-on-crypto)

    [G20 Reaffirms It Will Apply Expected Tough New FATF Rules on Crypt...](https://www.coindesk.com/g20-reaffirms-it-will-apply-expected-tough-new-fatf-rules-on-crypto)

    The G20 has reaffirmed it will apply standards to counter money laundering and terrorism funding, soon to be finalized by the Financial Action Task Force.

    
    FATF Report to the G20 Finance Ministers and Central Bank Governors —July 2018 [http://www.fatf-gafi.org/media/fatf/documents/reports/FATF-Report-G20-FM-CBG-July-2018.pdf](http://www.fatf-gafi.org/media/fatf/documents/reports/FATF-Report-G20-FM-CBG-July-2018.pdf) (edited)

    [http://www.fatf-gafi.org/publications/fatfrecommendations/documents/regulation-virtual-assets.html](http://www.fatf-gafi.org/publications/fatfrecommendations/documents/regulation-virtual-assets.html)

    [http://www.fatf-gafi.org/publications/fatfrecommendations/documents/guidance-rba-virtual-assets.html](http://www.fatf-gafi.org/publications/fatfrecommendations/documents/guidance-rba-virtual-assets.html)

    

    [http://www.fatf-gafi.org/media/fatf/documents/reports/Virtual-currency-key-definitions-and-potential-aml-cft-risks.pdf](http://www.fatf-gafi.org/media/fatf/documents/reports/Virtual-currency-key-definitions-and-potential-aml-cft-risks.pdf) (2014)

    

    [https://www.coindesk.com/planned-solution-for-fatf-travel-rule-compliance-keeps-user-data-private](https://www.coindesk.com/planned-solution-for-fatf-travel-rule-compliance-keeps-user-data-private) FATF gave member countries 12 months to adopt the guidelines, with a review set for June 2020.

    [Blockchain Solution for FATF 'Travel Rule' to Keep User Data Priva...](https://www.coindesk.com/planned-solution-for-fatf-travel-rule-compliance-keeps-user-data-private)

    CipherTrace is teaming with Shyft on a blockchain solution to help crypto firms meet tough new standards from the Financial Action Task Force.

    
    

    [https://www.coindesk.com/the-cat-and-mouse-game-of-crypto-regulation-enters-a-new-phase](https://www.coindesk.com/the-cat-and-mouse-game-of-crypto-regulation-enters-a-new-phase)

    [The Cat-and-Mouse Game of Crypto Regulation Enters a New Phase - C...](https://www.coindesk.com/the-cat-and-mouse-game-of-crypto-regulation-enters-a-new-phase)

    The cat-and-mouse game between regulators and crypto developers could spur a new era of innovation around the technology, writes Michael J. Casey.

    
    

    [https://www.ico.li/fatf-publishes-new-crypto-guidelines/](https://www.ico.li/fatf-publishes-new-crypto-guidelines/)

    [FATF Publishes New Crypto Guidelines – Threat or Opportunity? - ...](https://www.ico.li/fatf-publishes-new-crypto-guidelines/)

    The ICO and Blockchain Blog in Liechtenstein. Don't miss any news and events anymore, stay up to date.

    
    

    [https://icomplyis.com/icomply-blog/icomply-insights/fatf-prepares-massive-changes-for-virtual-asset-regulation/](https://icomplyis.com/icomply-blog/icomply-insights/fatf-prepares-massive-changes-for-virtual-asset-regulation/)

    [FATF Prepares Massive Changes for Virtual Asset Regulation - iComp...](https://icomplyis.com/icomply-blog/icomply-insights/fatf-prepares-massive-changes-for-virtual-asset-regulation/)

    
    

    [https://tokenomica.com/blog/first-regulated-decentralized-exchange-whats-in-it-for-you/](https://tokenomica.com/blog/first-regulated-decentralized-exchange-whats-in-it-for-you/)

    [Centralized or Decentralized Exchange. Regulated DEX is the Answer](https://tokenomica.com/blog/first-regulated-decentralized-exchange-whats-in-it-for-you/)

    One of the perceived aims of cryptocurrency and blockchain is the decentralization. But at the same time, the vast majority of crypto-based exchanges, are fully centralized.

    
    

    [https://thenextweb.com/hardfork/2019/06/12/bitcoin-cryptocurrency-fatf-regulation/](https://thenextweb.com/hardfork/2019/06/12/bitcoin-cryptocurrency-fatf-regulation/)

    [9 days until the crypto industry must verify the identity of anyon...](https://thenextweb.com/hardfork/2019/06/12/bitcoin-cryptocurrency-fatf-regulation/)

    New rules by the Financial Action Task Force (FATF) will force cryptocurrency businesses in 200 countries to introduce stricter identity checks.

    [https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2019%2F04%2Fransomware-bitcoin-cryptocurrency-blockchain-ryuk-.jpg&signature=c496c81439e30f557e89c21b978465a6](https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2019%2F04%2Fransomware-bitcoin-cryptocurrency-blockchain-ryuk-.jpg&signature=c496c81439e30f557e89c21b978465a6)

- 🗺-india-regulation

        Crypto⧉Finance

    🗺-india-regulation

    4 messages

    

    [https://thebitcoinnews.com/indian-cryptocurrency-exchanges-see-rapid-growth-in-p2p-trading/](https://thebitcoinnews.com/indian-cryptocurrency-exchanges-see-rapid-growth-in-p2p-trading/)

    [Indian Cryptocurrency Exchanges See Rapid Growth in P2P Trading - ...](https://thebitcoinnews.com/indian-cryptocurrency-exchanges-see-rapid-growth-in-p2p-trading/)

    Trading volumes on exchange-escrowed peer-to-peer (P2P) cryptocurrency trading platforms in India are rising rapidly amid the banking ban by the country’s central bank. “Indians are warming up to P2P in amazing ways,” the CEO of a local crypto exchange told news.Bitcoin...

    
    

    [https://news.bitcoin.com/reserve-bank-of-india-forms-unit-on-cryptocurrencies-blockchain-ai/](https://news.bitcoin.com/reserve-bank-of-india-forms-unit-on-cryptocurrencies-blockchain-ai/)

    [Reserve Bank of India Forms Unit on Cryptocurrencies, Blockchain, ...](https://news.bitcoin.com/reserve-bank-of-india-forms-unit-on-cryptocurrencies-blockchain-ai/)

    The central bank of India has reportedly formed a special unit tasked to track emerging technologies such as those related to cryptocurrencies.

    
    

    [https://davidgerard.co.uk/blockchain/2018/09/02/first-legally-mandated-blockchain-telecom-regulatory-authority-of-indias-spam-call-complaint-database/](https://davidgerard.co.uk/blockchain/2018/09/02/first-legally-mandated-blockchain-telecom-regulatory-authority-of-indias-spam-call-complaint-database/)

    [First legally-mandated blockchain: Telecom Regulatory Authority of...](https://davidgerard.co.uk/blockchain/2018/09/02/first-legally-mandated-blockchain-telecom-regulatory-authority-of-indias-spam-call-complaint-database/)

    Requiring a particular deep implementation is bad — and especially because of unrealisable hype.

    
    

    [https://news.ycombinator.com/item?id=20510190](https://news.ycombinator.com/item?id=20510190)

    thekhatribharat

    [India's draft bill on Cryptocurrency regulation [pdf]](https://news.ycombinator.com/item?id=20510190)

    
    Hacker News • 23-Jul-19 05:04 PM

- 🗺-global-digital-finance

        Crypto⧉Finance

    🗺-global-digital-finance

    3 messages

    

    [https://gdf.io/](https://gdf.io/)

    [Home | GDF](https://gdf.io/)

    The global formation and distribution of capital has value to both new market entrants and incumbents in financial services. Global Digital Finance endeavours to drive efficient, fair and transparent crypto asset markets by building a knowledge base and best practice for “T...

    
    GDF is an industry membership body that promotes the adoption of best practices for cryptoassets and digital finance technologies, through the development of conduct standards, in a shared engagement forum with market participants, policymakers and regulators. -Taxonomy for Cryptographic Assets From the Perspective of General Global Regulatory Standards [https://www.gdf.io/wp-content/uploads/2018/10/0003_GDF_Taxonomy-for-Cryptographic-Assets_Web-151018.pdf](https://www.gdf.io/wp-content/uploads/2018/10/0003_GDF_Taxonomy-for-Cryptographic-Assets_Web-151018.pdf) -Crypto Asset Safekeeping and Custody [https://www.gdf.io/wp-content/uploads/2019/02/GDF-Crypto-Asset-Safekeeping_20-April-2019.pdf](https://www.gdf.io/wp-content/uploads/2019/02/GDF-Crypto-Asset-Safekeeping_20-April-2019.pdf) GDF Response to the proposed canadian regulation for exchanges: [https://www.gdf.io/wp-content/uploads/2019/05/IIROC-April-24-2019.pdf](https://www.gdf.io/wp-content/uploads/2019/05/IIROC-April-24-2019.pdf) -Link to the source which above refers to: [https://www.bcsc.bc.ca/21-402_[Joint_Canadian_Securities_Administrators_Investment_Industry_Regulatory_Organization_of_Canada_Consultation_Paper]_03142019/](https://www.bcsc.bc.ca/21-402_%5BJoint_Canadian_Securities_Administrators_Investment_Industry_Regulatory_Organization_of_Canada_Consultation_Paper%5D_03142019/) -Part VII – Principles for Security Token Offerings & Secondary Market Trading Platforms [https://www.gdf.io/docsconsultations/part-vii-code-of-conduct-principles-for-security-token-offerings-secondary-market-trading-platforms/](https://www.gdf.io/docsconsultations/part-vii-code-of-conduct-principles-for-security-token-offerings-secondary-market-trading-platforms/) -Part VIII -Principles for Know Your Customer (KYC) & Anti-Money Laundering (AML) [https://www.gdf.io/docsconsultations/part-viii-code-of-conduct-principles-for-know-your-customer-kyc-anti-money-laundering-aml/](https://www.gdf.io/docsconsultations/part-viii-code-of-conduct-principles-for-know-your-customer-kyc-anti-money-laundering-aml/) (edited)

    [Part VII - Code of Conduct - Principles for Security Token Offerin...](https://www.gdf.io/docsconsultations/part-vii-code-of-conduct-principles-for-security-token-offerings-secondary-market-trading-platforms/)

    
    Part VI – Principles for Stablecoin Issuers [https://www.gdf.io/docsconsultations/part-vi-code-of-conduct-principles-for-stablecoin-issuers/](https://www.gdf.io/docsconsultations/part-vi-code-of-conduct-principles-for-stablecoin-issuers/) [https://www.gdf.io/working-groups/](https://www.gdf.io/working-groups/)

    [Part VI - Code of Conduct - Principles for Stablecoin Issuers | GDF](https://www.gdf.io/docsconsultations/part-vi-code-of-conduct-principles-for-stablecoin-issuers/)

    

    [Working Groups | GDF](https://www.gdf.io/working-groups/)

    
- 🗺-israel

        Crypto⧉Finance

    🗺-israel

    1 messages

    

    [https://www.iosco.org/library/ico-statements/Israel%20-%20ISA%20-%2020190301%20-%20Regulation%20of%20Decentralised%20Cryptographic%20Currency%20Issuance%20to%20the%20Public%20Final%20Report.pdf](https://www.iosco.org/library/ico-statements/Israel%2520-%2520ISA%2520-%252020190301%2520-%2520Regulation%2520of%2520Decentralised%2520Cryptographic%2520Currency%2520Issuance%2520to%2520the%2520Public%2520Final%2520Report.pdf)

- 🗺-self-regulation

        Crypto⧉Finance

    🗺-self-regulation

    5 messages

    

    [https://messari.io/registry](https://messari.io/registry)

    Open-source library of relevant token project data serves as a “single source of truth.”Disclosures provided by projects and validated by the system of validating token-holders.Platform ensures updates are disseminated publicly to a wide audience in a timely fashion.Self-regulatory mechanism ensures reporting requirements are met & only compliant projects admitted.Registry value may increase as the quality of the underlying data and registry size increases.How the Registry Works The crypto markets are more opaque and complicated to navigate than traditional asset classes. Data inf rastructure is poor, standardized disclosures are non-existent, and incentives can be misaligned. There are no standardized formats or channels for token projects to share essential information with their communities and stakeholders. Enforcing transparency standards amongst token projects presents a global coordination problem with significant bootstrapping challenges. Absent the authority of a government-sanctioned regulator, does crypto have the necessary tools to self-regulate?

    (edited)

    
    [Messari (@MessariCrypto)](https://twitter.com/MessariCrypto)

    
    ANNOUNCING our first cohort of launch partners who are committed to transparency and self-regulation
    [https://t.co/8](https://t.co/8)

    ...

    Likes

    255

    
    Twitter

    [Registry](https://messari.io/registry)

    An open-source data library helping researchers, investors, and regulators make sense of the industry.

    
    [https://messari.github.io/tcr/Messari%20Registry%20Press%20Release%2011-27-18.pdf](https://messari.github.io/tcr/Messari%2520Registry%2520Press%2520Release%252011-27-18.pdf)

    **MESSARI LAUNCHES A DISCLOSURES REGISTRY WITH 12 INITIAL PARTNERS**

    Messari, the leading provider of contextualized data and research tools for the cryptoasset industry, launched its open-source disclosures registry today. They are joined by a diverse, reputable group of 12 initial partner projects committed to transparency and self-regulation. Messari’s registry aims to become a single source of truth for basic cryptoasset information. Participating projects are voluntarily disclosing basic information regarding their token design, supply details, technology audits, official communication channels, and relevant team members, investors, and advisors. These profiles can then be f reely accessed industry-wide, providing a reliable, standardized resource to industry participants that is nonexistent today. Such basic data standards can facilitate diligence processes for crypto service providers like exchanges and wallets, retail and professional investors, and regulators alike.

    (edited)

    [https://twitter.com/MessariCrypto/status/1067420099991298050](https://twitter.com/MessariCrypto/status/1067420099991298050)

    
    [Messari (@MessariCrypto)](https://twitter.com/MessariCrypto)

    
    ANNOUNCING our first cohort of launch partners who are committed to transparency and self-regulation
    [https://t.co/8](https://t.co/8)

    ...

    Likes

    255

    
    Twitter

    

    [https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437367028](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437367028)

    [‎Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distr...](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437367028)

    ‎Show Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distributed Technologies, Ep Ryan Selkis: Messari – Bringing Transparency and Self-Regulation to the Blockchain Industry - Aug 9, 2018

    
    

    [https://virtualcommodities.org/](https://virtualcommodities.org/)

    [Virtual Commodity Association](https://virtualcommodities.org/)

    Fostering consumer protection and market integrity for virtual commodity marketplaces

- 🗺-naasa

        Crypto⧉Finance

    🗺-naasa

    6 messages

    

    [http://www.nasaa.org/45901/nasaa-updates-coordinated-crypto-crackdown/](http://www.nasaa.org/45901/nasaa-updates-coordinated-crypto-crackdown/)

    [NASAA Updates Coordinated Crypto Crackdown - NASAA](http://www.nasaa.org/45901/nasaa-updates-coordinated-crypto-crackdown/)

    August 28, 2018:Borg: “State and provincial securities regulators are committing significant regulatory resources to protect investors from financial harm involving fraudulent ICOs and cryptocurrency-related investment products and also are raising awareness among industry ...

    
    [https://www.nasaa.org/44848/informed-investor-advisory-cryptocurrencies/?qoid=investor-education](https://www.nasaa.org/44848/informed-investor-advisory-cryptocurrencies/?qoid=investor-education)

    [Informed Investor Advisory: Cryptocurrencies - NASAA](https://www.nasaa.org/44848/informed-investor-advisory-cryptocurrencies/?qoid=investor-education)

    Before you jump into the crypto craze, be mindful that cryptocurrencies and related financial products may be nothing more than public facing fronts for Ponzi schemes and other frauds. And because these products do not fall neatly into the existing federal/state regulatory fr...

    
    [https://www.nasaa.org/?s=crypto](https://www.nasaa.org/?s=crypto)

    [You searched for crypto - NASAA](https://www.nasaa.org/?s=crypto)

    
    [https://www.nasaa.org/46133/nasaa-releases-annual-enforcement-report-4/?qoid=newsroom](https://www.nasaa.org/46133/nasaa-releases-annual-enforcement-report-4/?qoid=newsroom)

    [NASAA Releases Annual Enforcement Report - NASAA](https://www.nasaa.org/46133/nasaa-releases-annual-enforcement-report-4/?qoid=newsroom)

    Pieciak: “The results from this year’s enforcement survey demonstrate that state securities regulators continue to play a critical role in protecting investors and holding securities law violators responsible for the damage that they cause to individual investors specific...

    
    

    STATE AND PROVINCIAL SECURITIES REGULATORS CONDUCT COORDINATED INTERNATIONAL CRYPTO CRACKDOWN [https://www.nasaa.org/45121/state-and-provincial-securities-regulators-conduct-coordinated-international-crypto-crackdown-2/](https://www.nasaa.org/45121/state-and-provincial-securities-regulators-conduct-coordinated-international-crypto-crackdown-2/)

    [State and Provincial Securities Regulators Conduct Coordinated Int...](https://www.nasaa.org/45121/state-and-provincial-securities-regulators-conduct-coordinated-international-crypto-crackdown-2/)

    May 21, 2018:Borg: “The actions announced today are just the tip of the iceberg."

    
    

    [https://epicenter.tv/episode/285/](https://epicenter.tv/episode/285/)

    [Mike Pieciak: NASAA – A “To The Moon” Approach to Regulating...](https://epicenter.tv/episode/285/)

    We're joined by Mike Pieciak, President of the North American Securities Administrators Association. In 2018, “Operation Cryptosweep” investigated over 200 ICOs for potential investor fraud.

    
- 🗺-media-regulation

        Crypto⧉Finance

    🗺-media-regulation

    3 messages

    

    [https://www.stitcher.com/podcast/hangar-studios/appetite-for-disruption](https://www.stitcher.com/podcast/hangar-studios/appetite-for-disruption)

    [Appetite for Disruption: The Business and Regulation of FinTech](https://www.stitcher.com/podcast/hangar-studios/appetite-for-disruption)

    Listen to Appetite for Disruption: The Business and Regulation of FinTech episodes free, on demand. Many opportunities and challenges are at the intersection of technology and financial services. And FinTech itself is rapidly evolving as businesses continuously innovate and ...

    
    [https://player.fm/series/off-the-chain-2428336/cz-founder-and-ceo-of-binance-binance-and-the-future-of-global-crypto-regulation](https://player.fm/series/off-the-chain-2428336/cz-founder-and-ceo-of-binance-binance-and-the-future-of-global-crypto-regulation)

    [CZ, Founder and CEO of Binance: Binance and the Future of Global C...](https://player.fm/series/off-the-chain-2428336/cz-founder-and-ceo-of-binance-binance-and-the-future-of-global-crypto-regulation)

    CZ is the Founder and CEO of Binance, a company with 400 employees, and hundreds of millions of dollars in profit. In this conversation, CZ and Anthony Pompliano discuss what he understood that many others didn't when he started the company, how he scaled, and what their futu...

    
    [https://unchainedpodcast.com/why-crypto-regulation-is-surprisingly-cypherpunk/](https://unchainedpodcast.com/why-crypto-regulation-is-surprisingly-cypherpunk/)

    [Why Crypto Regulation Is Surprisingly Cypherpunk - Unchained Podcast](https://unchainedpodcast.com/why-crypto-regulation-is-surprisingly-cypherpunk/)

    Lawson Baker, head of operations and general counsel at TokenSoft (disclosure: a previous sponsor of the podcast), explains the significance of this week’s SEC statement on broker-dealer custody of digital[...]Keep reading...

    
- 🗺-new-zealand

        Crypto⧉Finance

    🗺-new-zealand

    6 messages

    

    Bitcoin News: Bitcoin Is Now a Legal Form of Payment in New Zealand [https://bitcoinmagazine.com/articles/bitcoin-is-now-a-legal-form-of-payment-in-new-zealand](https://bitcoinmagazine.com/articles/bitcoin-is-now-a-legal-form-of-payment-in-new-zealand)

    [Bitcoin Is Now a Legal Form of Payment in New Zealand » Bitcoin...](https://bitcoinmagazine.com/articles/bitcoin-is-now-a-legal-form-of-payment-in-new-zealand)

    A public ruling integrates crypto assets as legal and taxable forms of payment in New Zealand.

    
    
    1

    

    [https://www.fma.govt.nz/compliance/cryptocurrencies/](https://www.fma.govt.nz/compliance/cryptocurrencies/)

    [Cryptocurrencies - Compliance | FMA](https://www.fma.govt.nz/compliance/cryptocurrencies/)

    Get an understanding of initial coin offers; cryptocurrency services, and what fair dealing obligations are for ICOs. The fair dealing requirements in Part 2 of the Financial Markets Conduct Act 2013 (FMC Act) are broad principles that prohibit misleading or deceptive conduct...

    [http://www.legislation.govt.nz/act/public/2013/0069/latest/DLM4090578.html](http://www.legislation.govt.nz/act/public/2013/0069/latest/DLM4090578.html)

    [http://www.legislation.govt.nz/act/public/2008/0097/75.0/DLM1109427.html](http://www.legislation.govt.nz/act/public/2008/0097/75.0/DLM1109427.html)

    [https://www.fma.govt.nz/compliance/role/market-operators/](https://www.fma.govt.nz/compliance/role/market-operators/)

    [Market operators | FMA](https://www.fma.govt.nz/compliance/role/market-operators/)

    Anyone operating a financial product market must be licensed. Find out more about registration, licensing and obligations.

    [https://www.fma.govt.nz/compliance/role/brokers-and-custodians/](https://www.fma.govt.nz/compliance/role/brokers-and-custodians/)

    [Brokers and custodians | FMA](https://www.fma.govt.nz/compliance/role/brokers-and-custodians/)

    Find out if you would need to register to provide broking /custodial services and what will be your obligations. Read more about breaches, offences, exemptions, fair dealings and fees and levies.

- 🗺-australia

        Crypto⧉Finance

    🗺-australia

    2 messages

    

    [https://www.theregister.co.uk/2018/08/12/australia_close_to_encryption_legislation/](https://www.theregister.co.uk/2018/08/12/australia_close_to_encryption_legislation/)

    [Australia on the cusp of showing the world how to break encryption](https://www.theregister.co.uk/2018/08/12/australia_close_to_encryption_legislation/)

    You just pass a law, apparently

    
    

    [https://bitcoinmagazine.com/articles/australian-government-publishes-update-cryptocurrency-and-ico-rules](https://bitcoinmagazine.com/articles/australian-government-publishes-update-cryptocurrency-and-ico-rules)

    [Australian Government Publishes Update on Cryptocurrency and ICO Rules](https://bitcoinmagazine.com/articles/australian-government-publishes-update-cryptocurrency-and-ico-rules)

    The Australian Securities and Investments Commission has updated its requirements for cryptocurrency business compliance.

- 🗺-africa-regulation

        Crypto⧉Finance

    🗺-africa-regulation

    2 messages

    

    [https://cointelegraph.com/news/egypt-central-banks-draft-law-requires-licenses-for-crypto-related-activities](https://cointelegraph.com/news/egypt-central-banks-draft-law-requires-licenses-for-crypto-related-activities)

    [Egypt: Central Bank’s Draft Law Requires Licenses for Crypto-Rel...](https://cointelegraph.com/news/egypt-central-banks-draft-law-requires-licenses-for-crypto-related-activities)

    A new draft banking law will make it mandatory to obtain licenses in advance of operating platforms for issuing or trading crypto.

    
    

    [https://cointelegraph.com/news/south-african-govt-reveals-it-has-no-plans-to-ban-crypto-in-recent-consultation-paper](https://cointelegraph.com/news/south-african-govt-reveals-it-has-no-plans-to-ban-crypto-in-recent-consultation-paper)

    [South African Gov’t Reveals It Has No Plans to Ban Crypto in Rec...](https://cointelegraph.com/news/south-african-govt-reveals-it-has-no-plans-to-ban-crypto-in-recent-consultation-paper)

    South African central bank has issued a jointly developed consultation paper, revealing that it does not intend to ban crypto.

    
- imf-international-monetary-fund

        Crypto⧉Finance

    imf-international-monetary-fund

    1 messages

    

    [https://twitter.com/pzilgalvis/status/1188556806693351425?s=12](https://twitter.com/pzilgalvis/status/1188556806693351425?s=12)

    
    [Pēteris Zilgalvis (@PZilgalvis)](https://twitter.com/PZilgalvis)

    Fintech : The Experience So Far” ⁦@IMFNews⁩ ‘Generally, Europe has been leading the way in enacting fintech regulations and regulatory innovations...... Last year the European Commission unveiled a fintech action plan.’ IMF #FinTech Roundtable #data [https://t](https://t/)....

    
    Twitter