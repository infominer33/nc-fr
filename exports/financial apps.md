# 🧩-financial-apps

Crypto⧉Finance

🧩-financial-apps

10 messages

[https://www.coindesk.com/all-big-insurers-are-uniting-behind-r3s-blockchain-tech/](https://www.coindesk.com/all-big-insurers-are-uniting-behind-r3s-blockchain-tech/) [https://www.trustnodes.com/2018/07/24/boston-fed-says-central-banks-can-disrupted-looking-blockchain-tech-not-uberized](https://www.trustnodes.com/2018/07/24/boston-fed-says-central-banks-can-disrupted-looking-blockchain-tech-not-uberized) [http://fortune.com/2018/08/08/fintech-crypto-trend-vc-invest/](http://fortune.com/2018/08/08/fintech-crypto-trend-vc-invest/) (edited)

[Big Insurers Are Uniting Behind R3's Blockchain Tech - CoinDesk](https://www.coindesk.com/all-big-insurers-are-uniting-behind-r3s-blockchain-tech/)

The RiskBlock Alliance, whose members include insurance giants Chubb, Marsh and Liberty Mutual, has decided to build its blockchain on R3's Corda.

[Boston Fed Looking at Blockchain Tech to Not Be "Uberized"](https://www.trustnodes.com/2018/07/24/boston-fed-says-central-banks-can-disrupted-looking-blockchain-tech-not-uberized)

Paul Brassil, vice president of information technology at the Boston branch of the Federal Reserve System stated that blockchain tech can disrupt central banking as any other industry. Speaking at...

[Fintech Frenzy: Hype or Reality? A Closer Look at 6 Key Sectors](http://fortune.com/2018/08/08/fintech-crypto-trend-vc-invest/)

A top venture capitalist grades the state of his industry—from payments to crypto.

[https://www.coindesk.com/all-big-insurers-are-uniting-behind-r3s-blockchain-tech/](https://www.coindesk.com/all-big-insurers-are-uniting-behind-r3s-blockchain-tech/) [https://twitter.com/Treasury_WB/status/1027653858951155712](https://twitter.com/Treasury_WB/status/1027653858951155712) [https://twitter.com/PhilCrypto77/status/1029800728611749888](https://twitter.com/PhilCrypto77/status/1029800728611749888) [http://en.ibena.ir/news/90482/Iranian-Cryptocurrency-s-Features-Revealed](http://en.ibena.ir/news/90482/Iranian-Cryptocurrency-s-Features-Revealed) [https://cloud.coinfeed.com/share/post/98888515354246/641535424650/25481](https://cloud.coinfeed.com/share/post/98888515354246/641535424650/25481) (edited)

[Big Insurers Are Uniting Behind R3's Blockchain Tech - CoinDesk](https://www.coindesk.com/all-big-insurers-are-uniting-behind-r3s-blockchain-tech/)

The RiskBlock Alliance, whose members include insurance giants Chubb, Marsh and Liberty Mutual, has decided to build its blockchain on R3's Corda.

[The World Bank Treasury (@Treasury_WB)](https://twitter.com/Treasury_WB)

Today we make history by creating the first global blockchain bond. The World Bank has mandated the Commonwealth Bank of Australia as the sole arranger for bond-i, the first global bond to use distributed ledger technology. Learn more: [https://t.co/tieQoQ9uLe](https://t.co/tieQoQ9uLe) #blockchain...

Retweets

102

Likes

113

Twitter

[Felipe (@PhilCrypto77)](https://twitter.com/PhilCrypto77)

Fairly big news that has gone under the radar... US investors will now be able to buy #Bitcoin through their @Fidelity accounts. [https://t.co/pCwMBd7G8M](https://t.co/pCwMBd7G8M)

Retweets

492

Likes

1070

Twitter

[Bank of Thailand Announces 'Milestone' Digital Currency Project Us...](https://cointelegraph.com/news/bank-of-thailand-announces-milestone-digital-currency-project-using-r3-corda-platform)

The Bank of Thailand has revealed a major collaborative project to develop a wholesale Central Bank Digital Currency using R3’s Corda platform.


[The Emerging Financial Stack](https://medium.com/@espitia7/the-emerging-financial-stack-3d80a278bd94)

An overview of the technologies that are shaping the future of finance on the blockchain

[Jake Chervinsky (@jchervinsky)](https://twitter.com/jchervinsky)

0/ A primer on legal risk assessment & mitigation. Institutional investors & blue chip companies spend a ton of time & resources analyzing & managing legal risk. If you're investing or working in crypto, you should probably give it some thought too. Thread.

Twitter

[Why & How to tokenize a Venture Capital Fund?](https://hackernoon.com/https-medium-com-firstcrypto-cryptovc-13ec13ff886)

[Max Mersch (@MerschMax_)](https://twitter.com/MerschMax_)

Excited to publish the @fabric_vc Investment Thesis in this 2nd edition of the State of the Token Market Report
[https://t.co/QxJjoM4GhT](https://t.co/QxJjoM4GhT)

Investment Thesis: pages 28-44 @richardmuirhead @Anastasiya_B94 @JulienThevenard @maciejrymarz

️ Core Arguments Below

️

Twitter

[Blockchain & Cryptoasset Opportunities for Islamic Finance](https://www.linkedin.com/pulse/blockchain-cryptoasset-opportunities-islamic-finance-matt-lunkes/)

Hi all
[https://media.licdn.com/dms/image/C4D12AQHo6fPah7jTQA/article-cover_image-shrink_600_2000/0?e=1569456000&v=beta&t=Itoilpj7wRdY0uJcmqALR2WSz17RlawWnT7XgSb5Fy8](https://media.licdn.com/dms/image/C4D12AQHo6fPah7jTQA/article-cover_image-shrink_600_2000/0?e=1569456000&v=beta&t=Itoilpj7wRdY0uJcmqALR2WSz17RlawWnT7XgSb5Fy8)

[Jake Chervinsky (@jchervinsky)](https://twitter.com/jchervinsky)

0/ @Bakkt plans to launch next month on December 12, and some people are hoping it kicks off another crypto bull run. Now seems like a good time for a quick discussion on: - what Bakkt is - why it might be exciting - when it will get regulatory approval Thread.

Retweets

1304

Likes

2861

Twitter

[nic carter (@nic__carter)](https://twitter.com/nic__carter)

A couple reflections on what regulators and central bankers are thinking about, having attended the 'cryptoasset' days of the DC Fintech week at the IMF

Retweets

102

Likes

342

Twitter

[Nathaniel Whittemore (@nlw)](https://twitter.com/nlw)

14/ Going even deeper into the existing infrastructure, @APompliano wrote up how a consortium of 15 banks is working with the DTCC (who process 95% of all credit derivates globally) to experiment with a blockchain system of trading credit derivates. [https://t.co/HQUD93ZXid](https://t.co/HQUD93ZXid)

Twitter

[CoinDesk (@coindesk)](https://www.theblockcrypto.com/2019/01/22/bank-for-international-settlements-argues-bitcoin-is-not-sustainable-without-block-rewards/)

In her opening remarks at the #BlockFS conference, @cmoyall, executive director for blockchain at J.P. Morgan, says: “We just put into production our blockchain system for inter-bank transactions, and over a 100 banks have signed up.

Retweets

124

Likes

288

Twitter

[70% of Central Banks are Working on Digital Currencies, No Hurry t...](https://www.ccn.com/70-of-central-banks-are-working-on-digital-currencies-no-hurry-to-issue-them-bis)

A survey commissioned by the bank of central banks, the Bank of International Settlements, has disclosed that even though Central Bank Digital Currencies (CBDC) are being researched by a big number of reserve banks, the work is mostly conceptual.

[Bank for International Settlements argues Bitcoin 'is not sustaina...](https://www.theblockcrypto.com/2019/01/22/bank-for-international-settlements-argues-bitcoin-is-not-sustainable-without-block-rewards/)

Raphael Auer, Principal Economist of Monetary and Economic Department at the Bank for International Settlements (BIS), wrote a paper titled “Beyond the doomsday economics of “proof-of-work” in cryptocurrencies.” BIS, an international financial institution owned by cen...

[Angus Champion de Crespigny (@anguschampion)](https://www.greenwich.com/steampunk-settlement-report-download)

What is DeFi?

Twitter

[Michael Kimani (@pesa_africa)](https://twitter.com/pesa_africa)

Jack Dorsey of Twitter answers our questions about Square's plans for Bitcoin [https://t.co/Z6QxunBs86](https://t.co/Z6QxunBs86)

Twitter

[Steampunk Settlement - Report Download | Greenwich Associates](https://www.greenwich.com/steampunk-settlement-report-download)

Regulatory enthusiasm for central clearing and the advent of distributed ledger technology have focused minds on the post trade system as never before

[https://t.co/OerVM5Tldh](https://t.co/OerVM5Tldh) [https://twitter.com/cryptinfominer/status/1032067981118005248](https://twitter.com/cryptinfominer/status/1032067981118005248)

[Goldman Sachs And J.P. Morgan Join $32M Series B In Enterprise Blo...](https://t.co/OerVM5Tldh)

Several of the largest financial institutions in the world are investing in this enterprise blockchain startup, and actually using the technology.

[⧉ Infominer (@infominer33)](https://twitter.com/infominer33)

35. (just found) Bitspark providing a blockchain based remittance platform in Tajikistan with the UNDP since 2017 Bitspark now switching to Bitshares blockchain for this purpose. [https://t.co/F1ck85ZJqz](https://t.co/F1ck85ZJqz)

Twitter

[Mike Dudas (@mdudas)](https://twitter.com/mdudas/status/1158718246406823938?s=12)

There truly aren't very many "advanced" retail cryptocurrency traders [https://t.co/XZqSypEHof](https://t.co/XZqSypEHof)

Twitter

- 🧩-wallets

        Crypto⧉Finance

    🧩-wallets

    5 messages

    

    ["I Was In Shock": Woman Finds Her BofA Safe Deposit Box Has Vanished](https://www.zerohedge.com/news/2018-07-26/i-just-got-robbed-bank-woman-finds-her-bofa-safe-deposit-box-has-vanished)

    "I just got robbed from the bank. They just took my stuff."

    
    [Wasabi: Privacy Focused Bitcoin Wallet for Desktop](https://medium.com/@nopara73/wasabi-privacy-focused-bitcoin-wallet-for-desktop-3962d567045a)

    (UPDATE) Download Link: [https://github.com/zkSNACKs/WalletWasabi/releases](https://github.com/zkSNACKs/WalletWasabi/releases)

    
    
    [Jean-Baptiste Lefevre (@jblefevre60)](https://twitter.com/jblefevre60)

    This #wallet claims to keep your #bitcoins safe from #hackers #fintech #Blockchain #CyberSecurity #XRP @leimer @andi_staub @AntonioSelas @JacBurns*Comext @Shirastweet @ipfconline1 @HaroldSinnott @mclynd @LouisSerge @chboursin @mallys* @jerome_joffre @diioannid @ahier @HIT...

    
    
    Twitter

    
    [NVK (@nvk)](https://twitter.com/nvk)

    This is what your @COLDCARDwallet pack should look like. And here are some getting started instructions: [https://t.co/Lr9oarqVhl](https://t.co/Lr9oarqVhl)

    
    Twitter

    [Casa: Crypto Wallet Security with Jameson Lopp - Software Engineer...](https://softwareengineeringdaily.com/2018/08/15/casa-crypto-wallet-security-with-jameson-lopp/)

    Cryptocurrency security is a concern to anyone who has a significant amount of money in the form of Bitcoin, Ethereum, or other crypto assets. Most Bitcoin is held in either a Bitcoin wallet or a Bitcoin bank. Your Bitcoin holdings are recorded on a public ledger. You access ...

    
    
    [Coinbase Wallet (@CoinbaseWallet)](https://twitter.com/CoinbaseWallet)

    Happy Friday
    Likes

    123

    
    Twitter

    [Coldcard Wallet – Coldcard is the Ultrasecure Bitcoin hardware w...](http://coldcardwallet.com/)

    Coldcard is the Ultrasecure Bitcoin hardware wallet

    
    
    [🛠️ Austin Griffith 🔥 (@austingriffith)](https://twitter.com/austingriffith)

    Introducing the

    
    
    Burner Wallet

    
    
    for "Ethereum in Emerging Economies - Mass adoption will start where decentralization is necessary"

    [https://t.co/fvuHoZLhCh](https://t.co/fvuHoZLhCh)

    One phone sends DAI to another in seconds without any wallet download, seed phrase, or gas
    Likes

    199

    
    Twitter

    [The hardware wallet in a phone](https://medium.com/@craig_10243/the-hardware-wallet-in-a-phone-a2fbbcf03a74)

    There is a rather simple means to have a secure hardware wallet and it works far better than Ledger or Trezor and those sort of wallets…

    
    
    [BIG DAWG crypto (@woofBIGDAWG)](https://twitter.com/woofBIGDAWG)

    Every morning with my coffee I look at the top 200 BTC wallets. Every morning for the past couple of weeks now there is a lot more BTC going in than out. Today on wallets #101 - #200 there are THOUSANDS of BTC being added. Accumulation fact.

    
    Retweets

    381

    Likes

    1431

    
    Twitter

    [media.ccc.de](https://www.youtube.com/user/mediacccde)

    [35C3 - wallet.fail](https://www.youtube.com/watch?v=Y1OBIGslgGM&feature=share)

    [https://media.ccc.de/v/35c3-9563-wallet_fail](https://media.ccc.de/v/35c3-9563-wallet_fail) Hacking the most popular cryptocurrency hardware wallets In this presentation we will take a look at how to brea...

    
    
    [Christopher Allen (@ChristopherA)](https://twitter.com/ChristopherA)

    
    Perfection is the enemy of good: “added Bluetooth means that the Nano X can readily be used with mobile devices, which has been a pain point for many users of the company’s current wallet, the Ledger S. Much of the world primarily uses mobile computing” https://...

    
    Twitter

    
    [Elaine Ou 🐤 (@eiaine)](https://twitter.com/eiaine)

    Non-custodial wallets that are just as easy to use as Coinbase and CashApp: @SamouraiWallet @AbraGlobal @blockchain [https://t.co/pXphAYIUTy](https://t.co/pXphAYIUTy)

    
    Twitter

    
    [Mihailo Bjelic (@MihailoBjelic)](https://twitter.com/MihailoBjelic)

    .@argentHQ looks fantastic: - No gas needed - Multiple recovery options - No seed phrase - Human-readable accounts - Spending limits - DeFi integration With wallets like this, mass adoption is inevitable.

    
    
    *wallet contracts need to be battle-tested

    [https://t.co/](https://t.co/)

    ...

    
    Twitter

    [How to use Third Party Wallets with the Trezor Model T](https://www.thecryptomerchant.com/blogs/resources/how-to-use-third-party-wallets-with-the-trezor-model-t)

    This guide shows you how to access a wide array of cryptocurrencies with your Model T using third party wallets.

    
    

    [https://www.youtube.com/channel/UC_CymrxptTtzqvQHkkIEO3A/videos](https://www.youtube.com/channel/UC_CymrxptTtzqvQHkkIEO3A/videos)

    [Hodl Helper](https://www.youtube.com/channel/UC_CymrxptTtzqvQHkkIEO3A/videos)

    Basic education about wallets, the usage of digital assets and decentralized networks. Also stuff like surveillance capitalism and self-sovereign identity.

    [https://yt3.ggpht.com/a/AGF-l7-WTitonCkAf-OmM5vRCzQ48qC4DvfsJSde4g=s900-c-k-c0xffffffff-no-rj-mo](https://yt3.ggpht.com/a/AGF-l7-WTitonCkAf-OmM5vRCzQ48qC4DvfsJSde4g=s900-c-k-c0xffffffff-no-rj-mo)

- 🧩-marketplaces

        Crypto⧉Finance

    🧩-marketplaces

    3 messages

    

    [https://coiniq.com/openbazaar-decentralized-marketplace/#Is_OpenBazaar_the_Next_Silk_Road](https://coiniq.com/openbazaar-decentralized-marketplace/#Is_OpenBazaar_the_Next_Silk_Road) [https://twitter.com/RedKatLife/status/1050911730308648972](https://twitter.com/RedKatLife/status/1050911730308648972) [https://hackernoon.com/execution-markets-automate-protocols-and-earn-crypto-67f64911010e](https://hackernoon.com/execution-markets-automate-protocols-and-earn-crypto-67f64911010e) [https://btcmanager.com/kaleido-partners-with-amazon-to-launch-ethereum-marketplace-for-enterprises/](https://btcmanager.com/kaleido-partners-with-amazon-to-launch-ethereum-marketplace-for-enterprises/) (edited)

    
    [Kat🍍🍍 (@RedKatLife)](https://twitter.com/RedKatLife)

    Currently doing a little shopping on @Overstock.com & I knew they accepted #Bitcoin but I had NO IDEA they also accepted #ETH #LTC #BCH #DASH #ETC #DOGE #BLK #DCR #KMD #POT #QTUM

    
    This just made my night
    
    Likes

    159

    
    Twitter

    [Execution markets — automate protocols and earn crypto](https://hackernoon.com/execution-markets-automate-protocols-and-earn-crypto-67f64911010e)

    
    [Kaleido Partners with Amazon to Launch Ethereum Marketplace for En...](https://btcmanager.com/kaleido-partners-with-amazon-to-launch-ethereum-marketplace-for-enterprises/)

    Kaleido, a startup that helps enterprises implement blockchain technology, has launched a new full-stack Ethereum marketplace platform in collaboration with ConsenSys and Amazon Web Services, the company announced in a press release on November 8, 2018. Kaleido Blockchain Mar...

    
    [Amazon Partners On New Ethereum Marketplace For Enterprises](https://www.forbes.com/sites/sarahhansen/2018/11/08/consensys-kaleido-launches-full-stack-marketplace-platform-for-enterprise-blockchains/)

    Kaleido, a startup aimed at helping enterprises implement blockchain technology, has launched a new platform in collaboration with Amazon Web Services (AWS).

    
    [https://www.forbes.com/sites/sarahhansen/2018/11/08/consensys-kaleido-launches-full-stack-marketplace-platform-for-enterprise-blockchains/#323d888c2ad8](https://www.forbes.com/sites/sarahhansen/2018/11/08/consensys-kaleido-launches-full-stack-marketplace-platform-for-enterprise-blockchains/#323d888c2ad8) [https://openbazaar.org/blog/how-openbazaar-bitcoin-could-kill-the-american-pickers-economy/](https://openbazaar.org/blog/how-openbazaar-bitcoin-could-kill-the-american-pickers-economy/) [https://twitter.com/openbazaar/status/1079845920232140800](https://twitter.com/openbazaar/status/1079845920232140800) [https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437366760](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437366760)

    [Amazon Partners On New Ethereum Marketplace For Enterprises](https://www.forbes.com/sites/sarahhansen/2018/11/08/consensys-kaleido-launches-full-stack-marketplace-platform-for-enterprise-blockchains/)

    Kaleido, a startup aimed at helping enterprises implement blockchain technology, has launched a new platform in collaboration with Amazon Web Services (AWS).

    
    [How OpenBazaar & Bitcoin Could Kill the American Pickers Economy](https://openbazaar.org/blog/how-openbazaar-bitcoin-could-kill-the-american-pickers-economy/)

    By Sam Patterson, OB1 Co-Founder

    
    [OpenBazaar (@openbazaar)](https://twitter.com/openbazaar)

    Here are 5 exciting stats that show network usage and the growth of our contributing community all around the world in 2018: [https://t.co/t81AXqKfqn](https://t.co/t81AXqKfqn)

    
    Twitter

    [‎Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distr...](https://podcasts.apple.com/us/podcast/epicenter-learn-about-blockchain-ethereum-bitcoin-distributed/id792338939?i=1000437366760)

    ‎Show Epicenter - Learn about Blockchain, Ethereum, Bitcoin and Distributed Technologies, Ep Brian Hoffman & Washington Sanchez: OpenBazaar – Growing a Permissionless Marketplace - Mar 19, 2019

    
    

    [https://twitter.com/ParticlProject/status/1158861814026969095?s=20](https://twitter.com/ParticlProject/status/1158861814026969095?s=20)

    
    [Particl [PART] (@ParticlProject)](https://twitter.com/ParticlProject)

    Instead, it uses an entirely decentralized moderation system based on game-theory.

    
    Follow the link below to understand how this is achieved
    
    [https://t.co/iYKd3n6Fji](https://t.co/iYKd3n6Fji)

    #TowardTheMarketplace

    
    Twitter

- 🧩-payments

        Crypto⧉Finance

    🧩-payments

    4 messages

    

    
    [Francis Pouliot ☣️ (@francispouliot_)](https://twitter.com/francispouliot_)

    As of now, @SatoshiPortal apps, notably Canada's first/largest Bitcoin processor @myBylls have eliminated all 3rd parties (bitgo, bcinfo, blockcypher) from our Bitcoin stack. Our custom self-hosted API infrastructure over @bitcoincoreorg (replacing Bitcore) is live in pr...

    Likes

    119

    
    Twitter

    
    [Pierre Rochard (@pierre_rochard)](https://twitter.com/pierre_rochard)

    The best way to increase demand for on-chain Bitcoin transactions and LN payments is to build open source consumer wallets and merchant tools. If you know C# / .NET, go check out @BtcpayServer [https://t.co/e0EMb18aGP](https://t.co/e0EMb18aGP) and see if there are open issues you're interested in fi...

    Likes

    156

    
    Twitter

    [Insight in payments | News | Reports | Events | The Paypers](https://www.thepaypers.com/)

    The Paypers is the go-to source for payments news and analysis worldwide. Our mission: provide independent views through reports, companies databases and events.

    [Cryptocurrencies Could Become Mainstream Payment Solution Within N...](https://www.prnewswire.com/news-releases/cryptocurrencies-could-become-mainstream-payment-solution-within-next-decade-finds-new-imperial-college-and-etoro-report-833609231.html)

    LONDON, July 9, 2018 /PRNewswire/ -- • New research from Imperial College London and eToro argues cryptocurrencies are already fulfilling one of three main...

    
    
    [Pierre Rochard (@pierre_rochard)](https://twitter.com/pierre_rochard)

    I set up an instance of @BtcpayServer on @Azure and tested it with @wordpressdotcom + @WooCommerce. It is a fantastic product. Finally someone (@NicolasDorier) has built a sensible, open source platform for Bitcoin merchants
    Likes

    230

    
    Twitter

    

    [https://lite.im/](https://lite.im/)

    [Your Crypto. Everywhere.](https://lite.im/)

    Your Crypto. Everywhere.

    

    [https://twitter.com/GetMosendo](https://twitter.com/GetMosendo)

    [Mosendo (@GetMosendo)](https://twitter.com/GetMosendo)

    Send money to anyone, anywhere. You won't even know it is cryptocurrency.

    Tweets

    113

    Followers

    420

    
    
    Twitter

    

    [https://www.saveonsend.com/blog/bitcoin-blockchain-money-transfer/](https://www.saveonsend.com/blog/bitcoin-blockchain-money-transfer/)

    [Does Bitcoin/Blockchain make sense for international money transfer?](https://www.saveonsend.com/blog/bitcoin-blockchain-money-transfer/)

    Bitcoin/Blockchain money transfer review of fees and speed vs. remittance startups and incumbents. Does it help unbanked and can it destroy Western Union?

    
- 🧩-icos

        Crypto⧉Finance

    🧩-icos

    13 messages

    

    [https://twitter.com/lawmaster/status/1039224845807116288](https://twitter.com/lawmaster/status/1039224845807116288)

    
    [Larry Cermak (@lawmaster)](https://twitter.com/lawmaster)

    1/ There is a big misconception that ICO companies have liquidated most of their ETH holdings. In today’s issue of Diar, we looked at all the publicly available ICO treasuries and analyzed the numbers. [https://t.co/EryLsW9Ouj](https://t.co/EryLsW9Ouj)

    Retweets

    456

    Likes

    956

    
    Twitter

    

    [https://news.bitcoin.com/46-last-years-icos-failed-already/](https://news.bitcoin.com/46-last-years-icos-failed-already/)

    [46% of Last Year’s ICOs Have Failed Already - Bitcoin News](https://news.bitcoin.com/46-last-years-icos-failed-already/)

    It has always been assumed that a large number of ICOs will fail, be it at the fundraising stage or when it comes to delivering the actual project. It’s

    
    [https://medium.com/@michaelflaxman/icos-are-cancer-c404594f181b](https://medium.com/@michaelflaxman/icos-are-cancer-c404594f181b) [https://medium.com/iconominet/ico-2-0-what-is-the-ideal-ico-ee9d285a8939#.btpvwin8c](https://medium.com/iconominet/ico-2-0-what-is-the-ideal-ico-ee9d285a8939#.btpvwin8c) [https://medium.com/@avtarsehra/icos-and-economics-of-lemon-markets-96638e86b3b2](https://medium.com/@avtarsehra/icos-and-economics-of-lemon-markets-96638e86b3b2)

    [ICOs are Cancer](https://medium.com/@michaelflaxman/icos-are-cancer-c404594f181b)

    I have been involved in a few venture-capital funded startups, so I’m used to getting introduced to entrepreneurs building software…

    
    [ICO 2.0 — what is the ideal ICO?](https://medium.com/iconominet/ico-2-0-what-is-the-ideal-ico-ee9d285a8939)

    ICONOMI ICO Fundamentals — 1 of 4

    
    [ICOs and Economics of Lemon Markets](https://medium.com/@avtarsehra/icos-and-economics-of-lemon-markets-96638e86b3b2)

    by Avtar Sehra

    
    [https://medium.com/blockchannel/understanding-the-ethereum-ico-token-hype-429481278f45](https://medium.com/blockchannel/understanding-the-ethereum-ico-token-hype-429481278f45)

    [Understanding the Ethereum ICO Token Hype](https://medium.com/blockchannel/understanding-the-ethereum-ico-token-hype-429481278f45)

    I’ve been reading a lot from the community in regards to tokens and the rise of the ICO crowdsale as of late. It’s been fascinating…

    
    

    [https://www.coininsider.com/ico-scams-top-countries/](https://www.coininsider.com/ico-scams-top-countries/) [https://twitter.com/lawmaster/status/1039224845807116288](https://twitter.com/lawmaster/status/1039224845807116288) [https://iconomist.com/](https://iconomist.com/) [https://twitter.com/ercwl/status/1033816350958014469](https://twitter.com/ercwl/status/1033816350958014469)

    [Notorious Five: the countries with the most inital coin offering scams](https://www.coininsider.com/ico-scams-top-countries/)

    Research conducted has identified the countries with the most number of the loudest fraudulent ICOs and the results might be suprising. #china #hongkong

    
    
    [Larry Cermak (@lawmaster)](https://twitter.com/lawmaster)

    1/ There is a big misconception that ICO companies have liquidated most of their ETH holdings. In today’s issue of Diar, we looked at all the publicly available ICO treasuries and analyzed the numbers. [https://t.co/EryLsW9Ouj](https://t.co/EryLsW9Ouj)

    Retweets

    456

    Likes

    956

    
    Twitter

    [Fifty.one – Curating the wild world of crypto](https://iconomist.com/)

    Discover the best news, products and people ranked by twitter influence.

    
    
    [Eric Wall (@ercwl)](https://twitter.com/ercwl)

    1/ Plenty of coins & ICOs are now down -90%. Hopefully the people who'll never be receptive the following commentary have left crypto twitter by now, but they'll surely be back for the next bull run. Making this thread to use as a reminder for when the idiocy returns

    
    Retweets

    756

    Likes

    1981

    
    Twitter

    [https://twitter.com/jchervinsky/status/1073722759765741568](https://twitter.com/jchervinsky/status/1073722759765741568) [https://twitter.com/ChristopherA/status/902950402525609984](https://twitter.com/ChristopherA/status/902950402525609984) [https://www.tokenethics.com/](https://www.tokenethics.com/) ^^^ [https://www.sec.gov/news/press-release/2019-15](https://www.sec.gov/news/press-release/2019-15) [https://twitter.com/camirusso/status/1149322104812244993?s=12](https://twitter.com/camirusso/status/1149322104812244993?s=12)

    
    [Jake Chervinsky (@jchervinsky)](https://twitter.com/jchervinsky)

    Every crypto company that raised funds through a public token sale is currently at risk of being forced to pay back every single dollar to their initial investors. The level of risk may differ for each company, but it's non-zero for all of them & it still hangs over their...

    Retweets

    184

    Likes

    663

    
    Twitter

    
    [Christopher Allen (@ChristopherA)](https://twitter.com/ChristopherA)

    Consensus of #TokenEthics Salon today: “All ICO white papers should contain (or link to) an ethical code of conduct” [https://t.co/WB8Lxy6uDT](https://t.co/WB8Lxy6uDT)

    
    Twitter

    [#TokenEthics](https://www.tokenethics.com/)

    Documents & Community Discussions on #TokenEthics

    
    [Camila Russo (@CamiRusso)](https://twitter.com/CamiRusso)

    Prediction

    
    : A new version of ICOs will emerge after people heal from 2017 trauma, just like there’s a DAO revival now. The Blockstack issuance may be the first step.

    [https://t.co/w3Lrm8zCHG](https://t.co/w3Lrm8zCHG)

    
    Twitter

    [https://www.coindesk.com/proof-of-use-civil-believes-this-ico-model-lets-it-sell-anyone-tokens-legally/](https://www.coindesk.com/proof-of-use-civil-believes-this-ico-model-lets-it-sell-anyone-tokens-legally/) [http://yetanotherico.com/](http://yetanotherico.com/) [https://thebitcoinnews.com/new-research-large-raise-icos-never-deliver-returns/](https://thebitcoinnews.com/new-research-large-raise-icos-never-deliver-returns/) [https://www.coindesk.com/enough-with-the-ico-me-so-horny-get-rich-quick-lambo-crypto](https://www.coindesk.com/enough-with-the-ico-me-so-horny-get-rich-quick-lambo-crypto)

    [https://twitter.com/paulsbohm/status/1015027146157273088](https://twitter.com/paulsbohm/status/1015027146157273088) [https://themerkle.com/vitalik-buterin-releases-revolutionary-new-daico-model-for-icos/](https://themerkle.com/vitalik-buterin-releases-revolutionary-new-daico-model-for-icos/) [https://concourseq.io/](https://concourseq.io/) [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3198694](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3198694)

    
    [Paul 💥 (@paulsbohm)](https://twitter.com/paulsbohm)

    20 years in, I'm supposed to be a distributed systems/cryptocurrency/cryptoeconomics expert. Many overfunded ICOs have whitepapers that are completely indecipherable to me. Bitcoin is compelling in its clarity. If I don't get what they are doing after 5 minutes, it's a No.

    Likes

    156

    
    Twitter

    [Vitalik Buterin Releases Revolutionary New DAICO Model for ICOs »...](https://themerkle.com/vitalik-buterin-releases-revolutionary-new-daico-model-for-icos/)

    Vitalik Buterin, the creator of the Ethereum Network, recently proposed a new method for decentralized fundraising called the “DAICO”. Incorporating elements of Decentralized Autonomous Organizations, or DAOs, the new model is designed to minimize the complexity and risk ...

    
    [ConcourseQ | ICO Due Diligence](https://concourseq.io/)

    ConcourseQ is a collaborative due diligence community. Together we research and review icos and blockchain projects.

    
    [The Market Cycles of ICOs, Bitcoin, and Ether by Christian Masiak,...](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3198694)

    We apply time series analysis to investigate the market cycles of Initial Coin Offerings (ICOs) as well as bitcoin and Ether. Our results show that shocks to IC

    [https://www.tokendata.io/](https://www.tokendata.io/)

    TokenData lists and provides all the data on current and upcoming Initial Coin Offerings (ICO) and token sales.

    

    [https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905](https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905)

    [Launching an ICO that is ready to bridge the old and new worlds fr...](https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905)

    Decisions (and Snags) in our Legal, Regulatory, and Financial Backstory

    
    When we started this journey, we surveyed the globe for the best place to run our ICO. We did a broad review of the best legal and financial environments for doing an ethical, responsible ICO. Our key criteria were: - Operates clearly inside an existing regulatory framework which can manage ICOs and is interested in working with crypto organisations. - Measures taken to ensure long-term security, through strong asset and liability protections. - Provides options for secure banking operations: bank accounts not severely limited for transfer in or out, and workable for operations not just assets. - Demonstrates acceptable business conduct, with a strong reputation. - And ideally demonstrates a strong community of developers, social organisations, service providers all in service of building new economies.

    

    [https://twitter.com/lawmaster/status/1159130023963561984?s=12](https://twitter.com/lawmaster/status/1159130023963561984?s=12)

    
    [Larry Cermak (@lawmaster)](https://twitter.com/lawmaster)

    1/ It’s now obvious that ICOs were a massive bubble that's unlikely to ever see a recovery. The median ICO return in terms of USD is -87% and constantly dropping. Let's look at some data
    Retweets

    207

    Likes

    532

    
    Twitter

    

    [https://twitter.com/lawmaster/status/1159131367176884224](https://twitter.com/lawmaster/status/1159131367176884224)

    
    [Larry Cermak (@lawmaster)](https://twitter.com/lawmaster)

    11/ The summary table of the best 20 performing ICOs in terms of USD is here

    Likes

    103

    
    Twitter

- 🧩-fiat-onramps

        Crypto⧉Finance

    🧩-fiat-onramps

    2 messages

    

    [https://twitter.com/AlexanderFisher/status/1041331090902970368?s=19](https://twitter.com/AlexanderFisher/status/1041331090902970368?s=19) [https://medium.com/2gether/the-seven-obstacles-we-have-encountered-building-a-decentralized-bank-27c13fca72d0](https://medium.com/2gether/the-seven-obstacles-we-have-encountered-building-a-decentralized-bank-27c13fca72d0) [https://letstalkbitcoin.com/blog/post/the-bitcoin-game-54-the-current-state-of-cryptocurrency-panel-from-sodm18](https://letstalkbitcoin.com/blog/post/the-bitcoin-game-54-the-current-state-of-cryptocurrency-panel-from-sodm18) [https://twitter.com/CryptoBrekkie/status/1131674450900840448](https://twitter.com/CryptoBrekkie/status/1131674450900840448) [https://twitter.com/jwilliamsfstmed/status/1132236755732766720?s=12](https://twitter.com/jwilliamsfstmed/status/1132236755732766720?s=12)

    
    [alexfisher.eth (@AlexanderFisher)](https://twitter.com/AlexanderFisher)

    I'm really excited by what @sendwyre and @MakerDAO are doing to enable fiat on/off ramp from #ethereum ecosystem
    
    Twitter

    [The seven obstacles we have encountered building a decentralized bank.](https://medium.com/2gether/the-seven-obstacles-we-have-encountered-building-a-decentralized-bank-27c13fca72d0)

    by Salvador Casquero

    
    [The Bitcoin Game #54: The Current State of Cryptocurrency Panel fr...](https://letstalkbitcoin.com/blog/post/the-bitcoin-game-54-the-current-state-of-cryptocurrency-panel-from-sodm18)

    Welcome to episode 54 of The Bitcoin Game, I'm Rob Mitchell. Back in 2015, I had been waiting and waiting for a Bitcoin conference to be held here in Los Angeles. I finally gave up and booked a trip to the 2015 Texas Bitcoin Conference. Of course, shortly after making arrang...

    
    
    [₿rekkie von ₿itcoin☣️🍯🦡 (@CryptoBrekkie)](https://twitter.com/CryptoBrekkie)

    Does anyone have a list of fiat onramps by country? I recommend @CashApp to people in the US but I get a lot of ppl asking me how they can buy Bitcoin from all over the world and I don’t always have an answer.. Help me spread adoption
    
    
    Where r u from? How do y...

    Likes

    139

    
    Twitter

    
    [Jason A. Williams 🦍 (@JWilliamsFstmed)](https://twitter.com/JWilliamsFstmed)

    Robinhood was granted the coveted BitLicense by the New York Department of Financial Services (NYDFS). Now New York residents can buy and sell Bitcoin. There are 21M Bitcoin and 8.6M New Yorkers.

    
    
    Accumulation Mode

    
    
    
    Retweets

    129

    Likes

    698

    
    Twitter

    

    [https://www.reddit.com/r/darknet/comments/clda1y/best_place_to_buy_bitcoin_i_have_none_located/](https://www.reddit.com/r/darknet/comments/clda1y/best_place_to_buy_bitcoin_i_have_none_located/)

    [r/darknet - Best place to buy bitcoin? I have none located near me...](https://www.reddit.com/r/darknet/comments/clda1y/best_place_to_buy_bitcoin_i_have_none_located/)

    12 votes and 40 comments so far on Reddit

- 🧩-defi

        Crypto⧉Finance

    🧩-defi

    12 messages

    

    [https://media.consensys.net/the-100-projects-pioneering-decentralized-finance-717478ddcdf2](https://media.consensys.net/the-100-projects-pioneering-decentralized-finance-717478ddcdf2) [https://github.com/ong/awesome-decentralized-finance](https://github.com/ong/awesome-decentralized-finance) [https://podcasts.apple.com/us/podcast/ledger-cast-crypto-bitcoin-trading-blockchain-ecosystem/id1322087447?i=1000441415935](https://podcasts.apple.com/us/podcast/ledger-cast-crypto-bitcoin-trading-blockchain-ecosystem/id1322087447?i=1000441415935) [https://twitter.com/seanlippel/status/1102951325757775872?s=12](https://twitter.com/seanlippel/status/1102951325757775872?s=12)

    [The 100+ Projects Pioneering Decentralized Finance](https://media.consensys.net/the-100-projects-pioneering-decentralized-finance-717478ddcdf2)

    The stablecoins, DEXs, investing, derivatives, payments, lending, and insurance platforms building on Ethereum

    
    [ong/awesome-decentralized-finance](https://github.com/ong/awesome-decentralized-finance)

    A curated list of awesome decentralized finance projects - ong/awesome-decentralized-finance

    [‎Bitcoin & Crypto Trading: Ledger Cast: Decentralized Finance, w...](https://podcasts.apple.com/us/podcast/ledger-cast-crypto-bitcoin-trading-blockchain-ecosystem/id1322087447?i=1000441415935)

    ‎Show Bitcoin & Crypto Trading: Ledger Cast, Ep Decentralized Finance, with Louis Aboud of Wyre - Jun 13, 2019

    
    
    [Sean Lippel (@seanlippel)](https://twitter.com/seanlippel)

    I just published "Decentralized Finance Is a Continuum." The financial services sector is at the dawn of a new era of network-based finance — an era characterized by transparent, trustless, secure, and programmable financial products. [https://t.co/PCvvZahbxd](https://t.co/PCvvZahbxd)

    Likes

    443

    
    Twitter

    

    [https://unchainedpodcast.com/how-to-earn-money-on-collateral-in-defi-and-why-thats-risky/](https://unchainedpodcast.com/how-to-earn-money-on-collateral-in-defi-and-why-thats-risky/)

    [How to Earn Money on Collateral in DeFi — and Why That's Risky -...](https://unchainedpodcast.com/how-to-earn-money-on-collateral-in-defi-and-why-thats-risky/)

    Dan Elitzer, investor at IDEO CoLab Ventures, describes a concept he describes as superfluid collateral made possible in the decentralized finance space. We talk about which DeFi projects make this[...]Keep reading...

    
    

    [https://www.youtube.com/channel/UCuulLHp0eyXnl9yAao_pbEA/videos](https://www.youtube.com/channel/UCuulLHp0eyXnl9yAao_pbEA/videos)

    [@ChrisBlec](https://www.youtube.com/channel/UCuulLHp0eyXnl9yAao_pbEA/videos)

    Give me a follow on Twitter @ChrisBlec

    [https://yt3.ggpht.com/a/AGF-l7_jKmUyIeVeYXt5CM6urPhvTF94xcznn1zssQ=s900-c-k-c0xffffffff-no-rj-mo](https://yt3.ggpht.com/a/AGF-l7_jKmUyIeVeYXt5CM6urPhvTF94xcznn1zssQ=s900-c-k-c0xffffffff-no-rj-mo)

    [https://media.discordapp.net/attachments/345651531342807043/609027755564072968/unknown.png](https://media.discordapp.net/attachments/345651531342807043/609027755564072968/unknown.png)

    
    

    [https://kyber.network](https://kyber.network/)

    [Kyber Network | The On-Chain Liquidity Protocol](https://kyber.network/)

    Kyber Network is connecting the fragmented tokenized world by enabling instant and seamless transactions between platforms, ecosystems and other use cases. Read more...

    
    

    [https://www.delphidigital.io/defi](https://www.delphidigital.io/defi)

    [Delphi Digital | Research & Consulting](https://www.delphidigital.io/defi)

    Premier research & consulting boutique specializing in the digital asset market.

    
    

    [https://defitutorials.substack.com](https://defitutorials.substack.com/)

    [DeFi Tutorials](https://defitutorials.substack.com/)

    Hands on video tutorials using popular Open Finance ('DeFi') products to highlight best use cases & risks involved. Learn how to generate passive income, margin trade, and more.

    
    

    [https://medium.com/@dangerzhang/these-top-defi-apps-are-freeing-us-from-banks-83f724bc543e](https://medium.com/@dangerzhang/these-top-defi-apps-are-freeing-us-from-banks-83f724bc543e)

    [These Top DeFi Apps Are Freeing Us From Banks](https://medium.com/@dangerzhang/these-top-defi-apps-are-freeing-us-from-banks-83f724bc543e)

    Bitcoin promised to free us from banks, but Ethereum might be the technology that ends up really doing it, through DeFi.

    
    

    [https://twitter.com/NodarJ/status/1185539963368198144?s=20](https://twitter.com/NodarJ/status/1185539963368198144?s=20)

    
    [Nodar ♞ (@NodarJ)](https://twitter.com/NodarJ)

    It was an honor to appear as a guest on @RyanSAdams Bankless earlier this month to explore @MakerDAO. Enthused to make a follow-up post as Multi-Collateral Dai goes live. In the meantime, share this guide with anyone who wants to learn about CDPs:

    
    [https://t.co/CVZnjH](https://t.co/CVZnjH)

    ...

    
    Twitter

    

    [https://medium.com/alethio/statistics-around-dai-stablecoin-fb359d6881aa](https://medium.com/alethio/statistics-around-dai-stablecoin-fb359d6881aa)

    [The DeFi Series — Statistics Around DAI Stablecoin](https://medium.com/alethio/statistics-around-dai-stablecoin-fb359d6881aa)

    Christian Seberino, Danning Sui

    
    

    [https://twitter.com/cpix420/status/1187820841658191872?s=20](https://twitter.com/cpix420/status/1187820841658191872?s=20)

    
    [Collin Pixley (@cpix420)](https://twitter.com/cpix420)

    @jacobkostecki @InvestahD @metalpaysme Not the first and most certainly not the last. -MKR for CDP holders -DAI for permissionless crypto stability -MCO for credit lines/interest/rewards on @cryptocom -BAT for earning crypto to watch ads and tipping online content -SNX f...

    
    Twitter

    

    [https://defiprime.com/exchanges](https://defiprime.com/exchanges)

    [List of Decentralized Exchanges - Best DEX Decentralized exchanges](https://defiprime.com/exchanges)

    A decentralized exchange (DEX) is a cryptocurrency exchange which operates in a decentralized way, without a central authority.

    
- 🧩-banking

        Crypto⧉Finance

    🧩-banking

    4 messages

    

    [https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/) [https://bitcointalk.org/index.php?topic=2674704.0](https://bitcointalk.org/index.php?topic=2674704.0)

    [Which Banks Accept Bitcoin? Get The List | Banks.com](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/)

    The recent rash of banks announcing that they would stop accepting credit card transactions for bitcoin purchases has created a stir in the altcoin sphere.

    
    [Where to open a bank account for Crypto business](https://bitcointalk.org/index.php?topic=2674704.0)

    Where to open a bank account for Crypto business

    

    [https://twitter.com/leocaetano/status/1155018563482247168?s=12](https://twitter.com/leocaetano/status/1155018563482247168?s=12)

    
    [leocaetano (@leocaetano)](https://twitter.com/leocaetano)

    So proud to see a bank that invests in people - putting its customers first, simplifying and enhancing the banking experience - growing without limits over the last 5 years. For more companies like this in the world
    
    Twitter

    

    [https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000445700077](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000445700077)

    [‎The What Bitcoin Did Podcast: Silvergate's Alan Lane on Banking...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000445700077)

    ‎Show The What Bitcoin Did Podcast, Ep Silvergate's Alan Lane on Banking the Corporate Unbanked - Jul 30, 2019

    
    

    [https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/)

    [Which Banks Accept Bitcoin? Get The List | Banks.com](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/)

    The recent rash of banks announcing that they would stop accepting credit card transactions for bitcoin purchases has created a stir in the altcoin sphere.

    
- 🧩-airdrops

        Crypto⧉Finance

    🧩-airdrops

    4 messages

    

    [https://twitter.com/msantoriESQ/status/1029782145663991811](https://twitter.com/msantoriESQ/status/1029782145663991811)

    
    [Marco Santori (@msantoriESQ)](https://twitter.com/msantoriESQ)

    1/ Put on your parachute pants because today we're diving into AIRDROPS. Are they illegal? Can they be an alternative to an ICO? Did the SEC say yesterday that they’re forbidden? Is this all FUD??? Read on because this is gonna be a weird one.

    Likes

    230

    
    Twitter

    [https://coincenter.org/link/a-token-airdrop-may-not-spare-you-from-securities-regulation](https://coincenter.org/link/a-token-airdrop-may-not-spare-you-from-securities-regulation)

    [A token airdrop may not spare you from securities regulation. | Co...](https://coincenter.org/link/a-token-airdrop-may-not-spare-you-from-securities-regulation)

    Blockchain token based projects need network effects. There needs to be a mechanism for fairly and widely distributing tokens to in order for the project …

    
    [https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown](https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown)

    [China's Central Bank Moves to Restrict Free Crypto Giveaways - Coi...](https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown)

    The People's Bank of China, the country's central bank, is looking to clamp down on airdrops – free distributions of crypto tokens.

    
    

    [https://www.planetcompliance.com/2018/08/15/no-room-for-doubt-airdrops-and-bounty-programs-constitute-securities/](https://www.planetcompliance.com/2018/08/15/no-room-for-doubt-airdrops-and-bounty-programs-constitute-securities/)

    [No room for doubt: Airdrops and Bounty Programs Constitute Securit...](https://www.planetcompliance.com/2018/08/15/no-room-for-doubt-airdrops-and-bounty-programs-constitute-securities/)

    The latest SEC enforcement action is not only another case of ICO fraud. It's an important step for a wider regulatory framework for Blockchain projects

    
- 🧩-crypto-atm

        Crypto⧉Finance

    🧩-crypto-atm

    5 messages

    

    [https://coinatmradar.com/](https://coinatmradar.com/) [https://en.wikipedia.org/wiki/Bitcoin_ATM](https://en.wikipedia.org/wiki/Bitcoin_ATM) [https://www.buybitcoinworldwide.com/bitcoin-atms/](https://www.buybitcoinworldwide.com/bitcoin-atms/) [https://cryptalker.com/best-bitcoin-atm/](https://cryptalker.com/best-bitcoin-atm/)

    [Bitcoin ATM Map – Find Bitcoin ATM, Online Rates](https://coinatmradar.com/)

    Find Bitcoin ATM locations easily with our Bitcoin ATM Map. For many Bitcoin machines online rates are available.

    
    [Bitcoin ATM](https://en.wikipedia.org/wiki/Bitcoin_ATM)

    A Bitcoin ATM is a kiosk that allows a person to purchase Bitcoin by using cash or debit card. Some Bitcoin ATMs offer bi-directional functionality enabling both the purchase of Bitcoin as well as the sale of Bitcoin for cash. In some cases, Bitcoin ATM providers require user...

    
    [1001+ Bitcoin ATM Map Locations Near Me (2019 Updated)](https://www.buybitcoinworldwide.com/bitcoin-atms/)

    Sell

    [10 Best Bitcoin ATM Machines – Start Your Own Business - Cryptalker](https://cryptalker.com/best-bitcoin-atm/)

    Bitcoin ATM is not as widely used as fiat currency, however, there are now over 3,700 of automatic teller machines installed around the world.

    
    [https://github.com/mythril/skyhook](https://github.com/mythril/skyhook)

    [mythril/skyhook](https://github.com/mythril/skyhook)

    Skyhook, the open-source sub-$1000.00 Bitcoin ATM. Contribute to mythril/skyhook development by creating an account on GitHub.

    [https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000447036421](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000447036421) [https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446960692](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446960692) [https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446788790](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446788790) [https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446698547](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446698547)

    [‎The What Bitcoin Did Podcast: Hassan Khoshtaghaza on Operating ...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000447036421)

    ‎Show The What Bitcoin Did Podcast, Ep Hassan Khoshtaghaza on Operating a UK Bitcoin ATM - Aug 16, 2019

    
    [‎The What Bitcoin Did Podcast: Bitcoin ATM Regulations with Bill...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446960692)

    ‎Show The What Bitcoin Did Podcast, Ep Bitcoin ATM Regulations with Bill Repasky - Aug 15, 2019

    
    [‎The What Bitcoin Did Podcast: Karel Kyovsky on Manufacturing Bi...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446788790)

    ‎Show The What Bitcoin Did Podcast, Ep Karel Kyovsky on Manufacturing Bitcoin ATMs - Aug 13, 2019

    
    [‎The What Bitcoin Did Podcast: Operating Bitcoin ATMs with Gil V...](https://podcasts.apple.com/us/podcast/the-what-bitcoin-did-podcast/id1317356120?i=1000446698547)

    ‎Show The What Bitcoin Did Podcast, Ep Operating Bitcoin ATMs with Gil Valentine & Eric Gravengaard from Athena Bitcoin - Aug 12, 2019

    
    

    [https://www.justice.gov/usao-cdca/pr/westwood-man-agrees-plead-guilty-federal-narcotics-money-laundering-charges-running](https://www.justice.gov/usao-cdca/pr/westwood-man-agrees-plead-guilty-federal-narcotics-money-laundering-charges-running)

    [Westwood Man Agrees to Plead Guilty to Federal Narcotics, Money La...](https://www.justice.gov/usao-cdca/pr/westwood-man-agrees-plead-guilty-federal-narcotics-money-laundering-charges-running)

    A Westwood man has agreed to plead guilty to federal criminal charges for owning and operating an unlicensed money transmitting business where he exchanged up to $25 million in cash and virtual currency for individuals, including Darknet drug dealers and other criminals, some...

    
    

    [https://podcasts.apple.com/us/podcast/the-world-crypto-network-podcast/id825708806?i=1000447767535](https://podcasts.apple.com/us/podcast/the-world-crypto-network-podcast/id825708806?i=1000447767535)

    [‎The World Crypto Network Podcast: Joesmoe Show - #2 (8 22 19) -...](https://podcasts.apple.com/us/podcast/the-world-crypto-network-podcast/id825708806?i=1000447767535)

    ‎Show The World Crypto Network Podcast, Ep Joesmoe Show - #2 (8 22 19) - IRS Letters, Database hacks, MtGox - Aug 25, 2019

    ![https://is3-ssl.mzstatic.com/image/thumb/Podcasts123/v4/b4/54/26/b45426cf-eb11-158a-1b4d-26ed59cc6ce6/mza_2736761568329494015.jpg/1200x630wp.png](https://is3-ssl.mzstatic.com/image/thumb/Podcasts123/v4/b4/54/26/b45426cf-eb11-158a-1b4d-26ed59cc6ce6/mza_2736761568329494015.jpg/1200x630wp.png)