# 🌍-european-regulation

[https://www.aclu.org/blog/ghost-user-ploy-break-encryption-wont-work](https://www.aclu.org/blog/ghost-user-ploy-break-encryption-wont-work)

[When You Have Data, They Will Come](https://www.aclu.org/blog/when-you-have-data-they-will-come)

Note: This is part two of a four-part series where security expert Jon Callas breaks down the fatal flaws of a recent proposal to add a secret user — the government — to our encrypted conversations. Part one can be found here.A recent essay by technical leaders of Britain...

[Adding a Ghost User to Our Encrypted Communications Is No 'Crocodi...](https://www.aclu.org/blog/adding-ghost-user-our-encrypted-communications-no-crocodile-chip)

Note: This is part three of a four-part series where security expert Jon Callas breaks down the fatal flaws of a recent proposal to add a secret user — the government — to our encrypted conversations. Part two can be found here.The latest intelligence community proposal f...

[The Recent Ploy to Break Encryption Is An Old Idea Proven Wrong](https://www.aclu.org/blog/recent-ploy-break-encryption-old-idea-proven-wrong)

This is the fourth and final in a series of essays about a proposal by officials at Britain’s GCHQ about requiring encrypted communications platforms to be designed to secretly add an extra participant — the government — to a conversation. In the previous essay, I expla...

[The 'Ghost User' Ploy to Break Encryption Won't Work](https://www.aclu.org/blog/ghost-user-ploy-break-encryption-wont-work)

Note: This is part one of a four-part series where security expert Jon Callas breaks down the fatal flaws of a recent proposal to add a secret user — the government — to our encrypted conversations.Twenty-five years ago, the FBI decided it needed a surveillance system bui...

[https://twitter.com/finck_m/status/1073563202812731392](https://twitter.com/finck_m/status/1073563202812731392) [https://www.coindesk.com/italys-senate-moves-to-set-legal-foundation-for-blockchain-timestamps](https://www.coindesk.com/italys-senate-moves-to-set-legal-foundation-for-blockchain-timestamps) [https://bitcointalk.org/index.php?topic=129461.0](https://bitcointalk.org/index.php?topic=129461.0) (edited)

[Handbook on European data protection law : 2018 edition.](https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en)

The rapid development of information technology has exacerbated the need for robust personal data protection, the right to which is safeguarded by both European Union (EU) and Council of Europe (CoE) instruments. Safeguarding this important right entails new and signifi cant ...

[https://publications.europa.eu/portal2012-services/thumbnail/cellar/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1.0001.01/DOC_2](https://publications.europa.eu/portal2012-services/thumbnail/cellar/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1.0001.01/DOC_2)

[Michèle Finck (@finck_m)](https://twitter.com/finck_m)

Over the past weeks, the

Parliament has issued two reports on #blockchain. I'll summarize the key points below.

[Italy's Senate Moves to Set Legal Foundation for Blockchain Timest...](https://www.coindesk.com/italys-senate-moves-to-set-legal-foundation-for-blockchain-timestamps)

The Italian government could soon amend a law to allow legalized blockchain verification of documents.

[Bitcoin-Central, first exchange licensed to operate with a bank. T...](https://bitcointalk.org/index.php?topic=129461.0)

Bitcoin-Central, first exchange licensed to operate with a bank. This is HUGE

[https://cointelegraph.com/news/new-eu-directive-sets-stricter-transparency-rules-for-digital-currencies](https://cointelegraph.com/news/new-eu-directive-sets-stricter-transparency-rules-for-digital-currencies)

[New EU Directive Sets Stricter Transparency Rules for Digital Curr...](https://cointelegraph.com/news/new-eu-directive-sets-stricter-transparency-rules-for-digital-currencies)

The EU Fifth Anti-Money Laundering Directive has come into force, setting new rules for European financial watchdogs to monitor cryptocurrencies

FinCEN’s interpretation, as Coin Center notes, only brings persons who have “independent control” over another person’s crypto assets under the purview of the BSA, excluding those that merely enable exchange or transmission — for example, open source software developers, multiple-signature service providers, and decentralized exchange facilitators.

[https://complyadvantage.com/knowledgebase/regulation/mifid/](https://complyadvantage.com/knowledgebase/regulation/mifid/)

There are several key aspects of MiFID that are meant to aid the regulation of the financial industry. One of these is the requirement of client categorization. Due to MiFID, firms are required to place their clients into categories in order to determine the level of protection that is needed with their types of accounts and investments. MiFID also requires that firms abide by both pre-trade and post-trade transparency. Pre-trade transparency means that those who operate order-matching systems must make information regarding the five best pricing levels (on both buy and sell side) available to all. Similarly, those who run quote-driven markets must make the best bids and offers publicly available. Post-trade transparency is a similar concept, but differs slightly. By requiring post-trade transparency, MiFID requires that firms release information regarding the price, time, and volume of all trades pertaining to listed shares, even if they are not executed in an open market scenario. There are certain circumstances where deferred publication may be granted, but that varies from case to case and must be dealt with on an individual level.

[What is MiFID - The Markets in Financial Instruments Directive ?](https://complyadvantage.com/knowledgebase/regulation/mifid/)

MiFID is a law that was created by the European Union for the purpose of regulating all investment services in member states of the European Economic Area.

[https://www.investopedia.com/terms/m/mifid.asp](https://www.investopedia.com/terms/m/mifid.asp)

[Markets in Financial Instruments Directive (MiFID)](https://www.investopedia.com/terms/m/mifid.asp)

MiFID is a European Union law that standardizes regulation for investment services across all member states of the European Economic Area.

AMSE | Aix-Marseille School of Economics Marseille, 11 December 2018 Speech by Denis Beau First Deputy Governor “Trends in financial intermediation and implications for the regulation and supervision of the European financial sector” [https://www.bis.org/review/r181213a.pdf](https://www.bis.org/review/r181213a.pdf)

“Fin-RegTech: Regulatory challenges with emphasis on Europe” Keynote speech by Professor John (Iannis) Mourmouras Senior Deputy Governor Bank of Greece Former Deputy Finance Minister Former Chief Economic Advisor to the Prime Minister [https://www.bis.org/review/r190318m.pdf](https://www.bis.org/review/r190318m.pdf)

[https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/13-cryptocurrency-compliance-and-risks-a-european-kycaml-perspective](https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/13-cryptocurrency-compliance-and-risks-a-european-kycaml-perspective)

[International legal business solutions - Global Legal Insights](https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/13-cryptocurrency-compliance-and-risks-a-european-kycaml-perspective)

Provides essential insights into the current legal issues, readers with expert analysis of legal, economic and policy developments with the world's leading lawyers.

[http://www.europarl.europa.eu/cmsdata/150761/TAX3%20Study%20on%20cryptocurrencies%20and%20blockchain.pdf](http://www.europarl.europa.eu/cmsdata/150761/TAX3%2520Study%2520on%2520cryptocurrencies%2520and%2520blockchain.pdf)

[https://www.planetcompliance.com/2019/01/09/eba-reports-on-crypto-assets/](https://www.planetcompliance.com/2019/01/09/eba-reports-on-crypto-assets/)

[EBA reports on crypto-assets - Planet Compliance](https://www.planetcompliance.com/2019/01/09/eba-reports-on-crypto-assets/)

The European Banking Authority (EBA) published today the results of its assessment of the applicability...

[https://europeanchamberofdigitalcommerce.com/the-legal-status-of-security-tokens-in-europe/](https://europeanchamberofdigitalcommerce.com/the-legal-status-of-security-tokens-in-europe/)

[The legal status of Security Tokens in Europe - European Chamber o...](https://europeanchamberofdigitalcommerce.com/the-legal-status-of-security-tokens-in-europe/)

This article describes the legal context in seven European countries regarding Security Token Offerings. Such offerings differ from Initial Coin Offerings (ICO’s). An Initial Coin Offering (ICO) is the cryptocurrency equivalent to an Initial Public Offering (offering shares...

Portugal’s tax authority says crypto trading and payments are tax-free - The Block [https://www.theblockcrypto.com/tiny/portugals-tax-authority-says-crypto-trading-and-payments-are-tax-free/](https://www.theblockcrypto.com/tiny/portugals-tax-authority-says-crypto-trading-and-payments-are-tax-free/)

[Portugal’s tax authority says crypto trading and payments are ta...](https://www.theblockcrypto.com/tiny/portugals-tax-authority-says-crypto-trading-and-payments-are-tax-free/)

Cryptocurrency trading and payments in Portugal are tax-free, the European country’s tax authority has clarified. Local business newspaper Jornal de Negócios reported the news earlier this week, saying that the authority has said that both cryptocurrency trading in real cu...

- 🌍-gibraltar-regulation

        Crypto⧉Finance

    🌍-gibraltar-regulation

    10 messages

    

    [https://kintu.co/crypto-companies-gibraltar/](https://kintu.co/crypto-companies-gibraltar/)

    [20 Crypto Companies in Gibraltar you should get to know](https://kintu.co/crypto-companies-gibraltar/)

    Gibraltar was also one of the first states to have successfully passed legislation around crypto. We take a look at who has moved their operations there.

    
    

    [Gibraltar introduces a legal framework for cryptocurrency business...](https://www.nomoretax.eu/gibraltar-legal-framework-cryptocurrency/)

    Regulation, use, and taxation of cryptocurrencies are topics that receive an increasing interest among businesses and regulatory authorities.

    
    [https://news.bitcoin.com/5-crypto-exchanges-have-been-licensed-in-gibraltar-since-regulation/](https://news.bitcoin.com/5-crypto-exchanges-have-been-licensed-in-gibraltar-since-regulation/)

    [5 Crypto Exchanges Have Been Licensed in Gibraltar Since Regulatio...](https://news.bitcoin.com/5-crypto-exchanges-have-been-licensed-in-gibraltar-since-regulation/)

    Several known crypto companies have been granted licenses to operate from Gibraltar since the British overseas territory adopted business-friendly Several known crypto companies have been granted licenses to operate from Gibraltar since the British overseas territory adopted ...

    
    [https://offshoreincorporate.com/where-to-setup-an-ico-gibraltar/](https://offshoreincorporate.com/where-to-setup-an-ico-gibraltar/) [http://www.gibraltarlaw.com/dlt-regulation-gibraltar/](http://www.gibraltarlaw.com/dlt-regulation-gibraltar/) [https://www.gibraltarlawyers.com/practice/fintech](https://www.gibraltarlawyers.com/practice/fintech)

    [DLT Regulation in Gibraltar - The Nine Principles](http://www.gibraltarlaw.com/dlt-regulation-gibraltar/)

    On January the first, 2018, Gibraltar's DLT Regulations came into force, based on nine core principles outlined here.

    
    [The leading DLT lawyers in Gibraltar for ICOs (Initial Coin Offeri...](https://www.gibraltarlawyers.com/practice/fintech)

    The leading DLT lawyers in Gibraltar for ICOs (Initial Coin Offerings) and general Fintech, Cryptocurrency, Block chain and Distributed Ledger Technology business

    

    [https://flagtheory.com/category/gibraltar/](https://flagtheory.com/category/gibraltar/)

    [Why and how to set up a company, trust and banking in Gibraltar - ...](https://flagtheory.com/category/gibraltar/)

    Learn why you should consider setting up an offshore company or a trust or open an offshore bank account in Gibraltar. Gibraltar is a crypto-friendly jurisdiction working on new regulations for ICOs issuing utility tokens.

    
    [https://complyadvantage.com/knowledgebase/crypto-regulations/cryptocurrency-regulations-gibraltar/](https://complyadvantage.com/knowledgebase/crypto-regulations/cryptocurrency-regulations-gibraltar/)

    [Crypto Regulations in Gibraltar](https://complyadvantage.com/knowledgebase/crypto-regulations/cryptocurrency-regulations-gibraltar/)

    Gibraltar is a global leader in cryptocurrency regulation: cryptocurrency is not considered legal tender in the country but cryptocurrency exchanges are legal and operate within a well-defined regulatory framework.

    
    [https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905](https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905)

    [Launching an ICO that is ready to bridge the old and new worlds fr...](https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905)

    Decisions (and Snags) in our Legal, Regulatory, and Financial Backstory

    
    [https://flagtheory.com/dlt-license-crypto-exchange-gibraltar/](https://flagtheory.com/dlt-license-crypto-exchange-gibraltar/)

    [Why you should consider setting up your crypto exchange in Gibralt...](https://flagtheory.com/dlt-license-crypto-exchange-gibraltar/)

    Gibraltar Financial Services Regulator has introduced a specific regulatory framework for blockchain companies: the DLT Provider License. Learn what are the benefits of setting up your regulated crypto exchange in Gibraltar and why Gibraltar is becoming the next Blockchain hub.

    
    [https://incorporations.io/gibraltar](https://incorporations.io/gibraltar)

    [Set up a company and open a bank account in Gibraltar - Incorporat...](https://incorporations.io/gibraltar)

    Incorporate a company in Gibraltar, open an offshore bank account, and learn what are the legal requirements and taxes in Gibraltar to set up an international business.

    [https://www.coindesk.com/gibraltar-to-create-license-for-blockchain-startups](https://www.coindesk.com/gibraltar-to-create-license-for-blockchain-startups)

    [Gibraltar to Launch License Scheme for Blockchain Startups - CoinDesk](https://www.coindesk.com/gibraltar-to-create-license-for-blockchain-startups)

    Gibraltar will publish guidance explaining how to apply its new blockchain legislation to startups on Friday.

    
- 🌍-uk-regulation

        Crypto⧉Finance

    🌍-uk-regulation

    21 messages

    

    [UK Tax Agency Publishes Detailed Guidance for Crypto Holders - Coi...](https://www.coindesk.com/uk-tax-agency-publishes-detailed-guidance-for-crypto-holders)

    U.K. tax body HMRC has provided an in-depth explanation of how cryptocurrency users should pay taxes on their holdings.

    
    

    House of Commons Treasury Committee Crypto-assets [https://publications.parliament.uk/pa/cm201719/cmselect/cmtreasy/910/910.pdf](https://publications.parliament.uk/pa/cm201719/cmselect/cmtreasy/910/910.pdf) (edited)

    

    [https://coincenter.org/entry/comment-to-her-majesty-s-treasury-on-transposition-of-the-fifth-money-laundering-directive](https://coincenter.org/entry/comment-to-her-majesty-s-treasury-on-transposition-of-the-fifth-money-laundering-directive)

    [Comment to Her Majesty’s Treasury on Transposition of the Fifth ...](https://coincenter.org/entry/comment-to-her-majesty-s-treasury-on-transposition-of-the-fifth-money-laundering-directive)

    We urge HM Treasury to refrain from an over-broadening of its AMF/CFT regulation, as such an expansion would violate UK citizens’ free speech and privacy …

    
    [https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/795670/20190415_Consultation_on_the_Transposition_of_5MLD__web.pdf](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/795670/20190415_Consultation_on_the_Transposition_of_5MLD__web.pdf)

    [https://www.theblockcrypto.com/2019/07/16/uk-regulator-looking-to-hire-series-of-cryptocurrency-specialists/](https://www.theblockcrypto.com/2019/07/16/uk-regulator-looking-to-hire-series-of-cryptocurrency-specialists/)

    [UK regulator looking to hire series of cryptocurrency specialists ...](https://www.theblockcrypto.com/2019/07/16/uk-regulator-looking-to-hire-series-of-cryptocurrency-specialists/)

    The UK’s Financial Conduct Authority (FCA) is getting serious about cryptocurrencies. According to a job posting, it is looking to hire a Crypto Intelligence Associate to join its Intelligence Services Team. It is also seeking a “Crypto-asset Specialist Supervisor” as p...

    
    [https://bitcoinmagazine.com/articles/survey-u-k-crypto-companies-must-bank-abroad](https://bitcoinmagazine.com/articles/survey-u-k-crypto-companies-must-bank-abroad)

    [Crypto Companies in U.K. Must Bank Abroad » Bitcoin Magazine](https://bitcoinmagazine.com/articles/survey-u-k-crypto-companies-must-bank-abroad)

    According to research from CryptoUK, nearly three-quarters of crypto companies in the United Kingdom are forced to open foreign bank accounts.

    
    [https://www.fca.org.uk/publication/consultation/cp19-22.pdf](https://www.fca.org.uk/publication/consultation/cp19-22.pdf)

    Prohibiting the sale to retail clients of investment products that reference cryptoassets

    We recognise that our proposals may encourage some retail consumers to ‘invest’ directly in unregulated tokens. We do not consider these forms of tokens to be appropriate investments for retail consumers, and will continue to warn consumers about them through our ScamSmart pages. The potential risk of retail consumers investing directly in unregulated tokens does not alter our proposals to ban those products within our regulatory remit which we assess as harmful to investors. We must act in line with our objectives and protect consumers from harmful regulated activities and investments within our perimeter.

    (edited)

    [https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/752070/cryptoassets_taskforce_final_report_final_web.pdf](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/752070/cryptoassets_taskforce_final_report_final_web.pdf)

    Guidance on Cryptoassets: UK Financial Conduct Authority Issues Consultation Paper

    [https://www.lexology.com/library/detail.aspx?g=00338a10-750b-4d59-8bd8-471812ff6fab](https://www.lexology.com/library/detail.aspx?g=00338a10-750b-4d59-8bd8-471812ff6fab)

    on the other hand, “exchange tokens” (such as Bitcoin, Ether or Litecoin) and “utility tokens” (which grant holders access to a current or prospective product or service but do not grant holders rights to profit or ownership) would be unlikely to constitute a “specified investment”.

    [Guidance on Cryptoassets: UK Financial Conduct Authority Issues Co...](https://www.lexology.com/library/detail.aspx?g=00338a10-750b-4d59-8bd8-471812ff6fab)

    On January 23, 2019, the UK Financial Conduct Authority (“FCA”) issued its widely anticipated “Guidance on Cryptoassets” consultation paper CP19/3…

    
    
    [https://www.fca.org.uk/publication/consultation/cp19-03.pdf](https://www.fca.org.uk/publication/consultation/cp19-03.pdf)

    How do I know if my token is a Specified Investment? Given the complexity of many tokens, it is not always easy to determine whether a token is a Specified Investment, specifically those types of Specified Investment that are securities, like shares or debt instruments. There are a few factors that are indicative of a security. These factors may include, but are not limited to: • the contractual rights and obligations the token-holder has by virtue of holding or owning that cryptoasset • any contractual entitlement to profit-share (like dividends), revenues, or other payment or benefit of any kind • any contractual entitlement to ownership in, or control of, the token issuer or other relevant person (like voting rights) • the language used in relevant documentation, like token ‘whitepapers’, that suggests the tokens are intended to function as an investment, although it should be noted that the substance of the token will determine whether an instrument is a Specified Investment, and not necessarily the labels used–For example, if a whitepaper declares a token to be a utility token, but the contractual rights that it confers would make it a Specified Investment, we would consider it to be a security token. • whether the token is transferable and tradeable on cryptoasset exchanges or any other type of exchange or market • a direct flow of payment from the issuer or other relevant party to token holders may be one of the indicators that the token is a security, although an indirect flow of payment (for instance through profits or payments derived exclusively from the secondary market) would not necessarily indicate the contrary.

    [https://www.fca.org.uk/publication/finalised-guidance/fca-approach-payment-services-electronic-money-2017.pdf](https://www.fca.org.uk/publication/finalised-guidance/fca-approach-payment-services-electronic-money-2017.pdf)

    Payment Services and Electronic Money –Our Approach The FCA’s role under the Payment Services Regulations 2017 and the Electronic Money Regulations 2011

    [https://www.bcl.com/an-update-on-the-regulation-of-cryptoassets/](https://www.bcl.com/an-update-on-the-regulation-of-cryptoassets/)

    [An update on the regulation of Cryptoassets – Lawyers, Solicitor...](https://www.bcl.com/an-update-on-the-regulation-of-cryptoassets/)

    Hannah Raphael discusses the UK’s policy and regulatory approach to cryptoassets. 2018 was a bad year for cryptocurrency. Bitcoin fell from its heady highs of $20,000 per coin at the …

    
    [https://www.zdnet.com/article/uk-regulator-looks-to-ban-crypto-derivatives/](https://www.zdnet.com/article/uk-regulator-looks-to-ban-crypto-derivatives/)

    [UK regulator looks to ban crypto-derivatives | ZDNet](https://www.zdnet.com/article/uk-regulator-looks-to-ban-crypto-derivatives/)

    The FCA says it is clear that crypto-derivatives and exchange traded notes are unsuitable investments for retail consumers.

    
    

    [https://cointelegraph.com/news/crypto-advocacy-center-says-proposed-uk-aml-regulations-violate-privacy-rights](https://cointelegraph.com/news/crypto-advocacy-center-says-proposed-uk-aml-regulations-violate-privacy-rights)

    [Crypto Advocacy Center Says Proposed UK AML Regulations Violate Pr...](https://cointelegraph.com/news/crypto-advocacy-center-says-proposed-uk-aml-regulations-violate-privacy-rights)

    Nonprofit crypto advocacy center Coin Center has urged Her Majesty’s Treasury not to over-broaden the scope of the U.K.’s anti-money laundering regulations

    
    

    [https://www.eff.org/deeplinks/2019/06/eff-and-open-rights-group-defend-right-publish-open-source-software-uk-government](https://www.eff.org/deeplinks/2019/06/eff-and-open-rights-group-defend-right-publish-open-source-software-uk-government)

    [EFF and Open Rights Group Defend the Right to Publish Open Source ...](https://www.eff.org/deeplinks/2019/06/eff-and-open-rights-group-defend-right-publish-open-source-software-uk-government)

    EFF and Open Rights Group today submitted formal comments to the British Treasury, urging restraint in applying anti-money-laundering regulations to the publication of open-source software.The UK government sought public feedback on proposals to update its financial regulatio...

    
    

    [https://www.fca.org.uk/publication/policy/ps19-22.pdf](https://www.fca.org.uk/publication/policy/ps19-22.pdf)

    

    [https://btcmanager.com/u-k-cryptoassets-taskforce-report-cryptocurrencies-dlt/](https://btcmanager.com/u-k-cryptoassets-taskforce-report-cryptocurrencies-dlt/)

    [U.K. Cryptoassets Taskforce Publishes Final Report on Cryptocurren...](https://btcmanager.com/u-k-cryptoassets-taskforce-report-cryptocurrencies-dlt/)

    The Cryptoassets Taskforce consisting of the HM Treasury, Financial Conduct Authority (FCA), and the Bank of England recently released a Cryptoassets Final Report on October 29, 2018. According to the U.K. Government, the report provides a high-level overview of the U.K.’s ...

    
    

    [https://cryptodaily.co.uk/2018/08/crypto-tax-and-ico-regulations-in-the-united-kingdom/](https://cryptodaily.co.uk/2018/08/crypto-tax-and-ico-regulations-in-the-united-kingdom/)

    [Crypto Tax And ICO Regulations In The United Kingdom](https://cryptodaily.co.uk/2018/08/crypto-tax-and-ico-regulations-in-the-united-kingdom/)

    Welcome to Crypto Daily News, this news piece "Crypto Tax And ICO Regulations In The United Kingdom" is breaking news from the Crypto sector.

    

    [https://www.planetcompliance.com/2018/11/23/the-future-of-uk-crypto-regulation-the-fcas-cryptoasset-taskforce-and-recent-events/](https://www.planetcompliance.com/2018/11/23/the-future-of-uk-crypto-regulation-the-fcas-cryptoasset-taskforce-and-recent-events/)

    [The Future of UK Crypto Regulation - The FCA’s Cryptoasset Taskf...](https://www.planetcompliance.com/2018/11/23/the-future-of-uk-crypto-regulation-the-fcas-cryptoasset-taskforce-and-recent-events/)

    The crypto world is not coming to an end. The market downturn will influence heavily though the future of Cryptoasset regulation globally and in particular in the UK.

    
    

    [https://cointelegraph.com/news/crypto-regulations-for-uk-could-take-two-years-says-legal-expert](https://cointelegraph.com/news/crypto-regulations-for-uk-could-take-two-years-says-legal-expert)

    [Crypto Regulations for UK Could Take Two Years, Says Legal Expert](https://cointelegraph.com/news/crypto-regulations-for-uk-could-take-two-years-says-legal-expert)

    An expert from major British law firm RPC stated that the introduction of a crypto regulatory framework in the U.K. could take two years.

    
- 🌍-netherlands

        Crypto⧉Finance

    🌍-netherlands

    6 messages

    

    [https://thenextweb.com/hardfork/2019/07/01/dutch-ministers-push-for-cryptocurrency-regulation-to-fight-money-laundering/](https://thenextweb.com/hardfork/2019/07/01/dutch-ministers-push-for-cryptocurrency-regulation-to-fight-money-laundering/)

    [Dutch ministers push for cryptocurrency regulation to fight money ...](https://thenextweb.com/hardfork/2019/07/01/dutch-ministers-push-for-cryptocurrency-regulation-to-fight-money-laundering/)

    Dutch ministers want to regulate cryptocurrencies as part of a wider effort to tackle money laundering in the country.

    [https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2019%2F07%2FUntitled-design-51.jpg&signature=e1c2d22e2ab8917155736ef31f4bbb1a](https://img-cdn.tnwcdn.com/image/hardfork?filter_last=1&fit=1280%2C640&url=https%3A%2F%2Fcdn0.tnwcdn.com%2Fwp-content%2Fblogs.dir%2F1%2Ffiles%2F2019%2F07%2FUntitled-design-51.jpg&signature=e1c2d22e2ab8917155736ef31f4bbb1a)

    [https://coinlaw.io/cryptoassets-and-blockchain-regulations-in-the-netherlands/](https://coinlaw.io/cryptoassets-and-blockchain-regulations-in-the-netherlands/)

    [Cryptoassets and Blockchain Regulations in the Netherlands - Coinl...](https://coinlaw.io/cryptoassets-and-blockchain-regulations-in-the-netherlands/)

    the state of crypto regulation the Netherlands in 2019

    
    [https://www.coindesk.com/dutch-financial-authorities-plan-licensing-scheme-for-crypto-exchanges](https://www.coindesk.com/dutch-financial-authorities-plan-licensing-scheme-for-crypto-exchanges)

    [Dutch Financial Authorities Plan Licensing Scheme for Crypto Excha...](https://www.coindesk.com/dutch-financial-authorities-plan-licensing-scheme-for-crypto-exchanges)

    Financial authorities in the Netherlands plan a licensing scheme for crypto exchanges and wallet providers to lower the risk of financial crimes.

    
    

    [https://www2.deloitte.com/nl/nl/pages/risk/articles/how-will-new-regulation-affect-the-crypto-market.html](https://www2.deloitte.com/nl/nl/pages/risk/articles/how-will-new-regulation-affect-the-crypto-market.html)

    There are, however, some reservations about the proposed law. The rules for companies dealing with cryptocurrencies have now become as strict as those applying to banks. Even if someone wants to buy only 10 euro worth of Bitcoin, an exchange must ask for identification. This seems excessive, especially if the person is already identified through the Dutch bank account they are using to pay. If the law is strictly interpreted — which is often the case shortly after a law’s introduction — then it could possibly overshoot its goal. It makes sense that companies dealing with large amounts of cryptocurrency must be thoroughly investigated. But if every individual with a small crypto wallet has to be identified as a high-risk customer, this will lead to the creation of large databases of personal data that bring about other, disproportional risks. In addition, a strict interpretation of the law might potentially slow down further innovation in the Dutch crypto market. Fortunately, Dutch regulators are willing to start conversations with cryptocurrency companies, as demonstrated by the sandbox environment provided for blockchain companies by the DNB and AFM. This demonstrates a willingness to start a dialogue with cryptocurrency companies in the hopes of achieving a risk-based approach to the new regulations.

    [How will new regulation affect the crypto market?](https://www2.deloitte.com/nl/nl/pages/risk/articles/how-will-new-regulation-affect-the-crypto-market.html)

    The Netherlands wants to start regulating cryptocurrency companies to prevent the virtual currency being used for criminal purposes.

    
    Dutch Proposed Crypto License Could Crush Startups The proposed crypto license would require crypto companies to store large amounts of customer data. [https://www.financemagnates.com/cryptocurrency/news/netherlands-proposed-crypto-license-could-crush-startups/](https://www.financemagnates.com/cryptocurrency/news/netherlands-proposed-crypto-license-could-crush-startups/)

    [https://medium.com/nextexchange/next-exchange-visits-the-dutch-parliament-in-pursuit-of-smart-regulations-for-cryptocurrency-fd1edae3c158](https://medium.com/nextexchange/next-exchange-visits-the-dutch-parliament-in-pursuit-of-smart-regulations-for-cryptocurrency-fd1edae3c158)

    [NEXT.exchange Visits the Dutch Parliament in Pursuit of Smart Regu...](https://medium.com/nextexchange/next-exchange-visits-the-dutch-parliament-in-pursuit-of-smart-regulations-for-cryptocurrency-fd1edae3c158)

    Amsterdam, on April 2nd, Christiaan Van Steenbergen, the CEO of NEXT.exchange attended a discussion focused on moving Netherlands to the…

    
- 🌍-malta-regulation

        Crypto⧉Finance

    🌍-malta-regulation

    17 messages

    

    [https://cointelegraph.com/news/malta-passes-blockchain-bills-into-law-confirming-malta-as-the-blockchain-island](https://cointelegraph.com/news/malta-passes-blockchain-bills-into-law-confirming-malta-as-the-blockchain-island) [https://www.forbes.com/sites/rachelwolfson/2018/07/05/maltese-parliament-passes-laws-that-set-regulatory-framework-for-blockchain-cryptocurrency-and-dlt/](https://www.forbes.com/sites/rachelwolfson/2018/07/05/maltese-parliament-passes-laws-that-set-regulatory-framework-for-blockchain-cryptocurrency-and-dlt/) [https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/malta](https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/malta) [https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html](https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html) (edited)

    [Malta Passes Blockchain Bills Into Law, ‘Confirming Malta as the...](https://cointelegraph.com/news/malta-passes-blockchain-bills-into-law-confirming-malta-as-the-blockchain-island)

    The Parliament of Malta has passed three bills into law that establish a regulatory framework for the blockchain sector in the country.

    
    [Maltese Parliament Passes Laws That Set Regulatory Framework For B...](https://www.forbes.com/sites/rachelwolfson/2018/07/05/maltese-parliament-passes-laws-that-set-regulatory-framework-for-blockchain-cryptocurrency-and-dlt/)

    The Maltese Parliament has passed 3 bills into law, establishing the first regulatory framework for blockchain, cryptocurrency and DLT Technology (Distributed Ledger Technology). This makes Malta the first country in the world to provide regulations for operators in the block...

    
    [International legal business solutions - Global Legal Insights](https://www.globallegalinsights.com/practice-areas/blockchain-laws-and-regulations/malta)

    Provides essential insights into the current legal issues, readers with expert analysis of legal, economic and policy developments with the world's leading lawyers.

    
    By NATHANIEL POPPER

    [Have a Cryptocurrency Company? Bermuda, Malta or Gibraltar Wants You](https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html)

    With their eyes on blockchain jobs and revenue, small countries and territories are competing to become the go-to destinations for entrepreneurs and projects.

    
    

    [https://mdia.gov.mt/acts/](https://mdia.gov.mt/acts/)

    The government’s willingness to attract blockchain companies is already paying off as the first and third exchange by trading volume, Binance and OKex have already officially announced that they will establish headquarters on the European island.

    (edited)

    

    [https://mdia.gov.mt/wp-content/uploads/2018/11/LN-355.pdf](https://mdia.gov.mt/wp-content/uploads/2018/11/LN-355.pdf)

    [https://www.coindesk.com/malta-needs-to-up-its-aml-game-as-crypto-sector-grows-says-eu](https://www.coindesk.com/malta-needs-to-up-its-aml-game-as-crypto-sector-grows-says-eu)

    [Malta Needs to Up Its AML Game As Crypto Sector Grows, Says EU - C...](https://www.coindesk.com/malta-needs-to-up-its-aml-game-as-crypto-sector-grows-says-eu)

    Malta should increase its anti-money laundering policing to match the growth in financial and crypto services, according to the EU.

    
    [http://justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29080&l=1](http://justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29080&l=1)

    AN ACT to provide for the establishment of an Authority to be known as the Malta Digital Innovation Authority, to support the development and implementation of the guiding principles described in this Act and to promoteconsistent principles for the development of visions, skills, and other qualities relating to technology innovation, including distributed or decentralise technology, and to exercise regulatory functions regarding innovative technology, arrangements and related services and to make provision with respect to matters ancillary thereto or connected therewith

    [http://www.justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29078&l=1](http://www.justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29078&l=1)

    AN ACT to provide for the regulation of designated innovative technology arrangements referred to in the Act, as well as of designated innovative technology services referred to in the Act, and for the exercise by or on behalf of the MaltaDigital Innovation Authority of regulatory functions with regard thereto.

    [https://www.emd.com.mt/innovative-technology-arrangements-and-services-act/](https://www.emd.com.mt/innovative-technology-arrangements-and-services-act/)

    [Innovative Technology Arrangements and Services Act in Malta | EMD](https://www.emd.com.mt/innovative-technology-arrangements-and-services-act/)

    On the 26 June 2018, Maltese Parliament approved the three bills regulating blockchain technology and cryptocurrencies in Malta. Whilst the Virtual Financial Assets Act creates a framework for the regulation of ICOs, cryptocurrencies, cryptocurrency exchanges and other interm...

    
    [Presenting the Malta Digital Innovation Authority Act](https://www.financemalta.org/publications/articles-interviews/articles-and-interviews-detail/second-reading-in-parliament-for-blockchain-bills-the-malta-digital-innovation-authority-act/)

    (edited)

    [http://justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29079&l=1](http://justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29079&l=1)

    AN ACT to regulate the field of Initial Virtual Financial Asset Offerings and Virtual Financial Assets and to make provision for matters ancillary or incidental thereto or connected therewith

    [https://www.grantthornton.com.mt/industry/fintech-and-innovation/The-Malta-Virtual-Financial-Asset-Act/](https://www.grantthornton.com.mt/industry/fintech-and-innovation/The-Malta-Virtual-Financial-Asset-Act/)

    [The Malta Virtual Financial Assets Act](https://www.grantthornton.com.mt/industry/fintech-and-innovation/The-Malta-Virtual-Financial-Asset-Act/)

    The Virtual Financial Assets Act provides a sound legislative framework for Malta to regulate and responsibly promote the use of cryptocurrencies, crypto-exchanges and other crypto-related services, through which Malta aims to promote further technological innovations and gro...

    
    

    [https://www.forbes.com/sites/rachelwolfson/2018/07/05/maltese-parliament-passes-laws-that-set-regulatory-framework-for-blockchain-cryptocurrency-and-dlt/#320a4da049ed](https://www.forbes.com/sites/rachelwolfson/2018/07/05/maltese-parliament-passes-laws-that-set-regulatory-framework-for-blockchain-cryptocurrency-and-dlt/#320a4da049ed)

    [Maltese Parliament Passes Laws That Set Regulatory Framework For B...](https://www.forbes.com/sites/rachelwolfson/2018/07/05/maltese-parliament-passes-laws-that-set-regulatory-framework-for-blockchain-cryptocurrency-and-dlt/)

    The Maltese Parliament has passed 3 bills into law, establishing the first regulatory framework for blockchain, cryptocurrency and DLT Technology (Distributed Ledger Technology). This makes Malta the first country in the world to provide regulations for operators in the block...

    
    

    [https://cointelegraph.com/news/bittrex-launches-malta-based-international-trading-platform-minus-us-customers](https://cointelegraph.com/news/bittrex-launches-malta-based-international-trading-platform-minus-us-customers)

    [Bittrex Launches Malta-Based ‘International’ Trading Platform,...](https://cointelegraph.com/news/bittrex-launches-malta-based-international-trading-platform-minus-us-customers)

    U.S. crypto exchange Bittrex launches international trading platform on the base of Maltese branch, which will reportedly increase the speed of listing.

    
    [https://bitshouts.com/binance-bnb-malta-exchange/](https://bitshouts.com/binance-bnb-malta-exchange/)

    [How And Why Binance Is Moving - Is Malta To Become A Crypto Hub? -...](https://bitshouts.com/binance-bnb-malta-exchange/)

    Binance moving to Malta is an interesting step for both the exchange and the country - but what exactly is the motivation behind the move?

    
- 🌍-lawyers-eu

        Crypto⧉Finance

    🌍-lawyers-eu

    1 messages

    

    [https://twitter.com/bmann/status/1158436750382886913?s=12](https://twitter.com/bmann/status/1158436750382886913?s=12)

    
    [Boris Mann (@bmann)](https://twitter.com/bmann)

    Anyone have recommendations for German lawyers specialized in tech startups? Apparently, stock options for employees is expensive / complicated in Germany, so @quidliprotocol is looking at making this easier. Ping @ahn_going from Quidli with your suggestions / intros ple...

    
    Twitter

- 🌍-germany

        Crypto⧉Finance

    🌍-germany

    2 messages

    

    [https://www.planetcompliance.com/2019/04/20/a-new-direction-for-german-token-regulation/](https://www.planetcompliance.com/2019/04/20/a-new-direction-for-german-token-regulation/) (edited)

    [A new direction for German Token Regulation? - Planet Compliance](https://www.planetcompliance.com/2019/04/20/a-new-direction-for-german-token-regulation/)

    Germany cannot be considered the most welcoming jurisdiction in terms of token regulation. A recent report might spell a change of direction though.

    
    [https://www.planetcompliance.com/2019/08/16/ico-sto-ito-the-requirements-if-you-launch-in-germany/](https://www.planetcompliance.com/2019/08/16/ico-sto-ito-the-requirements-if-you-launch-in-germany/)

    [ICO, STO, ITO – The Requirements If You Launch In Germany - Plan...](https://www.planetcompliance.com/2019/08/16/ico-sto-ito-the-requirements-if-you-launch-in-germany/)

    A comprehensive list of the requirements you need to keep in mind if you want to launch an ICO, STO or ITO and the information you need to provide.

    
- 🌍-gdpr

        Crypto⧉Finance

    🌍-gdpr

    3 messages

    
    >Guide to GDPR compliance recommended by TechGDPR, a Berlin-based GDPR consultancy. Available in many languages, for free in exchange for... a bunch of personal data

    
    ht\ @JuanSC

    [https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en](https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en)

    [Handbook on European data protection law : 2018 edition.](https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en)

    The rapid development of information technology has exacerbated the need for robust personal data protection, the right to which is safeguarded by both European Union (EU) and Council of Europe (CoE) instruments. Safeguarding this important right entails new and signifi cant ...

    [https://publications.europa.eu/portal2012-services/thumbnail/cellar/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1.0001.01/DOC_2](https://publications.europa.eu/portal2012-services/thumbnail/cellar/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1.0001.01/DOC_2)

    

    [https://threadreaderapp.com/thread/1044543538610999296.html](https://threadreaderapp.com/thread/1044543538610999296.html)

    [Thread by @finck_m: "The French data protection authority has issu...](https://threadreaderapp.com/thread/1044543538610999296.html)

    Thread by @finck_m: "The French data protection authority has issued the first formal guidance on the relationship between and the in the . n points. Here is the full text: cnil.fr/sites/default/… 1. The @CNIL make […]" #blockchain #GDPR #EU #developers

    
    

    Guide to GDPR compliance recommended by TechGDPR, a Berlin-based GDPR consultancy. Available in many languages, for free in exchange for... a bunch of personal data :D [https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en](https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en)

    [Handbook on European data protection law : 2018 edition.](https://publications.europa.eu/en/publication-detail/-/publication/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1/language-en)

    The rapid development of information technology has exacerbated the need for robust personal data protection, the right to which is safeguarded by both European Union (EU) and Council of Europe (CoE) instruments. Safeguarding this important right entails new and signifi cant ...

    [https://publications.europa.eu/portal2012-services/thumbnail/cellar/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1.0001.01/DOC_2](https://publications.europa.eu/portal2012-services/thumbnail/cellar/5b0cfa83-63f3-11e8-ab9c-01aa75ed71a1.0001.01/DOC_2)

- 🌍-france

        Crypto⧉Finance

    🌍-france

    5 messages

    

    [https://www.coindesk.com/france-gives-go-ahead-for-blockchain-trading-of-unlisted-securities/](https://www.coindesk.com/france-gives-go-ahead-for-blockchain-trading-of-unlisted-securities/)

    [France Approves Blockchain Trading of Unlisted Securities - CoinDesk](https://www.coindesk.com/france-gives-go-ahead-for-blockchain-trading-of-unlisted-securities/)

    The French government has given the official nod for trading unlisted securities using blockchain technology.

    
    

    [https://www.bis.org/review/r181213a.pdf](https://www.bis.org/review/r181213a.pdf)

    

    [https://www.reuters.com/article/us-crypto-currencies-regulation-france/france-to-approve-first-crypto-issuers-as-new-rules-loom-idUSKCN1UB18P](https://www.reuters.com/article/us-crypto-currencies-regulation-france/france-to-approve-first-crypto-issuers-as-new-rules-loom-idUSKCN1UB18P)

    [France to approve first crypto-issuers as new rules loom](https://www.reuters.com/article/us-crypto-currencies-regulation-france/france-to-approve-first-crypto-issuers-as-new-rules-loom-idUSKCN1UB18P)

    France's financial watchdog is poised to approve a first tranche of cryptoc...

    [https://s4.reutersmedia.net/resources/r/?m=02&d=20190716&t=2&i=1408581887&w=1200&r=LYNXNPEF6F0TS](https://s4.reutersmedia.net/resources/r/?m=02&d=20190716&t=2&i=1408581887&w=1200&r=LYNXNPEF6F0TS)

    

    [https://www.amf-france.org/en_US/Reglementation/Dossiers-thematiques/Fintech/Vers-un-nouveau-regime-pour-les-crypto-actifs-en-France](https://www.amf-france.org/en_US/Reglementation/Dossiers-thematiques/Fintech/Vers-un-nouveau-regime-pour-les-crypto-actifs-en-France)

    [Towards a new regime for crypto-assets in France](https://www.amf-france.org/en_US/Reglementation/Dossiers-thematiques/Fintech/Vers-un-nouveau-regime-pour-les-crypto-actifs-en-France)

    The PACTE draft Bill (Action Plan for Business Growth and Transformation) was adopted at its final reading in the French National Assembly on 11 April 2019. Once enacted, the law will establish a framework for fundraising via the issuance of virtual tokens (Initial Coin Offer...

    
    

    [https://twitter.com/theblock__/status/1172137874281717760?s=12](https://twitter.com/theblock__/status/1172137874281717760?s=12)

    
    [The Block (@TheBlock__)](https://twitter.com/TheBlock__)

    BREAKING: France will not tax crypto-to-crypto trades; will tax gains converted into “traditional” currency [https://t.co/28pZxjJzjd](https://t.co/28pZxjJzjd)

    Retweets

    386

    Likes

    1086

    
    Twitter

- 🌍-estonia-regulation

        Crypto⧉Finance

    🌍-estonia-regulation

    15 messages

    

    [https://e-resident.me/how-to-obtain-crypto-licenses-in-estonia/](https://e-resident.me/how-to-obtain-crypto-licenses-in-estonia/)

    [How to obtain Crypto Licenses in Estonia](https://e-resident.me/how-to-obtain-crypto-licenses-in-estonia/)

    
    [https://www2.politsei.ee/en/organisatsioon/rahapesu-andmeburoo/fius-advisory-guidelines/](https://www2.politsei.ee/en/organisatsioon/rahapesu-andmeburoo/fius-advisory-guidelines/)

    [https://www2.politsei.ee/en/teenused/majandustegevuse-luba.dot](https://www2.politsei.ee/en/teenused/majandustegevuse-luba.dot)

    [https://www2.politsei.ee/en/organisatsioon/rahapesu-andmeburoo/estonian-legislation/](https://www2.politsei.ee/en/organisatsioon/rahapesu-andmeburoo/estonian-legislation/)

    [https://incorporate.ee/insights/faq](https://incorporate.ee/insights/faq)

    [FAQ | Incorporate in Estonia](https://incorporate.ee/insights/faq)

    Usually our clients choose to establish a private limited liability company (in Estonian "osaühing" or "OÜ"). OÜ is the most widely used form - the easiest to set up and the cheapest to maintain. Depending on your business requirements, we can incorporate also other forms ...

    [https://prifinance.com/en/cryptocurrency-license/estonia/](https://prifinance.com/en/cryptocurrency-license/estonia/)

    [https://news.bitcoin.com/european-central-bank-criticizes-estonian-national-cryptocurrency-plans/](https://news.bitcoin.com/european-central-bank-criticizes-estonian-national-cryptocurrency-plans/)

    [European Central Bank Criticizes Estonian National Cryptocurrency ...](https://news.bitcoin.com/european-central-bank-criticizes-estonian-national-cryptocurrency-plans/)

    The president of the European Central Bank has criticized the Estonian government’s plan to launch a national cryptocurrency. The statements have implied that the European Union (EU) states will not be permitted to launch state-issued cryptocurrencies that compete with the ...

    
    [https://en.wikipedia.org/wiki/E-Residency_of_Estonia](https://en.wikipedia.org/wiki/E-Residency_of_Estonia)

    [E-Residency of Estonia](https://en.wikipedia.org/wiki/E-Residency_of_Estonia)

    e-Residency of Estonia (also called virtual residency or E-residency) is a program launched by Estonia on 1 December 2014. The program allows non-Estonians access to Estonian services such as company formation, banking, payment processing, and taxation. The program gives the ...

    
    [https://e-resident.gov.ee/](https://e-resident.gov.ee/)

    [What is e-Residency | How to Start an EU Company Online](https://e-resident.gov.ee/)

    E-Residency, powered by the Republic of Estonia, enables entrepreneurs to start a trusted location-independent EU company online.

    [https://e-resident.gov.ee/start-a-company/](https://e-resident.gov.ee/start-a-company/)

    [How to Start a Company in Estonia & EU | e-Residency](https://e-resident.gov.ee/start-a-company/)

    E-residents can form, start & incorporate an EU-based company 100% online, from anywhere, to go to market and access customers in the EU Single Market.

    [https://apply.gov.ee/](https://apply.gov.ee/)

    [Application for e-Residency](https://apply.gov.ee/)

    e-Residency offers to every world citizen a government-issued digital identity and the opportunity to run a trusted company online, unleashing the world’s entrepreneurial potential. The Republic of Estonia is the first country to offer e-Residency — a transnational digita...

    
    

    [https://yourcompanyinestonia.com/whats-the-e-residency-program-of-estonia/](https://yourcompanyinestonia.com/whats-the-e-residency-program-of-estonia/)

    [What's The e-Residency Program Of Estonia](https://yourcompanyinestonia.com/whats-the-e-residency-program-of-estonia/)

    How you can benefit from the e-Residency as an entrepreneur.

    
    "Estonia is one of the lower cost licensing jurisdictions. You should expect to pay 10,000 to 20,000 euros for a cryptocurrency exchange license from Estonia."

    

    [https://cryptodigestnews.com/the-beginners-guide-to-opening-a-crypto-business-in-estonia-e2ed55cb0fc4](https://cryptodigestnews.com/the-beginners-guide-to-opening-a-crypto-business-in-estonia-e2ed55cb0fc4)

    [The Beginner’s Guide to Opening a Crypto Business in Estonia](https://cryptodigestnews.com/the-beginners-guide-to-opening-a-crypto-business-in-estonia-e2ed55cb0fc4)

    A fresh study has revealed that, as of April 2019, Estonia houses some 700 foreign businesses related to blockchain and cryptocurrencies…

    
    

    K[https://news.bitcoin.com/czech-bank-launches-cryptocurrency-friendly-services/](https://news.bitcoin.com/czech-bank-launches-cryptocurrency-friendly-services/) [https://news.bitcoin.com/estonia-grants-licenses-for-wallet-and-exchange-services-to-coin-metro/](https://news.bitcoin.com/estonia-grants-licenses-for-wallet-and-exchange-services-to-coin-metro/) [https://news.bitcoin.com/the-daily-poloniex-drops-8-coins-new-exchange-licensed-in-estonia/](https://news.bitcoin.com/the-daily-poloniex-drops-8-coins-new-exchange-licensed-in-estonia/) [https://news.bitcoin.com/estonia-grants-license-to-crypto-trading-software-provider-ibinex/](https://news.bitcoin.com/estonia-grants-license-to-crypto-trading-software-provider-ibinex/) [https://news.bitcoin.com/the-daily-us-museum-to-accept-bitcoin-estonia-grants-license-to-b2bx-exchange/](https://news.bitcoin.com/the-daily-us-museum-to-accept-bitcoin-estonia-grants-license-to-b2bx-exchange/) [https://news.bitcoin.com/estonia-issues-over-900-licenses-to-cryptocurrency-businesses/](https://news.bitcoin.com/estonia-issues-over-900-licenses-to-cryptocurrency-businesses/)

    [Czech Bank Launches Cryptocurrency-Friendly Services - Bitcoin News](https://news.bitcoin.com/czech-bank-launches-cryptocurrency-friendly-services/)

    A commercial bank in the Czech Republic is now offering online services that combine traditional financial features with those for cryptocurrency users. A Czech bank is now offering a package of online services that combines traditional financial operations with features orie...

    
    [Estonia Grants Licenses for Wallet and Exchange Services to Coinme...](https://news.bitcoin.com/estonia-grants-licenses-for-wallet-and-exchange-services-to-coin-metro/)

    It isn’t just Malta that is attracting crypto business these days, other small and fast moving countries on the European continent are joining the action. The latest example comes from Estonia, which recently granted licenses for offering wallet and exchange services to the...

    
    [The Daily: Poloniex Drops 8 Coins, New Exchange Licensed in Estoni...](https://news.bitcoin.com/the-daily-poloniex-drops-8-coins-new-exchange-licensed-in-estonia/)

    Cryptocurrency trading platform Poloniex has delisted 8 coins with low liquidity, while another US-based exchange, Gemini, is adding litecoin to its offerings. Also in The Daily, Ironx has been licensed to offer exchange services from Estonia, and Bitmain’s Antpool will spo...

    
    [Estonia Grants License to Crypto Trading Software Provider Ibinex ...](https://news.bitcoin.com/estonia-grants-license-to-crypto-trading-software-provider-ibinex/)

    Ibinex, a company that specializes in white-label solutions for cryptocurrency exchanges, has obtained a license to operate in Estonia. Besides exchange services, it also plans to offer both hot and cold wallets within the regulatory framework of the tech-savvy, crypto-friend...

    
    [The Daily: US Museum to Accept Bitcoin, Estonia Grants License to ...](https://news.bitcoin.com/the-daily-us-museum-to-accept-bitcoin-estonia-grants-license-to-b2bx-exchange/)

    In today’s edition of The Daily we report about the latest American museum to open up to cryptocurrency payments as well as new lawsuits over SIM hijacking. Additionally, the government of Estonia has granted a license to B2BX exchange which will allow to it to attract trad...

    ![https://news.bitcoin.com/wp-content/uploads/2018/11/Great-Lakes-Science-Center.jpg](https://news.bitcoin.com/wp-content/uploads/2018/11/Great-Lakes-Science-Center.jpg)