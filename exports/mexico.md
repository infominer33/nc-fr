# mexico

[https://www.coindesk.com/experts-say-mexicos-regulations-raise-the-bar-too-high-for-crypto-entrepreneurs](https://www.coindesk.com/experts-say-mexicos-regulations-raise-the-bar-too-high-for-crypto-entrepreneurs)

[Experts Say Mexico's Regulations Raise the Bar 'Too High' for Cryp...](https://www.coindesk.com/experts-say-mexicos-regulations-raise-the-bar-too-high-for-crypto-entrepreneurs)

New laws are strangling crypto startups before they can begin to trade.

![https://static.coindesk.com/wp-content/uploads/2019/08/mexico-fintech-regulation-bitcoin.jpg](https://static.coindesk.com/wp-content/uploads/2019/08/mexico-fintech-regulation-bitcoin.jpg)