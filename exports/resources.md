# 📑-resources

Crypto⧉Finance

📑-resources

5 messages

[http://boards.4channel.org/biz/](http://boards.4channel.org/biz/)

[/biz/ - Business & Finance - 4chan](http://boards.4channel.org/biz/)

"/biz/ - Business & Finance" is 4chan's imageboard for the discussion of business and finance, and cryptocurrencies such as Bitcoin and Dogecoin.

[https://github.com/imbaniac/awesome-blockchain](https://github.com/imbaniac/awesome-blockchain)

[imbaniac/awesome-blockchain](https://github.com/imbaniac/awesome-blockchain)

Curated list of blockchain services and exchanges



- imbaniac/awesome-blockchain

[https://medium.com/@StrongWriters](https://medium.com/@StrongWriters)

[$trong – Medium](https://medium.com/@StrongWriters)

Read writing from $trong on Medium. Cryptocurrency Writer. Every day, $trong and thousands of other voices read, write, and share important stories on Medium.

[https://flagtheory.com/archives/](https://flagtheory.com/archives/)

[Flag Theory Blog on Offshore Internationalization - Flag Theory](https://flagtheory.com/archives/)

Find actionable information on second passports, tax residency, offshore companies, offshore bank accounts, investments, cryptocurrencies and much more..

[http://premieroffshore.com/](http://premieroffshore.com/) (edited)

[Premier Offshore Company Services - Research, News & Offshore Comp...](http://premieroffshore.com/)

Research, News & Offshore Company Formation Services

- 📑-literature

        Crypto⧉Finance

    📑-literature

    4 messages

    

    [https://mises.org/library/ethics-money-production](https://mises.org/library/ethics-money-production)

    [The Ethics of Money Production | Jörg Guido Hülsmann](https://mises.org/library/ethics-money-production)

    This pioneering work by Jörg Guido Hülsmann, professor of economics at the University of Angers in France and the author of Mises: The Last Knight of Liberalism, is the first full study of a critically important issue today: the ethics of money production.

    
    [http://www.nber.org/papers/w24717](http://www.nber.org/papers/w24717) -The Economic Limits of Bitcoin and the Blockchain v

    [https://twitter.com/hal2001/status/1029814266247958529](https://twitter.com/hal2001/status/1029814266247958529)

    
    [Hart Lambur (@hal2001)](https://twitter.com/hal2001)

    1/ Important paper published by my friend Prof Tonetti (@ChrisTonetti) and his colleague Prof Jones @StanfordGSB. They have rigorously studied the "economics of data" to show that there are large social gains from letting users own and sell their own data. Some key takeawa...

    Likes

    191

    
    Twitter

    

    [https://github.com/xasos/awesome-decentralized-papers](https://github.com/xasos/awesome-decentralized-papers)

    [xasos/awesome-decentralized-papers](https://github.com/xasos/awesome-decentralized-papers)

    Influential papers in decentralized systems (cryptocurrencies, contracts, consensus, etc.) - xasos/awesome-decentralized-papers

- 📑-legal

        Crypto⧉Finance

    📑-legal

    otherwise organized by region

    7 messages

    

    [Lewis Cohen (@NYcryptolawyer)](https://twitter.com/NYcryptolawyer)

    Co-founder at DLx Law. Living on reds, vitamin C and blockchain. Speaks only for self. A tweet does not legal advice make. Ever. Be safe out there.

    Tweets

    1310

    Followers

    2378

    
    
    Twitter

    
    [Nelson M. Rosario (@NelsonMRosario)](https://twitter.com/NelsonMRosario)

    @Nogoodtwts @stephendpalley Lots @valkenburgh @PeterTLuce @prestonjbyrne @jasonsomensatto @propelforward @angela_walch @msantoriESQ @awrigh01 @katherineykwu @dcsilver @FJasonSeibert @SamirPatelLaw @hackylawyER @San_person @ZachSmolinski and I'm sure I'm missing some more

    
    Twitter

    

    [https://twitter.com/notsofast/lists/crypto-law](https://twitter.com/notsofast/lists/crypto-law)

    [@notsofast/$crypto law on Twitter](https://twitter.com/notsofast/lists/crypto-law)

    From breaking news and entertainment to sports and politics, get the full story with all the live commentary.

    
    Twitter

    

    [https://twitter.com/cryptolawrev/status/1159577442832572416?s=12](https://twitter.com/cryptolawrev/status/1159577442832572416?s=12)

    
    [Crypto Law Review (@CryptoLawRev)](https://twitter.com/CryptoLawRev)

    Pleasure help us push past 400 followers & spread the news on Twitter. We’re buzzing on @Medium but want to bring the action to #CryptoTwitter too. Let folks know about @CryptoLawRev by RTing your favorite CLR piece
    
    Twitter

    

    [https://podcasts.apple.com/us/podcast/internet-history-podcast/id829119009?i=1000429578525](https://podcasts.apple.com/us/podcast/internet-history-podcast/id829119009?i=1000429578525)

    [‎Internet History Podcast: 189. A Legal History of the Web Era W...](https://podcasts.apple.com/us/podcast/internet-history-podcast/id829119009?i=1000429578525)

    ‎Show Internet History Podcast, Ep 189. A Legal History of the Web Era With Richard Chapo - Feb 10, 2019

    
    

    [https://techcrunch.com/2019/01/10/how-to-get-your-moneys-worth-from-your-startup-lawyer/](https://techcrunch.com/2019/01/10/how-to-get-your-moneys-worth-from-your-startup-lawyer/)

    [How to get your money’s worth from your startup lawyer – TechC...](https://techcrunch.com/2019/01/10/how-to-get-your-moneys-worth-from-your-startup-lawyer/)

    You will never know as much as your lawyers do about the legal services they provide to you. It is a classic asymmetry of information, where the party that knows less gets the worse deal. In this case, that is you, the startup founder. As an attorney and a co-founder of a ven...

    
    2. Educate yourself and then let your lawyer know you understand the basics. Today there are numerous high-quality, free templates and other resources available for the most common legal tasks facing startups (see links below). If you need new Terms of Service, for example, carefully read one of the many templates available, insert comments where you see fit, and pass on this marvelous example of intellectual aspiration to your attorney for final drafting. This will let the attorney know you have a basic understanding of what the assignment entails and at the very least reduce perceived asymmetries of information, improving your relative bargaining position.

    

    [https://openlaw.io/](https://openlaw.io/)

    [OpenLaw — A free legal repository](https://openlaw.io/)

    We are home to a passionate group of people, technologists, and dreamers committed to rebuilding the legal industry.

    
- 📑-how-to-earn-crypto

        Crypto⧉Finance

    📑-how-to-earn-crypto

    8 messages

    

    [https://medium.com/@OpenNode/opennode-affiliate-program-262ad1d42ffc](https://medium.com/@OpenNode/opennode-affiliate-program-262ad1d42ffc) [https://news.bitcoin.com/new-freelancing-platform-supports-12-different-cryptocurrencies/](https://news.bitcoin.com/new-freelancing-platform-supports-12-different-cryptocurrencies/) [https://twitter.com/andreahamel2/status/1110395712863461377?s=12](https://twitter.com/andreahamel2/status/1110395712863461377?s=12) [https://www.reddit.com/r/Jobs4Bitcoins/comments/bfd9vr/from_broke_to_bitcoin_effective_tactics_for/?utm_source=share&utm_medium=ios_app](https://www.reddit.com/r/Jobs4Bitcoins/comments/bfd9vr/from_broke_to_bitcoin_effective_tactics_for/?utm_source=share&utm_medium=ios_app)

    [OpenNode Affiliate Program](https://medium.com/@OpenNode/opennode-affiliate-program-262ad1d42ffc)

    Earn bitcoin while you sleep

    
    
    [New Freelancing Platform Supports 12 Different Cryptocurrencies - ...](https://news.bitcoin.com/new-freelancing-platform-supports-12-different-cryptocurrencies/)

    There’s a new freelance platform called Freelance for Coins that provides users with the ability to publish offers and bids for cryptocurrencies.

    
    [Twitter / Account Suspended](https://twitter.com/andreahamel2/status/1110395712863461377?s=12)

    From breaking news and entertainment to sports and politics, get the full story with all the live commentary.

    
    Twitter

    [r/Jobs4Bitcoins - From Broke To Bitcoin: Effective Tactics For Ear...](https://www.reddit.com/r/Jobs4Bitcoins/comments/bfd9vr/from_broke_to_bitcoin_effective_tactics_for/?utm_source=share&utm_medium=ios_app)

    4 votes and 0 comments so far on Reddit

    
    [https://gist.github.com/pedrouid/e8f6d74e4ca36128b3220c49612bf0e1](https://gist.github.com/pedrouid/e8f6d74e4ca36128b3220c49612bf0e1) [https://cryptojobslist.com/](https://cryptojobslist.com/) [https://ethlance.com/](https://ethlance.com/) [https://www.xbtfreelancer.com/](https://www.xbtfreelancer.com/)

    [WalletConnect Instant](https://gist.github.com/pedrouid/e8f6d74e4ca36128b3220c49612bf0e1)

    
    WalletConnect Instant

    
    . GitHub Gist: instantly share code, notes, and snippets.

    
    [Ethlance - hire or work for Ether cryptocurrency](https://ethlance.com/)

    
    [Work for bitcoin doing Freelance Jobs & Projects](https://www.xbtfreelancer.com/)

    The bitcoin freelancers site, where employers pay for projects in bitcoin and freelancers get paid in bitcoin doing freelance jobs. Work for bitcoin from home, be part of the emerging cryptocurrency industry.

    
    [https://coinality.com/](https://coinality.com/) [https://www.cryptogrind.com/](https://www.cryptogrind.com/) [http://www.bitgigs.com/](http://www.bitgigs.com/) [https://cryptojobslist.com/](https://cryptojobslist.com/)

    [Home](https://coinality.com/)

    [https://bitpatron.co/bitpatron.id.blockstack](https://bitpatron.co/bitpatron.id.blockstack) [https://blog.btcpayserver.org/payment-requests/](https://blog.btcpayserver.org/payment-requests/) [https://twitter.com/bitbacker_io/status/1102069498830602240?s=12](https://twitter.com/bitbacker_io/status/1102069498830602240?s=12)

    [Introducing Pay Request - Get paid in BTC by sharing a link • BT...](https://blog.btcpayserver.org/payment-requests/)

    I regularly pay freelancers with bitcoin. When we agree on the price in fiat, due to volatility, the numbers never add up. I have to pay again, or they have to refund me. If we agree on the amount a few days in advance, someone is always at financial or time loss. Making sure...

    
    
    [BitBacker (@BitBacker_io)](https://twitter.com/BitBacker_io)

    Support you favorite creators on [https://t.co/moaONrhmtB](https://t.co/moaONrhmtB) in crypto rather than with fiat
    
    Twitter

    [https://www.earncrypto.com/earn.php](https://www.earncrypto.com/earn.php) [https://jobs.dcg.co/](https://jobs.dcg.co/) [https://thebitcoinnews.com/blockchain-and-crypto-jobs-are-growing-in-asia/](https://thebitcoinnews.com/blockchain-and-crypto-jobs-are-growing-in-asia/) [https://t.me/CryptoJobs](https://t.me/CryptoJobs) (edited)

    [Earn With Earncrypto](https://www.earncrypto.com/earn.php)

    [Digital Currency Group Job Board](https://jobs.dcg.co/)

    Search job openings across the Digital Currency Group network.

    [https://cdn.filepicker.io/api/file/YALGXIM6QnyRbmoIV7ad](https://cdn.filepicker.io/api/file/YALGXIM6QnyRbmoIV7ad)

    [Blockchain and Crypto Jobs are Growing In Asia - The Bitcoin News](https://thebitcoinnews.com/blockchain-and-crypto-jobs-are-growing-in-asia/)

    Despite a downtrending crypto market, CNBC reports that blockchain and cryptocurrency jobs are growing in Asia. Recruitment firm Robert Walters told CNBC that the company has seen a 50 percent increase in the number of roles related to blockchain or cryptocurrencies in Asia s...

    
    [Crypto Jobs](https://t.me/CryptoJobs)

    This is a part of @Crypto, Want to publish a job? Use @ListMeBot For more information please read @CryptoFAQs

    
    [https://medium.com/cent-official/access-cent-from-anywhere-3dbd18b357f](https://medium.com/cent-official/access-cent-from-anywhere-3dbd18b357f) [https://twitter.com/naomibrockwell/status/1074792990630133760](https://twitter.com/naomibrockwell/status/1074792990630133760) [https://twitter.com/naomibrockwell/status/1074792990630133760](https://twitter.com/naomibrockwell/status/1074792990630133760) (edited)

    [Mobile? Desktop? You can access Cent from anywhere](https://medium.com/cent-official/access-cent-from-anywhere-3dbd18b357f)

    Your one-stop guide to the best Web 3.0 browser options available today

    
    
    [Naomi ₿rockwell (@naomibrockwell)](https://twitter.com/naomibrockwell)

    @RealSexyCyborg Try out @BitBacker_io
    
    Twitter

    [http://earn.com](http://earn.com/) [https://satoshibox.com/how-it-works](https://satoshibox.com/how-it-works) [https://jobs.lever.co/steemit](https://jobs.lever.co/steemit)

    [Earn: Earn Bitcoin by Answering Messages & Completing Tasks](http://earn.com/)

    Set up an Earn.com profile to receive paid messages from people outside your network. Keep the bitcoin you earn, or donate it to charity.

    
    [https://twitter.com/theonevortex/status/1075864287434833920](https://twitter.com/theonevortex/status/1075864287434833920) [https://twitter.com/vandrewattycpa/status/1079471260978040833](https://twitter.com/vandrewattycpa/status/1079471260978040833) [https://twitter.com/djbooth007/status/1081067021951918080](https://twitter.com/djbooth007/status/1081067021951918080)

    
    [Vortex (@theonevortex)](https://twitter.com/theonevortex)

    I've loved watching people switch from @Patreon to using #bitcoin with @tallyco_in lately
    
    Twitter

    
    [Jeff Vandrew Jr (@vandrewattycpa)](https://twitter.com/vandrewattycpa)

    Today I released LibrePatron, an alternative to Partreon backed by @BtcpayServer. Most Patreon alternatives don't implement the full Patreon feature set. This seeks to change that. Sample site (alpha not mobile responsive, mobile coming soon
    Retweets

    234

    Likes

    614

    
    Twitter

    
    [DJ Booth (@djbooth007)](https://twitter.com/djbooth007)

    @giacomozucco Once subscriptions are built in to [https://t.co/9wK47VOA7p](https://t.co/9wK47VOA7p) I’ll turn my attention to making long-form articles easy to create and set public or behind paywall for subscribers.

    
    Twitter

- 📑-financial-media

        Crypto⧉Finance

    📑-financial-media

    3 messages

    

    [https://www.youtube.com/watch?v=1JVjoh6xzIA](https://www.youtube.com/watch?v=1JVjoh6xzIA)

    [Amazon Prime Video](https://www.youtube.com/user/amazonstudios)

    [This Giant Beast That is the Global Economy - Official Trailer | P...](https://www.youtube.com/watch?v=1JVjoh6xzIA)

    This globe-spanning docuseries brings the smart, stylized storytelling of Adam McKay’s The Big Short to a quirky and compelling exploration of the global eco...

    
    [https://www.youtube.com/watch?v=vgqG3ITMv1Q](https://www.youtube.com/watch?v=vgqG3ITMv1Q)

    [Paramount Pictures](https://www.youtube.com/user/Paramount)

    [The Big Short Trailer (2015) ‐ Paramount Pictures](https://www.youtube.com/watch?v=vgqG3ITMv1Q)

    From the outrageous mind of director Adam Mckay comes THE BIG SHORT. Starring Christian Bale, Steve Carell, Ryan Gosling and Brad Pitt, in theaters Christmas...

    
    

    [https://www.youtube.com/watch?v=R8RXPRfhmA8](https://www.youtube.com/watch?v=R8RXPRfhmA8)

    [Sony Pictures India](https://www.youtube.com/user/sonypicturesindia)

    [MONEYBALL - Official Trailer](https://www.youtube.com/watch?v=R8RXPRfhmA8)

    Join the official page [https://www.facebook.com/sonypicturesofindia](https://www.facebook.com/sonypicturesofindia) Based on a true story, Moneyball is a movie for anybody who has ever dreamed of taking on...

    
- 📑-banks

        Crypto⧉Finance

    📑-banks

    2 messages

    

    [https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/)

    [Which Banks Accept Bitcoin? Get The List | Banks.com](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/)

    The recent rash of banks announcing that they would stop accepting credit card transactions for bitcoin purchases has created a stir in the altcoin sphere.

    
    [https://bitcointalk.org/index.php?topic=2674704.0](https://bitcointalk.org/index.php?topic=2674704.0)

    [Where to open a bank account for Crypto business](https://bitcointalk.org/index.php?topic=2674704.0)

    Where to open a bank account for Crypto business

- 📑-advertising

        Crypto⧉Finance

    📑-advertising

    12 messages

    

    [https://cointelegraph.com/news/big-tech-are-banning-crypto-and-ico-ads-is-there-a-reason-to-panic](https://cointelegraph.com/news/big-tech-are-banning-crypto-and-ico-ads-is-there-a-reason-to-panic) (edited)

    [Big Tech Are Banning Crypto And ICO Ads - Is There A Reason to Panic?](https://cointelegraph.com/news/big-tech-are-banning-crypto-and-ico-ads-is-there-a-reason-to-panic)

    Major social media platforms have kicked out crypto and ICO related ads. Google will introduce similar actions in the nearest future. How does it actually affect the industry?

    
    [https://blocksdecoded.com/crypto-advertising-restrictions/](https://blocksdecoded.com/crypto-advertising-restrictions/)

    Adverts for cryptocurrency exchanges are sometimes allowed (and are not restricted in the same way adverts for trading are), but there are further regulations. Exchanges must get licensed by their local authority, must meet all local laws and industry standards, and use an account verified by Google. Only once meeting these requirements, can you apply for a Google AdSense certification. If you trade in more than one country, you’ll need a certification for each country you wish to advertise in, providing its a country approved by Google.

    (edited)

    [Can You Use Online Advertising for Cryptocurrency Sites? - Blocks ...](https://blocksdecoded.com/crypto-advertising-restrictions/)

    Google AdSense limits cryptocurrency adverts, so here's everything you need to know.

    
    [https://www.denverlawreview.org/dlr-online-article/2019/4/6/crypto-concerns-initial-coin-offerings-and-the-us-securities.html](https://www.denverlawreview.org/dlr-online-article/2019/4/6/crypto-concerns-initial-coin-offerings-and-the-us-securities.html)

    An ICO issuer using Rule 506(b) cannot use “general solicitation,”[167] such as advertising, to market its tokens.[168] This prohibition on general solicitation may rule out Rule 506(b) for ICO issuers because it limits the ability to market the ICO via the Internet to a large number of potential investors. Furthermore, if non-accredited investors are participating in the offering, the ICO issuer must provide them with disclosure similar to the type of information provided in registered public offerings, an unheard-of level of disclosure for an ICO.[169] Tokens purchased pursuant to Rule 506(b) would be exempt from state registration requirements, and investors who purchase tokens in a Rule 506(b) offering would receive restricted securities, meaning they are not freely tradable unless they are either registered or comply with a valid resale exemption such as Rule 144.[170]

    (edited)

    [Crypto-Concerns: Initial Coin Offerings and the U.S. Securities La...](https://www.denverlawreview.org/dlr-online-article/2019/4/6/crypto-concerns-initial-coin-offerings-and-the-us-securities.html)

    { PDF } Kenneth S. Witt [*] & Mark A. Staines [**] Once upon a time, cryptocurrency trading was thought by many to lie outside the scope of U.S. securities law. But since mid-2017, the SEC has increasingly asserted its regulatory authority over cryptocurrency trad...

    The fairly new Rule 506(c) of Regulation D complements Rule 506(b). Unlike Rule 506(b), Rule 506(c) would allow ICO issuers to use general solicitation, including advertising, to market the ICO in the U.S.[171] However, all purchasers in the offering must be accredited investors, and the ICO issuer using Rule 506(c) must take reasonable steps to verify the accredited status of the investors purchasing the tokens[172] or risk enforcement action by the SEC.[173]

    [https://www.sec.gov/info/smallbus/secg/general-solicitation-small-entity-compliance-guide.htm](https://www.sec.gov/info/smallbus/secg/general-solicitation-small-entity-compliance-guide.htm)

    Enacted in 2012, the Jumpstart Our Business Startups Act, or JOBS Act, is intended, among other things, to reduce barriers to capital formation, particularly for smaller companies. The JOBS Act requires the SEC to adopt rules amending existing exemptions from registration under the Securities Act of 1933 and creating new exemptions that permit issuers of securities to raise capital without SEC registration. On July 10, 2013, the SEC adopted amendments to Rule 506 of Regulation D and Rule 144A under the Securities Act to implement the requirements of Section 201(a) of the JOBS Act. The amendments are effective on September 23, 2013.

    

    [https://www.facebook.com/business/news/updating-our-ad-policies-for-financial-services-and-products](https://www.facebook.com/business/news/updating-our-ad-policies-for-financial-services-and-products) (edited)

    [Updating Our Ad Policies for Financial Services and Products](https://www.facebook.com/business/news/updating-our-ad-policies-for-financial-services-and-products)

    We're updating our ad policies for financial services and products to prevent misleading advertising and predatory behavior on our platforms.

    [https://www.facebook.com/business/f/GFCFlgNQ2Wn2vbgHAAAAAADCzalJbj0JAAAD/](https://www.facebook.com/business/f/GFCFlgNQ2Wn2vbgHAAAAAADCzalJbj0JAAAD/)

    [https://www.facebook.com/policies/ads/restricted_content/cryptocurrency_products_and_services?ref=FBB_UpdatingOurAdPolicies#](https://www.facebook.com/policies/ads/restricted_content/cryptocurrency_products_and_services?ref=FBB_UpdatingOurAdPolicies#)

    
    [https://www.facebook.com/business/help/438252513416690](https://www.facebook.com/business/help/438252513416690)

    Learn about our policies on ads related to cryptocurrency products and services.

    [https://www.facebook.com/help/contact/532535307141067](https://www.facebook.com/help/contact/532535307141067)

    [Log into Facebook | Facebook](https://www.facebook.com/help/contact/532535307141067)

    Log into Facebook to start sharing and connecting with your friends, family, and people you know.

    [https://www.compliancebuilding.com/2019/04/03/pre-existing-substantive-relationships-and-general-solicitation/](https://www.compliancebuilding.com/2019/04/03/pre-existing-substantive-relationships-and-general-solicitation/)

    The fund could have engaged in general solicitation if it checked the 506(c) box on its Form D. But it checked the 506(b) box. Of course, with 506(c) the fund would have needed to take reasonable steps to confirm that investors were accredited investors. But at least one investor was not accredited according to the SEC order.

    [Pre-existing, Substantive Relationships and General Solicitation](https://www.compliancebuilding.com/2019/04/03/pre-existing-substantive-relationships-and-general-solicitation/)

    As cryptocurrency issuance declines, the Securities and Exchange Commission is continuing to clean out the fraud, mis-steps, and foolishness of coin promoters. These actions have carried over to th…

    
- 📑-growth-adoption

        Crypto⧉Finance

    📑-growth-adoption

    stats and other evidence of increasing adoption

    12 messages

    

    [Financial Services Industry Spends $1.7B Per Year on Blockchain | ...](https://www.greenwich.com/press-release/financial-service-industry-spends-17b-year-blockchain)

    Tuesday, June 12, 2018 Stamford, CT USA — The financial services industry is spending about $1.7 billion per year on blockchain, as banks and other

    [Worldwide Spending on Blockchain Forecast to Reach $11.7 Billion i...](https://www.idc.com/getdoc.jsp?containerId=prUS44150518)

    IDC examines consumer markets by devices, applications, networks, and services to provide complete solutions for succeeding in these expanding markets.

    
    
    [⧉ Infominer (@infominer33)](https://twitter.com/infominer33)

    1. Cryptocurrency, Blockchain, and The United Nations: A Chronological Tweet Storm In 2010 the UN Commission on Science and Technology for Development [CSTD] proposed the introduction of a technology-driven universal currency. [https://t.co/artC88Fuug](https://t.co/artC88Fuug)

    
    Twitter

    
    [dennis 🌱 (@pourteaux)](https://twitter.com/pourteaux)

    University rankings by alumni founders raising $1m or more in the last 12 months.

    
    Twitter

    [Bitcoin-Friendly Square Cash is Now More Popular Than Venmo](https://t.co/832u4VflyT)

    Square's Cash App, which now offers bitcoin trading in all 50 U.S. states, is now more popular than PayPal's peer-to-peer Venmo app.

    

    [All Australians Can Now Pay Their Bills With Bitcoin - Bitcoinist.com](https://bitcoinist.com/all-australians-can-now-pay-bills-bitcoin/)

    Cointree and Gobbill have announced a partnership that allows Australians to pay their bills using cryptocurrency.

    
    [White Paper: Canada’s Digital ID Future - A Federated Approach](https://www.cba.ca/embracing-digital-id-in-canada)

    In this brief, we highlight why Canada needs a digital identity system, how other countries have made progress in this area and the lessons we can learn from those experiences to build a system in Canada.

    

    [Cryptocurrencies Could Become Mainstream Payment Solution Within N...](https://www.prnewswire.com/news-releases/cryptocurrencies-could-become-mainstream-payment-solution-within-next-decade-finds-new-imperial-college-and-etoro-report-833609231.html)

    LONDON, July 9, 2018 /PRNewswire/ -- • New research from Imperial College London and eToro argues cryptocurrencies are already fulfilling one of three main...

    
    
    [Pomp 🌪 (@APompliano)](https://twitter.com/APompliano)

    1/ The best kep secret in crypto is that the institutional investors are already here.

    Retweets

    224

    Likes

    899

    
    Twitter

    
    [⧉ Infominer (@infominer33)](https://twitter.com/infominer33)

    "IDC expects blockchain spending to grow at a robust pace over the 2016-2021 forecast period with a five-year compound annual growth rate (CAGR) of 81.2% and total spending of $9.7 billion in 2021." [https://t.co/9DPdshgnZE](https://t.co/9DPdshgnZE) #blockchain

    
    Twitter

    [Worldwide Spending on Blockchain Forecast to Reach $11.7 Billion i...](https://www.idc.com/getdoc.jsp?containerId=prUS44150518)

    IDC examines consumer markets by devices, applications, networks, and services to provide complete solutions for succeeding in these expanding markets.

    

    [[UPDATED] Australian Bank Ready To Launch World Bank-Facilitated B...](https://www.ethnews.com/australian-bank-ready-to-launch-world-bank-facilitated-blockchain-bond)

    The Commonwealth Bank of Australia has announced it is creating what it claims is the first bond entirely transferred and managed on the blockchain.

    
    
    [The World Bank Treasury (@Treasury_WB)](https://twitter.com/Treasury_WB)

    Today we make history by creating the first global blockchain bond. The World Bank has mandated the Commonwealth Bank of Australia as the sole arranger for bond-i, the first global bond to use distributed ledger technology. Learn more: [https://t.co/tieQoQ9uLe](https://t.co/tieQoQ9uLe) #blockchain...

    Retweets

    102

    Likes

    113

    
    Twitter

    
    [Felipe (@PhilCrypto77)](https://twitter.com/PhilCrypto77)

    Fairly big news that has gone under the radar... US investors will now be able to buy #Bitcoin through their @Fidelity accounts. [https://t.co/pCwMBd7G8M](https://t.co/pCwMBd7G8M)

    Retweets

    492

    Likes

    1070

    
    Twitter

    
    [Ted Rogers (@tedmrogers)](https://twitter.com/tedmrogers)

    The Bay Area in two headlines

    Retweets

    1355

    Likes

    4299

    
    Twitter

    [Ethereum Co-Founder Joseph Lubin: Crypto Price Collapse Will Not C...](https://cointelegraph.com/news/ethereum-co-founder-joseph-lubin-crypto-price-collapse-will-not-constrain-further-growth)

    Ethereum co-founder Joseph Lubin anticipates new crypto value highs as successive bubbles have made the ecosystem stronger.

    
    [BitShares Taking Remittance Industry By Storm](https://medium.com/@michaelx777/bitshares-taking-remittance-industry-by-storm-72c5c3d09433)

    Last week Bitcoin dominated headlines after hitting all time highs and breaking the $10,000 barrier. While many investors had their eyes on…

    
    [Login With Coinbase: Can The Crypto Giant Protect User Privacy?](https://www.forbes.com/sites/stevenehrlich/2018/08/23/login-with-coinbase-can-the-crypto-giant-protect-user-privacy/)

    Coinbase has become ubiquitous throughout the crypto community. Now after its recent startup acquisition, Distributed Systems, it is moving aggressively into the decentralized identity space. However, its outsize influence on the industry creates privacy challenges that mus...

    [Deloitte 2018 Survey: Blockchain "Getting Closer to Breakout Momen...](https://cointelegraph.com/news/deloitte-2018-survey-blockchain-getting-closer-to-breakout-moment-every-day)

    Deloitte’s 2018 blockchain survey reveals that the technology is gaining significantly in traction at the executive level of enterprises across diverse industries.

    
    [Bitcoin Accepted [Everyw]here: Square Patents Crypto Payment Network](https://www.ccn.com/bitcoin-accepted-everywhere-square-wins-patent-for-cryptocurrency-payment-network/)

    Square has won a patent for a POS system that allows a merchant to accept payment in any currency, including bitcoin or another cryptocurrency.

    
    
    [شتر دیدی؟ ندیدی (@arbedout)](https://twitter.com/arbedout)

    First screenshot: Upper West Side, Manhattan Second screenshot; Upper East Side Manhattan Third screenshot: Iran Fourth screenshot: Venezuela

    Retweets

    1040

    Likes

    2749

    
    Twitter

    [SafeChain tests blockchain in an Ohio forfeiture auction](https://www.inman.com/2018/08/27/safechain-tests-blockchain-for-real-estate-in-an-ohio-forfeiture-auction/)

    Startup will help Franklin County auditor's office manage the backend of a forfeiture auction

    

    [Coinbase Research: 42% of Top 50 Universities Offer at Least One C...](https://cointelegraph.com/news/coinbase-research-42-of-top-50-universities-offer-at-least-one-crypto-related-class)

    Coinbase has reviewed the world’s top 50 universities and found out that forty-two percent of them offer crypto- and blockchain-related classes.

    
    [Opera crypto wallet update: Send crypto-collectibles directly from...](https://blogs.opera.com/mobile/2018/09/opera-crypto-wallet-update-opera-send-crypto-collectibles-directly-from-wallet/)

    We are innovating by allowing you to send crypto-collectibles directly from the wallet to the person you want to. You will get notifications about your transaction progress too.

    
    [Blockchain is here. What’s your next move?](https://www.pwc.com/blockchainsurvey)

    In PwC’s global survey, 84% of execs say their organisations have some involvement with blockchain. Four key strategies can help them build trust and stay on a path toward successful execution.

    

    [Blockchain and Crypto Jobs are Growing In Asia - The Bitcoin News](https://thebitcoinnews.com/blockchain-and-crypto-jobs-are-growing-in-asia/)

    Despite a downtrending crypto market, CNBC reports that blockchain and cryptocurrency jobs are growing in Asia. Recruitment firm Robert Walters told CNBC that the company has seen a 50 percent increase in the number of roles related to blockchain or cryptocurrencies in Asia s...

    

    [Blockchain and Crypto Jobs are Growing In Asia - The Bitcoin News](https://thebitcoinnews.com/blockchain-and-crypto-jobs-are-growing-in-asia/)

    Despite a downtrending crypto market, CNBC reports that blockchain and cryptocurrency jobs are growing in Asia. Recruitment firm Robert Walters told CNBC that the company has seen a 50 percent increase in the number of roles related to blockchain or cryptocurrencies in Asia s...

    

    [PwC Survey Identifies 'Usual Suspects' as Hindrances to Widespread...](https://bitcoinist.com/pwc-survey-identifies-usual-suspects-as-hindrances-to-widespread-blockchain-adoption/)

    A PwC survey has identified regulatory uncertainty, lack of trust, as major factors that hamper the adoption of blockchain.

    
    
    [Alexander Tapscott (@alextapscott)](https://twitter.com/alextapscott)

    LocalBitcoin volumes in Venezuela continue to skyrocket. This is why bitcoin matters. cc @_jillruth for first flagging this a few months back

    Retweets

    194

    Likes

    531

    
    Twitter

    [Over the next decade, over $2.5 trillion will be generated by bloc...](https://thefinanser.com/2018/09/next-decade-2-5-trillion-will-generated-blockchain-trade.html/)

    I just saw a report from the World Economic Forum, who estimate that over $1 trillion in new trade will be created through blockchain-based distributed ledger technologies (DLT) over the next decade. They also estimate it will reduce the global trading gap by $1.5 trillion. B...

    
    [$1.7B Retailer Overstock To Sell Bitcoin And Cryptos](https://cryptodaily.co.uk/2018/09/overstock-to-sell-bitcoin-cryptos/)

    Overstock.com will sell Bitcoin and other cryptocurrencies in Q1 or Q2 next year to take advantage of growing crypto awareness by consumers.

    
    
    [⧉ Infominer (@infominer33)](https://twitter.com/infominer33)

    [https://t.co/qi1PqpxtoU](https://t.co/qi1PqpxtoU)

    
    Twitter

    
    [⧉ Infominer (@infominer33)](https://twitter.com/infominer33)

    [https://t.co/R8PbmxsbSr](https://t.co/R8PbmxsbSr)

    
    Twitter

    [$194 Million was Moved Using Bitcoin With $0.1 Fee, Potential of C...](https://www.ccn.com/194-million-was-moved-using-bitcoin-with-0-1-fee-true-potential-of-crypto/)

    On October 16, an investor moved 29,999 Bitcoin (BTC) worth $194 million with a $0.1 fee, which with banks cost tens of thousands of dollars.

    
    [Top 5 Working Products in Blockchain (Updated — October 2018)](https://medium.com/theblock1/top-5-working-products-in-blockchain-updated-october-2018-9b18548bf8fc)

    The purpose of this article is purely educational (free of market price discussion) and will exhibit different use-cases of blockchain. The…

    
    [50+ Examples of How Blockchains are Taking Over the World](https://medium.com/@matteozago/50-examples-of-how-blockchains-are-taking-over-the-world-4276bf488a4b)

    When talking about blockchains, we commonly think of its applications in the future. “Blockchain will solve this, blockchain will achieve…

    

    [French tobacco shops will sell Bitcoin and Ethereum starting Janua...](https://venturebeat.com/2018/11/22/french-tobacco-shops-will-sell-bitcoin-and-ethereum-starting-january-2019/)

    The French Federation of Tobacco Vendors has approved plans for the country's 27,000 tobacco shops to sell Bitcoin and Ethereum.

    

- 📑-twitter

        Crypto⧉Finance

    📑-twitter

    4 messages

    

    
    [Nathaniel Popper (@nathanielpopper)](https://twitter.com/nathanielpopper)

    My take on the 10 most influential people in blockchain/crypto today, in today's @Dealbook special section Amber Baldet Brian Behlendorf Vitalik Buterin Chris Dixon Daniel Larimer Jed McCaleb Michael Novogratz Elizabeth Stark Valerie Szczepanik Jihan Wu [https://t.co/iVP6](https://t.co/iVP6)...

    Retweets

    204

    Likes

    591

    
    Twitter

    
    [Vortex (@theonevortex)](https://twitter.com/theonevortex)

    A reminder that my twitter lists are 100% public and available for all to see. The #bitcoin list currently has 341 members. If they are on my list then they are an important person to follow. [https://t.co/ifXMJtGHtY](https://t.co/ifXMJtGHtY)

    
    Twitter

    
    [Jameson Lopp (@lopp)](https://twitter.com/lopp)

    #FollowFriday: Bitcoin Developers: [https://t.co/LfBo9OFCqI](https://t.co/LfBo9OFCqI) Lightning Developers: [https://t.co/IZEdrH3Z4M](https://t.co/IZEdrH3Z4M) If you know of anyone I missed who belongs on either list, please reply to this tweet
    Likes

    204

    
    Twitter

    
    [The Crypto Dog📈 (@TheCryptoDog)](https://twitter.com/TheCryptoDog)

    Nuts
    
    Twitter

    
    [Richard ⚖️ (@ricburton)](https://twitter.com/ricburton)

    Who has taught you a lot on Twitter? @reply them below:

    
    Twitter

    
    [Godfather (@GodfatherCrypto)](https://twitter.com/GodfatherCrypto)

    #FF Technicals, fundamentals, insights @cryptogainz1 @cryptocurb @nlw @crypto_brahma @joezabb @altcoinpsycho @jonnymoetrades @filbfilb @cryptowilson @mansa_godson @trader_travis @technicalcrypto @cryptomessiah @anbessa100 @galaxybtc @coinztrader @cryptobanger @onemanatati...

    
    Twitter

    
    [YORK780 (@YORK780)](https://twitter.com/YORK780)

    #FF the kings Enjoy the weekend boys @crypto_rand @Marsmensch @MediumSqueeze @BullyEsq @p0nd3ea @CryptoTutor @gMAKcrypto @anondran @Anbessa100 @shitcoinz @StartaleTV @Crypto_Twitt_r @TheCryptoDog @CryptOJSimpson @Tradermayne @paul_btc [https://t.co/ytrymMq](https://t.co/ytrymMq)...

    
    
    Twitter

    

    [https://twitter.com/mrjasonchoi/status/1154957604864610304?s=12](https://twitter.com/mrjasonchoi/status/1154957604864610304?s=12)

    
    [Jason Choi (@mrjasonchoi)](https://twitter.com/mrjasonchoi)

    .@Wharton @Penn has one of the most passionate crypto communities: Finance - @BitMEXdotcom

    
    
    @CryptoHayes - @BitwiseInvest

    
    
    @HHorsley & @hongkim__ - @BlockTower

    
    
    @AriDavidPaul - @fundstrat

    
    
    Tom Lee - @wave_financial

    
    
    @WarcMeinstei...

    
    Twitter

    

    [https://twitter.com/mycrypto/status/1161382124034723840?s=12](https://twitter.com/mycrypto/status/1161382124034723840?s=12)

    
    [MyCrypto.com (@MyCrypto)](https://twitter.com/MyCrypto)

    There are over 300 Twitter accounts, and more every day, that exist solely to steal your crypto.

    
    Article by @sniko_, our Director of Security:

    [https://t.co/qvPbo9inBW](https://t.co/qvPbo9inBW)

    
    Twitter