# 🌏-asia

Crypto⧉Finance

🌏-asia

4 messages

[https://techcrunch.com/2018/08/15/asian-investors-have-plenty-of-cash-a-hearty-appetite-for-investments-and-a-different-approach-to-doing-deals/](https://techcrunch.com/2018/08/15/asian-investors-have-plenty-of-cash-a-hearty-appetite-for-investments-and-a-different-approach-to-doing-deals/)

[Asian investors have plenty of cash, a hearty appetite for investm...](https://techcrunch.com/2018/08/15/asian-investors-have-plenty-of-cash-a-hearty-appetite-for-investments-and-a-different-approach-to-doing-deals/)

If you’re being courted by Asian investors, you’ll need to adjust the VCs’ expectations. That can be a challenging task when the parties have different perspectives on appropriate management styles and levels of control.

[https://www.bitcoin.org.hk/hong-kong-bitcoin-taxation/](https://www.bitcoin.org.hk/hong-kong-bitcoin-taxation/)

[Hong Kong Bitcoin Taxation](https://www.bitcoin.org.hk/hong-kong-bitcoin-taxation/)

Taxation of Bitcoin, Cryptocurrencies and Tokens in Hong Kong

[https://www.elliptic.co/our-thinking/cryptocurrency-regulation-asia-pacific](https://www.elliptic.co/our-thinking/cryptocurrency-regulation-asia-pacific)

[The Extremes of Cryptocurrency Regulation in Asia-Pacific](https://www.elliptic.co/our-thinking/cryptocurrency-regulation-asia-pacific)

Cryptocurrency regulation across the Asia-Pacific region varies immensely. Here’s what your crypto business needs to know to meet your compliance objectives.

[Jason Choi (@mrjasonchoi)](https://twitter.com/mrjasonchoi/status/1167391900057210880?s=20)

When I ventured into crypto investing full time, I looked for: 1) Small team, more ownership than I was ready for 2) Deep relationships in Asia 3) Fundamentals focused 4) Mentors with 10-20 yrs of exp managing large teams & Ms - Bs in capital If these are your priorit...

Twitter

- 🌏-philippine

        Crypto⧉Finance

    🌏-philippine

    1 messages

    

    [https://news.bitcoin.com/philippine-sec-publish-draft-crypto-exchange-regulations-next-week/](https://news.bitcoin.com/philippine-sec-publish-draft-crypto-exchange-regulations-next-week/)

    [Philippine SEC to Publish Draft Crypto Exchange Regulations Next Week](https://news.bitcoin.com/philippine-sec-publish-draft-crypto-exchange-regulations-next-week/)

    The Philippine Securities and Exchange Commission (SEC) has exploring regulating cryptocurrency as trading platforms, according to SEC Commissioner Ephyro Luis Amatong. Commissioner Amatong also indicated that the current Australian and Swiss legislative apparatus pertaining ...

    
- 🌏-thailand

        Crypto⧉Finance

    🌏-thailand

    1 messages

    

    [https://cointelegraph.com/news/bank-of-thailand-announces-milestone-digital-currency-project-using-r3-corda-platform](https://cointelegraph.com/news/bank-of-thailand-announces-milestone-digital-currency-project-using-r3-corda-platform)

    [Bank of Thailand Announces 'Milestone' Digital Currency Project Us...](https://cointelegraph.com/news/bank-of-thailand-announces-milestone-digital-currency-project-using-r3-corda-platform)

    The Bank of Thailand has revealed a major collaborative project to develop a wholesale Central Bank Digital Currency using R3’s Corda platform.

    
- 🌏-korea-regulation

        Crypto⧉Finance

    🌏-korea-regulation

    2 messages

    

    [https://news.bitcoin.com/korean-crypto-exchanges-aml-compliance-banks/](https://news.bitcoin.com/korean-crypto-exchanges-aml-compliance-banks/)

    [As Korean Crypto Exchanges Step Up AML Compliance, Banks Are Faili...](https://news.bitcoin.com/korean-crypto-exchanges-aml-compliance-banks/)

    As the South Korean government steps up its anti-money laundering (AML) oversight, major crypto exchanges in the country are voluntarily complying while banks are reportedly failing to meet the guidelines for compliance. South Korea has also been discussing ways to boost cryp...

    
    

    [https://theicon.ist/2019/08/19/jeju-bounces-back-with-a-blockchain-hub-after-loss-of-regulation-free-zone-to-busan/](https://theicon.ist/2019/08/19/jeju-bounces-back-with-a-blockchain-hub-after-loss-of-regulation-free-zone-to-busan/)

    [Jeju Bounces Back With a Blockchain Hub After Loss of ‘Regulatio...](https://theicon.ist/2019/08/19/jeju-bounces-back-with-a-blockchain-hub-after-loss-of-regulation-free-zone-to-busan/)

    Jeju might have lost to Busan in its bid to become Korea’s first blockchain regulation-free zone, but that doesn’t mean Jeju’s giving up on the blockchain industry.

    
- 🌏-china-regulation

        Crypto⧉Finance

    🌏-china-regulation

    2 messages

    

    [https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown](https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown) [https://twitter.com/katherineykwu/status/1068581766833848320](https://twitter.com/katherineykwu/status/1068581766833848320) [https://twitter.com/mahieu_tje/status/1132578987648143360?s=12](https://twitter.com/mahieu_tje/status/1132578987648143360?s=12)

    [China's Central Bank Moves to Restrict Free Crypto Giveaways - Coi...](https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown)

    The People's Bank of China, the country's central bank, is looking to clamp down on airdrops – free distributions of crypto tokens.

    
    
    [Katherine Wu (@katherineykwu)](https://twitter.com/katherineykwu)

    
    I often get questions around crypto/blockchain related developments in China, so I thought I would start doing periodic summaries of headlines or news that I come across that is not often covered by the media or discussion over here in the U.S. Here is the first ...

    Likes

    187

    
    Twitter

    [https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown](https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown) [https://www.bbc.com/news/business-41157249](https://www.bbc.com/news/business-41157249)

    [China's Central Bank Moves to Restrict Free Crypto Giveaways - Coi...](https://www.coindesk.com/pboc-looks-to-tackle-airdrop-tokens-market-in-new-clampdown)

    The People's Bank of China, the country's central bank, is looking to clamp down on airdrops – free distributions of crypto tokens.

    
    [China bans initial coin offerings](https://www.bbc.com/news/business-41157249)

    Chinese regulators make it illegal to raise funds by offering digital currencies.

    
- 🌏-japan-regulation

        Crypto⧉Finance

    🌏-japan-regulation

    1 messages

    

    [https://www.coindesk.com/japanese-crypto-exchanges-file-to-form-self-regulatory-organization](https://www.coindesk.com/japanese-crypto-exchanges-file-to-form-self-regulatory-organization)

    [Japanese Crypto Exchanges File to Form Self-Regulatory Organizatio...](https://www.coindesk.com/japanese-crypto-exchanges-file-to-form-self-regulatory-organization)

    Sixteen Japanese cryptocurrency exchanges have applied to form a certified self-regulatory organization for the industry.

    ![https://static.coindesk.com/wp-content/uploads/2018/08/jcvea.jpg](https://static.coindesk.com/wp-content/uploads/2018/08/jcvea.jpg)