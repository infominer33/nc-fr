---
title: Overview of Global Crypto Regulations
---

## Contents

* [Crypto Self-Regulation](#Crypto-Self-Regulation)
  * [Messari](#Messari)
  * [Global Digital Finance](#Global-Digital-Finance)
* [Global Regulations](#Global-Regulations)
* [Where to Locate](#Where-to-Locate)
* [USA Regulations](#USA-Regulations)
  * [SEC](#SEC)
    * [Reg +A](#Reg-A)
    * [Reg +A Requirements](#Reg-A-Requirements)
    * [Blockstack](#Blockstack)
  * [Utility Tokens](#Utility-Tokens)
  * [California \ Airdrops](#California)
  * [Wyoming](#Wyoming)
    * [WY-Regulation](#WY-Regulation)
    * [WY-Podcasts](#WY-Podcasts)
* [Asia](#Asia)
* [Europe](#Europe)
  * [Estonia](#Estonia) 
  * [Gibraltar](#Gibraltar)
* [Resources](#Resources)
  * [Compliance](#Compliance)
  * [Custody](#Custody)
  * [Banking](#Banking)
  * [Exchanges](#Exchanges)
* [PremierOffshore.com](#PremierOffshorecom) 

## Crypto Self-Regulation

These industry bodies for self-regulation may prove invaluable for certain insights that wouldn't be easy to find up in a google search. I would seek to cultivate a relationship with them, and others.

### Messari

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">🥳 ANNOUNCING our first cohort of launch partners who are committed to transparency and self-regulation

* [messari.io/registry](https://messari.io/registry)
  >Open-source library of relevant token project data serves as a “single source of truth.” Disclosures provided by projects and validated by the system of validating token-holders. Platform ensures updates are disseminated publicly to a wide audience in a timely fashion.

https://messari.github.io/tcr/Messari%20Registry%20Press%20Release%2011-27-18.pdf
  >Messari’s registry aims to become a single source of truth for basic cryptoasset information. Participating  projects are voluntarily disclosing basic information regarding their token design, supply details, technology audits, official communication channels, and relevant team members, investors, and advisors. These profiles  can then be freely accessed industry-wide, providing a reliable, standardized resource to industry participants that is nonexistent today. Such basic data standards can facilitate diligence  processes for crypto service providers like exchanges and wallets, retail and  professional investors, and regulators alike.

### Global Digital Finance

>GDF is an industry membership body that promotes the adoption of best practices for cryptoassets and digital finance technologies, through the development of conduct standards, in a shared engagement forum with market participants, policymakers and regulators.

GDF is a wealth of information and I will be repeatedly referring back to their contet. They do a good job at generally condensing global crypto regulation, and have offices in the UK.

**Working Groups**
https://www.gdf.io/working-groups/

**Taxonomy for Cryptographic Assets** -From the Perspective of General Global Regulatory Standards
https://www.gdf.io/wp-content/uploads/2018/10/0003_GDF_Taxonomy-for-Cryptographic-Assets_Web-151018.pdf

**GDF Response to the proposed canadian regulation for exchanges**: 
https://www.gdf.io/wp-content/uploads/2019/05/IIROC-April-24-2019.pdf

**Link to the source which above refers to**: https://www.bcsc.bc.ca/21-402_[Joint_Canadian_Securities_Administrators_Investment_Industry_Regulatory_Organization_of_Canada_Consultation_Paper]_03142019/

## Global Regulations

These are some of the more comprehensive resources I've gathered, I've listed them in order of how up to date I think they are. 

* [Cryptocurrency regulation in 2018: Where the world stands right now](https://thenextweb.com/hardfork/2018/04/27/cryptocurrency-regulation-2018-world-stands-right-now/)
* [Regulation of Cryptocurrencies in Selected Jurisdictions](https://www.loc.gov/law/help/cryptocurrency/regulation-of-cryptocurrency.pdf) -June 2018 
  • Argentina • Australia • Belarus • Brazil • Canada • China • France • Gibraltar • Iran • Israel • Japan • Jersey • Mexico • Switzerland
* [Regulating cryptocurrencies: assessing market reactions](https://www.bis.org/publ/qtrpdf/r_qt1809f.htm)
* [US, China and Europe have different data laws - Published Mon, Dec 10 2018](https://www.cnbc.com/2018/12/11/data-regulations-trade-war-top-business-risks-in-2019-control-risks.html)
* [Regulation of Cryptocurrency Around the World](https://www.loc.gov/law/help/cryptocurrency/world-survey.php) -Last Updated: 06/24/2019
  * [Full Report](https://www.loc.gov/law/help/cryptocurrency/cryptocurrency-world-survey.pdf)
  * [Countries that Have or Are Issuing National or Regional Cryptocurrencies](https://www.loc.gov/law/help/cryptocurrency/map3.pdf)

* [Cryptocurrency Regulation across the World: September 2018](https://bitfalls.com/2018/08/31/cryptocurrency-regulation-across-the-world/) 
  * a quick breakdown of many (sub)juristictions I didn't cover
* https://flagtheory.com/archives/ - Valuable Resource since 2012

## Where to Locate

These are broad guides that seek to highlight the friendliest juristictions for establishing your crypto firm.

* https://coin360.com/blog/cryptofriendly-jurisdictions-an-overview 
  * Malta, Gibralter, Estonia, Switzerland, Phillipeans

https://www.nytimes.com/2018/07/29/technology/cryptocurrency-bermuda-malta-gibraltar.html
>SAN FRANCISCO — Hedge funds go to the Cayman Islands to incorporate. Big companies are generally domiciled in Delaware. And online poker companies often set up their bases in Gibraltar and Malta.
>
>Now the race is on to become the go-to destination for cryptocurrency companies that are looking for shelter from regulatory uncertainty in the United States and Asia.
>
>In small countries and territories including Bermuda, Malta, Gibraltar and Liechtenstein, officials have recently passed laws, or have legislation in the works, to make themselves more welcoming to cryptocurrency companies and projects. In Malta, the government passed three laws on July 4 so companies can easily issue new cryptocurrencies and trade existing ones. In Bermuda this year, the legislature passed a law that lets start-ups doing initial coin offerings apply to the minister of finance for speedy approval.

* [Pros & Cons: How do you choose which crypto-friendly jurisdiction to start your company in?](https://hackernoon.com/pros-cons-how-do-you-choose-which-crypto-friendly-jurisdiction-to-start-your-company-in-df4e0b239aa8) - March 2019
  >Malta, USA, Switzerland, Gibraltar, Canada, Lithuania, Estonia, Israel, Singapore, Hong Kong, United Arab Emirates, Japan, Korea
* https://medium.com/h-o-l-o/choosing-a-jurisdiction-for-your-digital-currency-business-32ff1643e17c
* https://flagtheory.com/where-to-start-your-cryptocurrency-business/
* [Seven Countries Where Cryptocurrency Investments Are Not Taxed](https://www.forbes.com/sites/rogerhuang/2019/06/24/seven-countries-where-cryptocurrency-investments-are-not-taxed/#3f74a4d37303)

## USA Regulations

We'll start here, because these seem to be the farthest reaching, in the world.

* [Supreme Court And Digital Privacy: Should Blockchain Companies Challenge The Bank Secrecy Act?](https://www.forbes.com/sites/caitlinlong/2018/06/28/supreme-court-and-digital-privacy-should-blockchain-companies-challenge-the-bank-secrecy-act/#4a4a0e5962fc) - Operation Choke-Point
* [Interpreting Recent Signals from US Regulatory Agnecies](https://blog.circle.com/2019/05/23/our-take-interpreting-recent-signals-from-us-regulatory-agencies/)

### SEC

* [www.sec.gov/ICO](https://www.sec.gov/ICO)
* [www.sec.gov/finhub](https://www.sec.gov/finhub)
* [Joint Staff Statement on Broker-Dealer Custody of Digital Asset Securities](https://www.sec.gov/news/public-statement/joint-staff-statement-broker-dealer-custody-digital-asset-securities) - Division of Trading and Markets, U.S. Securities and Exchange Commission - Office of General Counsel, Financial Industry Regulatory Authority
  >An entity that buys, sells, or otherwise transacts or is involved in effecting transactions in digital asset securities for customers or its own account is subject to the federal securities laws, and may be required to register with the Commission as a broker-dealer and become a member of and comply with the rules of a self-regulatory organization (“SRO”), which in most cases is FINRA.  Importantly, if the entity is a broker-dealer, it must comply with broker-dealer financial responsibility rules,[4] including, as applicable, custodial requirements under Rule 15c3-3 under the Securities Exchange Act of 1934 (the “Exchange Act”), which is known as the Customer Protection Rule. 
  >
  >**Noncustodial Broker-Dealer Models for Digital Asset Securities** -examples:
  >
  >is where the broker-dealer sends the trade-matching details (e.g., identity of the parties, price, and quantity) to the buyer and issuer of a digital asset security—similar to a traditional private placement—and the issuer settles the transaction bilaterally between the buyer and issuer, away from the broker-dealer.  In this case, the broker-dealer instructs the customer to pay the issuer directly and instructs the issuer to issue the digital asset security to the customer directly (e.g., the customer’s “digital wallet”). 
  >  
  >where a broker-dealer facilitates “over-the counter” secondary market transactions in digital asset securities without taking custody of or exercising control over the digital asset securities.  In this example, the buyer and seller complete the transaction directly and, therefore, the securities do not pass through the broker-dealer facilitating the transaction.
  >
  >Another example is where a secondary market transaction involves a broker-dealer introducing a buyer to a seller of digital asset securities through a trading platform where the trade is settled directly between the buyer and seller.  For instance, a broker-dealer that operates an alternative trading system (“ATS”) could match buyers and sellers of digital asset securities and the trades would either be settled directly between the buyer and seller, or the buyer and seller would give instructions to their respective custodians to settle the transactions.
* [SEC.gov -Engaging on Fund Innovation and Cryptocurrency-related Holdings](https://www.sec.gov/investment/fund-innovation-cryptocurrency-related-holdings)
  * https://www.sec.gov/divisions/investment/reality-shares-innovation-cryptocurrency.pdf 
* [Thread of threads from the SEC’s FinTech Forum #SECfintech](https://twitter.com/verityesq/status/1134516992696823809?s=12)
* [Framework for Investment Contract Analysis](https://www.sec.gov/corpfin/framework-investment-contract-analysis-digital-assets)
  >The term "security" includes an "investment contract," as well as other instruments such as stocks, bonds, and transferable shares.  A digital asset should be analyzed to determine whether it has the characteristics of any product that meets the definition of "security" under the federal securities laws.  In this guidance, we provide a framework for analyzing whether a digital asset has the characteristics of one particular type of security – an "investment contract."
  * [Hinman Statement on Framework for Investment Contract Analysis](https://www.sec.gov/news/public-statement/statement-framework-investment-contract-analysis-digital-assets)
  * [How Useful is the SEC's Guidance on ICOs?](https://unconfirmed.libsyn.com/how-useful-is-the-secs-guidance-on-icos-ep067) -podcast

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">1/ Talked to a NYAG attorney last night, who said this:<br><br>The SEC is engaged in the crypto exchange space. With Etherdelta, they got Coburn to cooperate &amp; show that existing laws fit in this space. Translated: “We know how to put the crypto economy in our existing framework.”</p>&mdash; Katherine Wu (@katherineykwu) <a href="https://twitter.com/katherineykwu/status/1060913478288973824?ref_src=twsrc%5Etfw">November 9, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

* [Can a Future Network’s Token Be “Presold” and Not Qualify as an investment contract](https://coincenter.org/entry/can-a-future-network-s-token-be-presold-and-not-qualify-as-an-investment-contract)
* [Company Settles Unregistered ICO Charges After Self-Reporting to SEC](https://www.sec.gov/news/press-release/2019-15)
* [Annotated Guide to the SEC's Complaint against KIK](https://www.katherinewu.me/writings/2019/6/4/annotated-guide-to-the-secs-complaint-against-kik)
  * https://news.ycombinator.com/item?id=20096733

#### Reg +A

* https://www.wealthforge.com/insights/the-difference-between-regulation-a-and-other-capital-raise-options
  - Reg A, Reg D, Crowdfunding and IPO are considered 
* https://www.sec.gov/files/form1-a.pdf
* https://www.investopedia.com/terms/r/regulationa.asp
* https://www.sec.gov/smallbusiness/exemptofferings/rega
  >Regulation A is an exemption from registration for public offerings. Regulation A has two offering tiers: Tier 1, for offerings of up to $20 million in a 12-month period; and Tier 2, for offerings of up to $50 million in a 12-month period. For offerings of up to $20 million, companies can elect to proceed under the requirements for either Tier 1 or Tier 2.
  >
  >There are certain basic requirements applicable to both Tier 1 and Tier 2 offerings, including company eligibility requirements, bad actor disqualification provisions, disclosure, and other matters. Additional requirements apply to Tier 2 offerings, including limitations on the amount of money a non-accredited investor may invest in a Tier 2 offering, requirements for audited financial statements and the filing of ongoing reports. Issuers in Tier 2 offerings are not required to register or qualify their offerings with state securities regulators.

  - [Regulation-A-FAQ.pdf](https://www.bassberrysecuritieslawexchange.com/wp-content/uploads/sites/201/2018/06/Regulation-A-FAQ.pdf)

>**Who is Reg A+ Right For?**
>* Companies Looking to Raise Between $3 – $50 million
>* Consumer-Facing Companies with Clear Value Propositions
>* Companies That Want To Make a Large Splash
  >When executed correctly, a Regulation A mini-IPO can also be a robust customer acquisition channel, just as with a “typical” product launch.

#### Reg +A Requirements

[Blockstack's Reg A+ Filing and the Future of Token Offerings - Ep.069](https://podcasts.apple.com/us/podcast/blockstacks-reg-filing-future-token-offerings-ep-069/id1347049808?i=1000435420416) (more on blockstack, below)

* https://mangumlaw.net/regulation-a-offering/
  >  * A registration statement on Form 1-A, containing information about your securities’ material risks, distribution plan, financial health, operations, and other key data
  >  * Two years of financial information that was audited by a licensed auditor
  >  * Other supporting evidence and exhibits, such as contracts, agreements, and corporate records
  >  * Information about how your company will use your fundraising proceeds
* https://help.startengine.com/hc/en-us/articles/115002319846-What-are-some-of-the-Reg-A-qualifications-
  > What are all of the things I need to include in my Form 1-A?
    > * Issuer Information
    > * Contact Information
    > * Financial Statements
    > * Issuer Eligibility
    > * Rule 262 Application (Bad Actor Rules)
    > * Summary Information Regarding the Offering and Other Current or Proposed Offerings
    > * Jurisdictions in Which Securities are to be Offered
    > * Unregistered Securities Issued or Sold Within One Year
    > * Offering Circular
    > * Summary and Risk Factors
    > * Dilution
    > * Plan of Distribution and Selling Security Holders
    > * Use of Proceeds to Issuer
    > * Description of Business
    > * Description of Property
    > * Management’s Discussion and Analysis of Financial Condition and Results of Operations
    > * Directors, Executive Officers and Significant Employees
    > * Compensation of Directors and Executive Officers
    > * Security Ownership of Management and Certain Security Holders
    > * Interest of Management and Others in Certain Transactions
    > * Securities Being Offered
    > * Part F/S
    > * Financial Statements for Tier 1 Offerings
    > * Financial Statement Requirements for Tier 2 Offerings  
    > * Index to Exhibits (P. 26)
    > * Description of Exhibits (P. 26)
    > * Signatures
* http://www.capmarketslaw.com/post/545/one-of-the-hidden-benefits-of-regulation-a-offerings-variable-pricing/3
  >A likely overlooked advantage of Regulation A+ (“Reg A”) is that it allows an issuer to conduct certain delayed and continuous offerings without the need to set a share price at the time of qualification.
   
#### Testing the waters

* https://corpgov.law.harvard.edu/2019/03/27/practical-implications-of-proposed-testing-the-waters-for-all-issuers-under-u-s-securities-law/
* https://www.securitieslawyer101.com/2018/regulation-a-attorneys-testing-waters-regulation/
* https://www.crowdcheck.com/blog/regulation-and-testing-waters

#### Blockstack

* https://blog.blockstack.org/blockstack-publicly-files-regulated-token-offering-with-the-sec/
* https://www.sec.gov/Archives/edgar/data/1719379/000110465919020748/a18-15736_1partiiandiii.htm
* https://news.ycombinator.com/item?id=20413420
* https://blog.blockstack.org/blockstack-token-sale-sec-qualified/

### Utility Tokens

There are indications that utility tokens, may, after all, eventually be excempted from securities law in all\most US juristictions.

* https://www.coindesk.com/montana-passes-bill-to-exempt-utility-tokens-from-securities-laws
* [Unconfirmed: Caitlin Long on How 'Utility Tokens' Are Now Legal In Wyoming](https://podcasts.apple.com/us/podcast/unchained-your-no-hype-resource-for-all-things-crypto/id1123922160?i=1000406509010)

### California

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">1/ 🚨 NEW LAW ALERT. A federal court in California just ruled against the SEC in an ICO case, explaining what a plaintiff will now have to prove for an ICO to be a security. If you care about whether ICOs create securities, read on. If not whatever im not the boss of you</p>&mdash; Marco Santori (@msantoriESQ) <a href="https://twitter.com/msantoriESQ/status/1068246911533092866?ref_src=twsrc%5Etfw">November 29, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Wyoming

* [2019 Blockchain Task Force](https://www.wyoleg.gov/Committees/2019/S3)
  * [DraftOnly - SUMMARY of PROCEEDINGS](https://wyoleg.gov/InterimCommittee/2019/S3-20190506Meeting%20Minutes.pdf)
  * [Wyoming Legislative Joint Blockchain Task Force 2019, Day 1, May 6](https://www.youtube.com/watch?v=CPvN8HdhdXo) - Video

* [WyoHackathon 2019](https://wyohackathon.io/)
* [FIAU08-10690-Assistant Bank Examiner-Cheyenne](https://www.governmentjobs.com/careers/wyoming?department%5b0%5d=021-Division%20of%20Banking&sort=PositionTitle%7CAscending)
* [The State of Wyoming’s Economic Development Agency -Opportunity Zones](http://www.wyomingbusiness.org/opportunityzones)
* [facebook.com/WyomingBlockChain/](https://www.facebook.com/WyomingBlockChain/)
* [twitter.com/WyoBlockchain](https://twitter.com/WyoBlockchain)
* [Wyoming’s “Crypto-Bank” Charter](https://www.lexology.com/library/detail.aspx?g=bcdf7b6a-5fde-4b08-aa17-2349676de0dd)
* https://www.bestwyomingregisteredagent.com/non-us-wyoming-business/
* https://howtostartanllc.com/wyoming-llc
* https://www.hkwyolaw.com/kaufman-appointed-to-wyoming-blockchain-task-force/
* https://www.hkwyolaw.com/attorney/matthew-d-kaufman/

#### WY-Regulation

* [What Do Wyoming's 13 New Blockchain Laws Mean?](https://www.forbes.com/sites/caitlinlong/2019/03/04/what-do-wyomings-new-blockchain-laws-mean/#145d09d45fde)
  >Wyoming has now enacted a total of 13 blockchain-enabling laws, making it the only US state to provide a comprehensive, welcoming legal framework that enables blockchain technology to flourish, both for individuals and companies.

-https://wyoleg.gov/Legislation/2019/sf0125
>AN ACT **relating to property; classifying digital assets within existing laws; specifying that digital assets are property** within the Uniform Commercial Code; **authorizing security interests in digital assets**; **establishing an opt-in framework for banks to provide custodial services** for digital asset property as custodians; **specifying standards and procedures for custodial services under this act**; clarifying **the jurisdiction of Wyoming courts relating to digital assets; authorizing a supervision fee; making an appropriation; authorizing positions; specifying applicability; authorizing the promulgation of rules; and providing for an effective date.**

>Authorizes the first true “qualified custodian” for digital assets which is a bank. Wyoming banks can start such operations as soon as September 1, 2019. Wyoming’s digital asset custodians will stand out above all others because they will respect the DIRECT ownership nature of digital assets
...

>In sum, Wyoming will become known as the home of SOLVENT, investor-friendly digital asset custodians to which investment fiduciaries are likely to migrate over time.  

-https://www.wyoleg.gov/Legislation/2019/hb0057
  >Creates a fintech sandbox to provide **regulatory relief to financial innovators from existing laws for up to 3 years**. It’s broadly reciprocal with fintech sandboxes both in the US and globally;

-https://www.wyoleg.gov/Legislation/2019/hb0074
  >Authorizes a **new type of state-chartered depository institution to provide basic banking services to blockchain and other businesses**. The bank is required to have 100% reserves, cannot lend, is for business depositors only, and **FDIC insurance is optional. Such banks could be operating as soon as March 31, 2020**;

-https://www.wyoleg.gov/Legislation/2018/SF0111
>And, for digital assets specifically, last year Wyoming exempted them from property taxes.

-https://www.wyoleg.gov/Legislation/2018/HB0019
  >Wyoming’s money transmitter law exempts crypto-to-crypto transactions, effective as of last year. Many lawyers worry that Lightning Network transactions may run afoul of money transmitter laws. Well, not in Wyoming (#probably
-https://www.wyoleg.gov/Legislation/2018/HB0101
-https://www.wyoleg.gov/Legislation/2019/hb0185
  >If you’re working on security tokens, you won’t find a friendlier state because Wyoming law legally recognizes both uncertificated and certificated blockchain shares of stock. Delaware was first to recognize blockchain shares, but it only recognizes uncertificated versions. Wyoming’s new law regarding certificated shares just took effect this week, and WOW, Missouri was lightning fast in already copying it
-https://www.wyoleg.gov/Legislation/2018/HB0070
  >Wyoming was the first state to exempt utility tokens from its state securities laws, which took effect last year. State law doesn’t trump federal laws regarding securities, but I’m pleased that Arizona also enacted a similar law last year and five other states have proposed it this year.

-https://www.wyoleg.gov/Legislation/2019/hb0113
  >One bill enables Wyoming’s electric utilities to negotiate directly with miners, instead of requiring them to go through the ratemaking process. All gains and losses from mining agreements remain with the utility’s shareholders, thereby completely insulating retail electric customers from these transactions. 

-https://www.wyoleg.gov/Legislation/2019/sf0159
  >And, with a goal to help Wyoming’s struggling coal industry—which is crucial to Wyoming and is trying to recover from low coal prices—Wyoming passed a bill to provide a process for Wyoming’s electric utilities to sell the coal-fired generation plants they would otherwise permanently be shutting down. Potential buyers may include crypto miners, among others, and I’m told power costs available in Wyoming would be highly competitive with the best electricity prices available to miners around the world.

-https://supreme.justia.com/cases/federal/us/474/361/
  >A bank license is superior to a trust company license for digital asset custody, for many reasons. Some have expressed concerns about triggering the Bank Holding Company Act (BHCA) by obtaining a Wyoming bank license, but a Wyoming special-purpose depository institution does not meet the definition of “bank” under the BHCA because it can’t make commercial loans. The US Supreme Court has rejected previous attempts by the Federal Reserve to expand this definition, so Wyoming's special-purpose depository institution is a pretty neat regulatory option for those wanting to become qualified custodians of digital assets.

* [KORPORATIO - FIRST MOVER IN WYOMING ENABLING REAL-WORLD COMPANIES](https://korporatio.com/2019/07/15/korporatio-first-mover-in-wyoming-enabling-real-world-companies/)
* [The Newest Haven for Cryptocurrency Companies? Wyoming](https://www.wired.com/story/newest-haven-cryptocurrency-companies-wyoming/)
* [Bulls, Bills, and Blue Sky Thinking: How Wyoming Became the Blockchain State](https://breakermag.com/bulls-bills-and-blue-sky-thinking-how-wyoming-became-the-blockchain-state/)
* [ConsensysMedia-What Wyoming’s 13 New Crypto Laws Mean for Blockchain in the US](https://media.consensys.net/what-wyomings-13-new-crypto-laws-mean-for-blockchain-in-the-us-1bcf8b7a39d4)
  >I think there are things that Wyoming give a couple of very big advantages. One is privacy. Wyoming protects privacy for its LLC registrants more than most other states. Wyoming invented the LLC in 1977, and since then Wyoming has protected that privacy approach fiercely. That value is aligned very well with the crypto industry. And the other value that aligns very well with the crypto industry is no state taxes. And again, this gets back to Wyoming assessing no state taxes on any crypto-asset whatsoever. Other states may copy Wyoming's laws, but none of them can offer such a favorable state tax regime. So Wyoming wins on both of those fronts.
* [Wyoming Passes New Friendly Regulations for Crypto Assets](https://bitcoinmagazine.com/articles/wyoming-passes-new-friendly-regulations-crypto-assets)

#### WY-Podcasts 

* [Wall Street, Wyoming and Bitcoin- The Exchange with Caitlin Long of Wyoming Blockchain Coalition](https://podcasts.apple.com/us/podcast/wall-street-wyoming-bitcoin-exchange-caitlin-long-wyoming/id1440493210?i=1000429482274)
* [Show This Week in Bitcoin, Ep Special: Wyoming, the Crypto state - with Caitlin Long - Mar 4, 2019](https://podcasts.apple.com/us/podcast/special-wyoming-the-crypto-state-with-caitlin-long/id1332111459?i=1000431096624)

## Asia

Asia is an exciting market, but nothing desireable as far as regulations go. Korea is where it's at if you wanted to release some legit decentralized product(or stop caring about US investors), Korea are massive investors. (Most of Ripple $$ comes from Korea.)

https://flagtheory.com/start-crypto-exchange-asia/

## Europe

* https://flagtheory.com/start-crypto-exchange-europe/

### Estonia

Estonia was a lower-cost juristiction, but is getting stricter. Rightly so, as it seems all the smaller juristictions that were welcoming earlier on have been under increasing pressure.

* https://cointelegraph.com/news/estonian-consulting-firm-claims-its-harder-to-get-a-crypto-license-following-regulation
* [AML Officer for Estonian crypto exchanges?](https://medium.com/@Comistar/aml-officer-for-estonian-crypto-exchanges-8f8ec34168fa)
  >Crypto exchanges registered in Estonia are experiencing uncertain times. This is due to change of AML regulations which Estonia is implementing for crypto exchanges. This will also mean that all license holders will need to have activity and management functions in Estonia by the end of 2019. For many foreigners, that’s a deal breaker, and we can fully understand it.
*https://paymentscompliance.com/premium-content/insights_analysis/depth-worrying-crypto-licensing-spree-estonia-set-eu-spotlight

[New Requirements for Cryptocurrency Companies in Estonia](https://www.gatetobaltics.com/news/new-requirements-for-cryptocurrency-companies-in-estonia)
  >Currently Estonian Parliament is reviewing changes in Money Laundering and Terrorist Financing Prevention Act. With high certainty the amendments will take effect from January 2020 and will be applied to all new licensing applications as well as to companies holding crypto exchange and e-wallet service provider licenses in Estonia. When approved, the amendments will include following requirements:
  > * Application fee will be raised from EUR 345 per license to EUR 3300;
  > * Company’s headquarter and management will be required to be located in Estonia;
  > * Application review time will be increased from 30 days up to 3 months. 

* https://www.brightlaw.ee/cryptolicenses - Cryptocurrency exchange and virtual wallet operating licenses
* https://e-resident.me/how-to-obtain-crypto-licenses-in-estonia/
* https://www2.politsei.ee/en/organisatsioon/rahapesu-andmeburoo/fius-advisory-guidelines/ - Must guard against transactions with no apparent legal purpose coming from juristictions with no AML in place
  >You may find information about the countries of high risk here: http://www.fatf-gafi.org/countries/#high-risk
* https://www2.politsei.ee/en/teenused/majandustegevuse-luba.dot
* https://www2.politsei.ee/en/organisatsioon/rahapesu-andmeburoo/estonian-legislation/
* https://incorporate.ee/insights/faq
* https://prifinance.com/en/cryptocurrency-license/estonia/
* https://e-resident.gov.ee/ - What is e-Residency | How to Start an EU Company Online
  > E-Residency, powered by the Republic of Estonia, enables entrepreneurs to start a trusted location-independent EU company online.
* https://e-resident.gov.ee/start-a-company/
* https://apply.gov.ee/ -Application for e-Residency
  >e-Residency offers to every world citizen a government-issued digital identity and the opportunity to run a trusted company online, unleashing the world’s entrepreneurial potential. The Republic of Estonia is the first country to offer e-Residency — a transnational digita...

### Gibraltar

>1. We spoke to people who had successfully run ICOs to ask for their recommendations.
>2. We spoke with contacts in the UK startup space with experience advising startups (non-crypto) and connections into the legal world to get technical advice free and fast.
>3. We cold contacted (either through phone or linkedin) legal teams around the world.
>The [summary of our research on jurisdiction narrowed our choices](https://medium.com/h-o-l-o/launching-an-ico-that-is-ready-to-bridge-the-old-and-new-worlds-from-the-outset-8ed6149ab905)
* [Where to set up a Crypto Exchange – Part 3: Start a Coin Exchange in Europe](https://flagtheory.com/set-up-coin-exchange-europe/) -2018
* [20 Companies in Gibraltar you should get to know](https://kintu.co/crypto-companies-gibraltar/)
* https://www.nomoretax.eu/gibraltar-legal-framework-cryptocurrency/
  * Honesty and integrity. In order to be fit and proper to undertake a DLT activity, companies will be required to conduct business in an integral, honest, skilled, and competent manner.
  * Fairness and clarity. For the purpose of protecting customer’s interests, companies engaged in DLT should mitigate the risks associated with use of DLT and employ the best practices in informing their clients in a clear, fair, ethical, and non-misleading way. Also, the clients of DLT companies should be notified about per-transaction risks and terms and conditions in an understandable language.
  * Maintenance of adequate financial and non-financial resources. DLT companies will be required to hold sufficient capital and monitor its sufficiency for business objectives. Moreover, in some cases, DLT companies will have to obtain professional indemnity insurance coverage. The requirements regarding non-financial resources will be imposed by the GFSC.
  * Risk management practices. The management and control practices of DLT companies should include skill, care, and diligence, in order to reduce and timely control any risks emerging from the DLT.
  * Protection of clients’ assets. DLT companies will be required to place effective arrangements, precautions, and records of transactions for protecting customers’ money from any DLT-related threats. Such custodial assets should be segregated from the company’s assets.
  * Corporate governance arrangements. DLT companies will be required to establish and clearly define their corporate systems, including board structure, business processes, culture and strategies. Also, DLT companies are required to maintain “open, cooperative and transparent relationship with the GFSC and other regulators”.
  * Secure systems and protocols. High security standards should be applied to company’s computer infrastructure, including proactive security assessments, mitigation of technological threats and vulnerabilities, as well as conducting and reporting independent compliance audits.
  * Prevention of financial crime risks. DLT companies have to ensure that they will take preventive measures and disclose any anti-money laundering, terrorist financing, and other suspicious transactions that may take place using the DLT.
  * Contingency plans for winding down of business. DLT companies should have clear and well-managed strategies to maintain action plans in cases of disaster recovery and crisis management.
* https://news.bitcoin.com/5-crypto-exchanges-have-been-licensed-in-gibraltar-since-regulation/
* https://offshoreincorporate.com/where-to-setup-an-ico-gibraltar/
* http://www.gibraltarlaw.com/dlt-regulation-gibraltar/
* https://www.gibraltarlawyers.com/practice/fintech
* [Why you should consider setting up your regulated crypto exchange in Gibraltar](https://flagtheory.com/dlt-license-crypto-exchange-gibraltar/)
* https://incorporations.io/gibraltar

## Malta

* https://flagtheory.com/malta-crypto-regulations-ico-exchanges/
  >The new Malta crypto regulations represent an important opportunity for the industry players, both for ICOs and for exchanges. In fact, two of the largest exchanges worldwide have recently incorporated in Malta.
  >
  >However, an ICO registered under the VFAA does not imply circumventing regulations of other jurisdictions. ICOs have a global scope so an ICO can still be subject to the laws of the countries where it operates or where it sells tokens.
* https://news.bitcoin.com/malta-might-be-blockchain-island-but-dont-try-opening-a-crypto-bank-account/
  >These [cryptocurrency] outfits, like any other industry, need bank accounts to manage their operations. We have seen a slight opening from certain banks – albeit, in all honesty, it’s still minimal. I am sanguine, however, that once our regulatory regime is fully rolled out (which is imminent), banks can assess the sector from a different angle and in all probability be more receptive to this new industry.

http://justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29080&l=1
  >AN ACT to provide for the establishment of an Authority to be known as the Malta    Digital    Innovation    Authority,    to    support    the    development    and implementation  of  the  guiding  principles  described  in  this  Act  and  to  promoteconsistent  principles  for  the  development  of  visions,  skills,  and  other  qualities relating   to   technology   innovation,   including   distributed   or   decentralise technology, and to exercise regulatory functions regarding innovative technology, arrangements and related services and to make provision with respect to matters ancillary thereto or connected therewith
http://www.justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29078&l=1
  >AN  ACT  to  provide  for  the  regulation  of  designated  innovative  technology arrangements referred to in the Act, as well as of designated innovative technology services  referred  to  in  the  Act,  and  for  the  exercise  by  or  on  behalf  of  the  MaltaDigital Innovation Authority of regulatory functions with regard thereto.
http://justiceservices.gov.mt/DownloadDocument.aspx?app=lp&itemid=29079&l=1
  >AN ACT to regulate the field of Initial Virtual Financial Asset Offerings and Virtual Financial Assets and to make provision for matters ancillary or incidental thereto or connected therewith

* https://cryptodigestnews.com/tough-but-worth-it-pros-and-cons-of-blockchain-regulation-in-malta-c337a5906025
  >In the final tally, starting a crypto or blockchain business in Malta requires a significant investment, both in terms of time and money. Companies looking for a quick and easy start would be better off going elsewhere; for example, Estonia. But for those ready to play in the big leagues — and equipped with the resources to do so — Malta provides a promising entry point.
* https://bitcoinmagazine.com/articles/eu-warns-malta-of-the-dangers-of-neglecting-money-laundering
  >Malta has been advised to step up its Anti-Money Laundering (AML) enforcement just as it's growing its crypto and gaming sector. The European Union believes the island needs to increase its AML policing meet the demands of its ever-growing fintech industries. European Commission made these comments in its recommendations to member states on the utilization of EU funds, Malta Today reports.

  
## Resources
### Compliance

* [Outlier Compliance Group Live Stream - Canadian AML Updates for Dealers in Virtual Currency](https://www.youtube.com/watch?v=Wkf--uOyvAQ) (video)
* [Dash has partnered with a “transaction monitoring solutions for blockchains”](https://eng.ambcrypto.com/dash-joins-hands-with-blockchainintel-to-promote-adoption-and-provide-compliance-reporting-for-exchanges/)
  >In an attempt to increase the adoption of its crypto, Dash has partnered with a “transaction monitoring solutions for blockchains” company, BlockchainIntel. BlockchainIntel analyzes cryptocurrency transactions, along with addressing and risk-scoring them 
* [What Crypto Exchanges Do to Comply With KYC, AML and CFT Regulations](https://cointelegraph.com/news/what-crypto-exchanges-do-to-comply-with-kyc-aml-and-cft-regulations)
  >Top fiat-to-crypto exchanges are adopting market surveillance technologies. Of all crypto-to-crypto exchanges, only Binance has one.
* [𝚓𝚊𝚛𝚞𝚐𝚊 (@har00ga)](https://twitter.com/har00ga/status/1155034888594173952?s=12)
  >Oh crap
**Part VI – Principles for Stablecoin Issuers**
https://www.gdf.io/docsconsultations/part-vi-code-of-conduct-principles-for-stablecoin-issuers/

**Part VII –  Principles for Security Token Offerings & Secondary Market Trading Platforms**
https://www.gdf.io/docsconsultations/part-vii-code-of-conduct-principles-for-security-token-offerings-secondary-market-trading-platforms/

**Part VIII -Principles for Know Your Customer (KYC) & Anti-Money Laundering (AML)**
https://www.gdf.io/docsconsultations/part-viii-code-of-conduct-principles-for-know-your-customer-kyc-anti-money-laundering-aml/

### Custody

**Crypto Asset Safekeeping and Custody**
https://www.gdf.io/wp-content/uploads/2019/02/GDF-Crypto-Asset-Safekeeping_20-April-2019.pdf
* [Crypto Startup Wants You to Trade on Exchanges Without Trusting Them](https://www.coindesk.com/crypto-startup-wants-you-to-trade-on-exchanges-without-trusting-them)
  * [The Arwen Trading Protocols](https://arwen.io/whitepaper.pdf) ∗ Ethan Heilman, Sebastien Lipmann, Sharon Goldberg - January 28, 2019
    > The Arwen Trading Protocol is a layer-two blockhchainprotocol that allows traders to securely trade cryptocur-rencies  at  a  centralized  exchange,  without  ceding  cus-tody of their coins to the exchange.  Before trading be-gins, traders deposit their coins in an on-blockchain es-crow, rather than in the exchange’s wallet.  The agent ofescrow is the blockchain itself.  Each individual trade isbacked by the coins locked in escrow.  Each trade is fast,because it happens off-blockchain, and secure, becauseatomic swaps prevent even a hacked exchange from tak-ing  custody  of  a  trader’s  coins.   Arwen  is  designed  towork  even  with  the “lowest  common  denominator” ofblockchains—namely Bitcoin-derived coins without Seg-Wit  support.   As  a  result,  Arwen  supports  essentiallyall “Bitcoin-derived” coins, including BTC, LTC, BCH,ZEC as well as Ethereum and ERC-20 tokens.

### Banking

* [Which Banks Accept Bitcoin? Get The List | Banks.com](https://www.banks.com/articles/cryptocurrency/banks-that-accept-bitcoin/)
* [Where to open a bank account for Crypto business](https://bitcointalk.org/index.php?topic=2674704.0)

### Exchanges

* [Detailed Report Into The Cryptocurrency Exchange Industry (From CryptoCompare)](https://blog.bitmex.com/crypto-currency-exchange-review-from-cryptocompare/)
* [Over Half of All Crypto Exchanges Have Security Vulnerabilities](https://www.ccn.com/over-half-of-all-crypto-exchanges-have-security-vulnerabilities-report/)
* https://flagtheory.com/set-up-cryptocurrency-exchange/

#### DEX

* https://stanford-jblp.pubpub.org/pub/deconstructing-dex
* [SEC Charges EtherDelta Founder With Operating an Unregistered Exchange](https://www.sec.gov/news/press-release/2018-258)

## [PremierOffshore.com](http://premieroffshore.com/)
This site is a valuable resource, and worth a browse. These are its main blog categories, and I'll save any direct links below that fall beyond the scope of this assignment:

- [Asset Protection](http://premieroffshore.com/tag/asset-protection-2/)
- [Offshore Company](http://premieroffshore.com/offshore-company-and-offshore-companies/)
- [Panama Foundation](http://premieroffshore.com/panama-foundation-asset-protection-2/)
- [International Trust](http://premieroffshore.com/international-trust-asset-protection-trust/)
- [Offshore Business Planning](http://premieroffshore.com/offshore-business-planning/)
- [Offshore Bank Account](http://premieroffshore.com/offshore-bank-account-offshore-banking/)
- [Self Directed IRA](http://premieroffshore.com/self-directed-ira/)
- [Second Passport](http://premieroffshore.com/second-passport-2/)
- [US Tax Preparation](http://premieroffshore.com/offshore-tax-international-tax/)
- [Offshore Merchant Accounts](http://premieroffshore.com/offshore-merchant-accounts/)
- [Offshore Bank License](http://premieroffshore.com/offshore-bank-license/)

[How to Build an International Cryptocurrency Exchange](http://premieroffshore.com/how-to-build-an-international-cryptocurrency-exchange/) (this is a keyphase I should do a search on, later)
